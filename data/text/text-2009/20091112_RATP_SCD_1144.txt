
DIALOG=20091112_RATP_SCD_1144

001 spk1>	 NNAAMMEE bonjour je vous écoute
002 spk2>	 oui bonjour
003 spk2>	 en+fait c' est pour un renseignement je voulais savoir à quelle heure partait le premier métro en+fait le matin je suis dans le
004 spk2>	 Paris Paris
005 spk1>	 alors
006 spk2>	 onzième
007 spk2>	 ouais
008 spk1>	 tous
009 spk1>	 les premiers métros quittent leur terminus à cinq heures trente madame précises
010 spk2>	 ah cinq heures
011 spk2>	 trente ah oui d'accord
012 spk1>	 à cinq heures trente
013 spk1>	 tous+les premiers métros quittent leur terminus leur leur terminus de départ
014 spk2>	 euh mais qu'est-ce+que vous appelez terminus euh terminus de
015 spk2>	 départ c' est quoi c' est par+rapport+à
016 spk1>	 ben leur point de départ
017 spk2>	 parce+que moi par+exemple je pars de Ledru-Rollin
018 spk1>	 alors vous
019 spk1>	 voulez l' heure de passage
020 spk2>	 c' est pareil c' est ouais
021 spk1>	 du premier
022 spk1>	 métro à la
023 spk2>	 oui voilà ouais
024 spk1>	 station laquelle Ledru-Rollin
025 spk2>	 euh Ledru-Rollin ouais
026 spk1>	 pour aller jusqu'+où madame
027 spk2>	 euh c' était pour aller jusqu'à Gare+du+Nord donc je dois avoir un changement à Châtelet
028 spk1>	 Gare+du+Nord vous patientez un instant je
029 spk1>	 recherche ne
030 spk2>	 merci
031 spk1>	 quittez pas
032 spk2>	 merci
033 spk1>	 madame
034 spk2>	 oui
035 spk1>	 l' heure du passage du premier métro est à cinq heures trente-trois à Ledru-Rollin
036 spk2>	 ah d'accord
037 spk1>	 oui
038 spk1>	 parce+qu' il y a un
039 spk2>	 d'accord
040 spk1>	 terminus intermédiaire à République sur la ligne euh huit
041 spk2>	 d'accord bon+ben c' est tôt et+donc c' est bon non je devrais avoir mon train OK
042 spk2>	 d'accord
043 spk1>	 vous pouvez
044 spk1>	 espérer arriver à la Gare+du+Nord à cinq heures cinquante madame votre train est à quelle heure
045 spk2>	 oh ça va il est à six heures et demie
046 spk1>	 ah sans problème madame
047 spk2>	 ah oui d'accord
048 spk2>	 sans problème OK
049 spk1>	 sans problème
050 spk2>	 ben
051 spk1>	 hm
052 spk2>	 merci beaucoup hein
053 spk2>	 désolée hein au+revoir OK merci hein au+revoir
054 spk1>	 je vous en prie il y a pas de soucis il y a pas de mal madame il y a pas de mal je suis là pour ça au au madame

