
DIALOG=20091112_RATP_SCD_0916

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 bonjour monsieur j' aimerais sa
003 spk2>	 donc euh l() là là je suis avec un un ami à Place+d'Italie alors j' aimerais savoir comment ça se fait que quarante-sept un bus sur quatre s'il+vous+plaît
004 spk1>	 Place+d'Italie
005 spk1>	 alors je
006 spk2>	 oui
007 spk1>	 vais regarder parce+qu' il y a un pré() il y a eu un il y a des grèves sur le dépôt de Vitry
008 spk1>	 donc il faut
009 spk2>	 ah il y a des grèves
010 spk2>	 au+dépôt+de Vitry
011 spk1>	 ouais ouais
012 spk1>	 toutes+les lignes sont sont touchées
013 spk2>	 ça
014 spk2>	 ça veut dire qu' il faut l' attendre pendant pendant combien+de temps le bus s'il+vous+plaît
015 spk1>	 alors déjà il va falloir je me renseigne combien il y a de bus qui circulent sur la ligne
016 spk1>	 vous patientez
017 spk2>	 ben on sait pas
018 spk1>	 parce+que i() non mais moi je vais me renseigner
019 spk2>	 d'accord
020 spk1>	 vous patientez
021 spk1>	 merci
022 spk2>	 merci
023 spk3>	 oui C M L
024 spk1>	 oui c' est toujours nous C M L c' est NNAAMMEE
025 spk1>	 je t' appelle
026 spk3>	 vas -y je t' écoute
027 spk1>	 par+rapport+au niveau des grèves
028 spk3>	 oui
029 spk1>	 a() a() sur le quarante-sept je voudrais juste
030 spk3>	 oui
031 spk1>	 savoir
032 spk1>	 s' il te plaît t' as combien+de lignes euh de bus qui tournent s' il te plaît
033 spk3>	 sur le quarante-sept j' en ai euh deux trois quatre cinq six j' en ai
034 spk3>	 sept
035 spk1>	 ah
036 spk1>	 t' en as sept ben dis donc est
037 spk1>	 bien ça par+rapport+aux autres
038 spk3>	 oui oui
039 spk3>	 oui oui c' est pas trop mal sur
040 spk1>	 ouais
041 spk3>	 le quarante euh
042 spk1>	 tu as quelque+chose qui arrive sur la Place+d'Italie
043 spk3>	 Place+d'Italie
044 spk1>	 ah par+contre
045 spk3>	 ben il vient
046 spk1>	 j' ai pas demandé
047 spk3>	 quelle direction ça doit être K B
048 spk1>	 attends je
049 spk3>	 à mon avis
050 spk1>	 je vais lui
051 spk3>	 hein
052 spk1>	 demander
053 spk1>	 rapidement s' il te plaît
054 spk3>	 ben à mon avis K si c' est K B c' est pas tout+de+suite et si c' est dans l' autre sens elle le l' a raté
055 spk1>	 ah
056 spk3>	 voilà
057 spk1>	 d'accord
058 spk3>	 donc il faut patienter
059 spk1>	 attends je lui demande
060 spk1>	 rapidement
061 spk3>	 vas
062 spk3>	 y vas -y
063 spk1>	 s'il+vous+plaît
064 spk1>	 dans quel
065 spk1>	 sens
066 spk2>	 oui
067 spk1>	 vous allez monsieur vers euh Gare+de+l'Est ou f() Bicêtre
068 spk2>	 euh Bicêtre
069 spk1>	 d'accord
070 spk1>	 ouais il va vers Bicêtre
071 spk3>	 ouais ben métro
072 spk1>	 ah métro d'accord
073 spk3>	 voilà ça sera
074 spk3>	 plus rapide
075 spk1>	 parce+que là ça ça revient pas c' est ça
076 spk3>	 ouais non là j' ai euh le il est à Gare+de+l'Est
077 spk3>	 encore
078 spk1>	 là il est
079 spk1>	 à la Gare+de+l'Est oh+là
080 spk3>	 oui oui oui oui là c' est il y a un gros trou là
081 spk1>	 ah d'accord d'accord bon+ben c' est pas ouais ouais ouais
082 spk1>	 eh+ben je te remercie en+tout+cas
083 spk1>	 bon courage hein
084 spk3>	 c' est
085 spk3>	 moi
086 spk3>	 salut
087 spk1>	 salut
088 spk1>	 s'il+vous+plaît
089 spk2>	 oui
090 spk1>	 bon là je pense que vous il va falloir prendre le métro hein
091 spk1>	 parce+que i() s() il y a sept bus qui qui circulent mais là
092 spk1>	 euh
093 spk2>	 ouais
094 spk1>	 ils sont tous dans l' autre sens
095 spk1>	 le prochain qui
096 spk2>	 ah ils vont tous dans l' autre sens
097 spk1>	 venir ouais
098 spk1>	 le prochain qui arrive dans votre sens il est à la Gare+de+l'Est encore
099 spk2>	 il est encore Gare+de+l'Est
100 spk1>	 ouais donc il faut prendre le
101 spk2>	 là ben heureusement qu' on
102 spk1>	 métro là
103 spk2>	 OK ben je
104 spk2>	 vous remercie
105 spk1>	 je vous en prie
106 spk1>	 bonne journée
107 spk2>	 au+revoir
108 spk1>	 monsieur au+revoir

