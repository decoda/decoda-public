
DIALOG=20091112_RATP_SCD_0126

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 oui bonjour euh j' habite à Cachan je voudrais aller au à IKEA Thiais euh c' est le TVM normalement euh est-ce+qu' il fonctionne le dimanche s'il+vous+plaît
004 spk2>	 le dimanche oui oui il circule tous+les jours ce bus hein
005 spk1>	 d'accord est-ce+que vous pouvez me dire à quel euh moment je peux être en en liaison avec parce+que je prends euh de Cachan ou le cent+quatre-vingt-quatre ou le cent+quatre-vingt-sept de bus euh
006 spk2>	 alors à quel arrêt vous le prenez
007 spk3>	 euh mairie de Cachan dimanche matin
008 spk2>	 mairie de Cachan d'accord
009 spk2>	 vous patientez un instant
010 spk3>	 monte vers Fresnes ouais
011 spk2>	 hm un instant
012 spk2>	 hm madame
013 spk3>	 oui
014 spk2>	 plutôt avec une correspondance
015 spk2>	 avec le cent+quatre-vingt-sept
016 spk3>	 cent+quatre-vingt-sept oui
017 spk2>	 oui il faudrait que vous descendiez à l' arrêt Tuileries
018 spk3>	 Tuileries
019 spk2>	 donc là vous avez le TVM à la hauteur du de l' arrêt du docteur Tenine
020 spk3>	 docteur Ténine
021 spk3>	 d'accord ben je vous remercie
022 spk3>	 beaucoup au+revoir
023 spk2>	 je vous en prie bonne journée au+revoir

