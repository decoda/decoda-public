
DIALOG=20091112_RATP_SCD_1069

001 spk1>	 NNAAMMEE bonjour
002 spk1>	 NNAAMMEE bonjour
003 spk2>	 allo oui bonjour monsieur
004 spk1>	 oui bonjour
005 spk2>	 oui bonjour
006 spk2>	 c' est pour signaler que j' ai euh oublié mon sac euh sur la ligne vingt-deux
007 spk1>	 alors sac la ligne vingt-deux
008 spk2>	 donc
009 spk1>	 c' était quoi q()
010 spk1>	 oui
011 spk2>	 il était bleu et noir c' était un un sac attendez je vais vous dire la marque bon bref dedans il y avaient des affaires de sport un short un t-shirt rouge et blanc et des baskets blanches et un+peu bleu violettes sur le bout
012 spk1>	 d'accord
013 spk2>	 et il y avait un pistolet à billes dedans aussi
014 spk1>	 oui
015 spk2>	 et voilà et je
016 spk1>	 et ça fait combien+de temps à+peu+près que vous l' avez perdu
017 spk2>	 je l' ai oublié ce matin
018 spk1>	 ce matin alors veuillez patienter je me renseigne hein
019 spk2>	 merci beaucoup
020 spk1>	 je vous en prie
021 spk3>	 la Porte+de+Saint-Cloud
022 spk1>	 ouais
023 spk3>	 bonjour
024 spk1>	 bonsoir excuse -moi de te déranger c' est le Service+Clients+à+Distance juste un petit renseignement par+rapport+à un OT s' il te plaît oublié ce matin dans le bus vingt-deux est-ce+que tu aurais eu un
025 spk3>	 alors
026 spk1>	 sac à dos
027 spk3>	 alors je peux pas te dire parce+que maintenant c' est le BU
028 spk1>	 ah oui oui alors
029 spk3>	 qui gère les OT alors
030 spk3>	 tu peux composer le cinquante-neuf+mille+trois+cent+quarante-cinq
031 spk1>	 alors cinquante-neuf+mille+trois+cent+quarante-cinq je te remercie
032 spk3>	 je t' en prie merci à toi au+revoir
033 spk1>	 bonne fin de journée au+revoir
034 spk1>	 hein
035 spk1>	 non j' y crois pas
036 spk1>	 non+sans déconner
037 spk1>	 c' est une plaisanterie ou quoi
038 spk1>	 ah j' arrive pas à avoir
039 spk1>	 s'il+vous+plaît
040 spk1>	 s'il+vous+plaît
041 spk2>	 oui
042 spk1>	 je suis désolé mais j' arrive à avoir personne au téléphone à cette heure-ci malheureusement alors moi je vous conseille alors il faut faut rappeler mais z() z() le demain à+partir+de dix heures
043 spk1>	 on a un logiciel
044 spk2>	 oui
045 spk1>	 où tous+les objets de la veille sont enregistrés
046 spk2>	 oui
047 spk1>	 hein donc la personne d() que que vous aurez au téléphone consultera directement le logiciel et parce+qu' on n' aura pas besoin d' intermédiaire parce+que a+priori là malheureusement j' arrive pas à avoir personne au téléphone j' ai eu une personne et elle m' a dit de téléphoner un autre numéro et malheureusement là il y a personne qui répond alors
048 spk2>	 OK
049 spk1>	 j' avance j' avance pas on va dire donc demain à+partir+de dix heures on a un logiciel on peut consulter et on vous dit vraiment s' il y a quelque+chose ou pas
050 spk2>	 OK d'accord ben merci beaucoup
051 spk2>	 monsieur mais euh enfin c' est
052 spk1>	 désolé hein je vous en prie
053 spk2>	 je peux quand+même le récupérer même+si c' est le lendemain
054 spk1>	 alors normalement oui normalement je pense pas qu' il soit parti aujourd'hui il sera parti que demain oui oui je pense
055 spk1>	 il y a de fortes chances
056 spk2>	 comment ça parti
057 spk1>	 c'est-à-dire ils vont aller il va être expédié au trente-six rue des Morillons mais si vous appelez demain à+partir+de dix heures la personne que vous aurez au téléphone téléphonera au dépôt pour savoir s' ils l' ont toujours puis à+ce+moment-là s' ils l' ont
058 spk1>	 vous pourrez les
059 spk2>	 et je
060 spk2>	 je le cherche où
061 spk2>	 je vais le chercher où après
062 spk1>	 à la Porte+de+Saint-Cloud
063 spk2>	 Porte+de+Saint-Cloud OK
064 spk1>	 c' est dans le seizième hein
065 spk2>	 oui ben oui
066 spk1>	 donc normalement
067 spk1>	 il y aura il y aura aucun problème pour récupérer
068 spk2>	 c' est un c' est quoi c' est un terminus
069 spk1>	 non c' est c' est le dépôt
070 spk2>	 quoi
071 spk1>	 un dépôt du
072 spk1>	 de point du jour de de tous+les bus euh du vingt-deux par+exemple hm
073 spk2>	 OK d'accord ben écoutez
074 spk2>	 merci beaucoup
075 spk1>	 je vous en prie
076 spk2>	 et si+jamais il a été remis à la
077 spk1>	 alors si par+exemple aujourd'hui il a été remis
078 spk1>	 vraiment il est non je pense pas il partira pas ce soir
079 spk2>	 d'accord OK
080 spk1>	 donc vous appelez à+partir+de demain
081 spk2>	 je vous remercie beaucoup
082 spk1>	 et il y a aucun problème parce+que c' est le dépôt
083 spk2>	 et
084 spk1>	 tout+de+suite
085 spk2>	 demain dix heures
086 spk2>	 pas avant
087 spk1>	 ah pas avant dix heures parce+que le logiciel comme+ça au+moins on est sûr que tous+les objets sont enregistrés
088 spk2>	 et il y a pas de risque
089 spk1>	 à+partir+de
090 spk2>	 qu' il parte
091 spk1>	 non non non non
092 spk2>	 avant dix heures
093 spk1>	 non non
094 spk2>	 d'accord ben merci beaucoup
095 spk1>	 non hm
096 spk1>	 ah je vous en prie
097 spk2>	 au+revoir monsieur
098 spk1>	 bonne fin de journée au+revoir

