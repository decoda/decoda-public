
DIALOG=20091112_RATP_SCD_0794

001 spk1>	 un instant NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour je vous écoute
003 spk3>	 oui bonjour
004 spk3>	 je suis au métro Ménilmontant
005 spk2>	 oui
006 spk3>	 et
007 spk3>	 je dois me rendre euh exactement à l' adresse au soixante+seize rue Aristide+Briand dans l() à Levallois dans le quatre-vingt-douze trois+cents
008 spk2>	 alors
009 spk3>	 donc Levallois Perret oui oui
010 spk2>	 donc pour Levallois-Perret soixante-seize rue Aristide+Briand
011 spk2>	 d'accord vous vous partez de Ménilmontant
012 spk3>	 voilà
013 spk3>	 exactement donc je voulais
014 spk2>	 hm d'accord
015 spk3>	 savoir à+peu+près combien+de temps ça prenait et combien enfin
016 spk3>	 oui s'il+vous+plaît oui
017 spk2>	 l' itinéraire hm d'accord
018 spk2>	 vous patientez un instant madame je recherche
019 spk3>	 je je vous remercie madame
020 spk2>	 ne quittez pas
021 spk2>	 madame
022 spk3>	 oui
023 spk2>	 alors j' ai un trajet en quarante-cinq minutes qui se décompose comme suit au+départ+de Ménilmontant vous prendrez le métro ligne deux
024 spk3>	 oui
025 spk2>	 direction Porte+Dauphine
026 spk3>	 d'accord je m' arrête donc à Porte+Dauphine
027 spk3>	 c' est ça
028 spk2>	 non
029 spk2>	 en direction
030 spk3>	 ah d'accord
031 spk3>	 ah d() d'accord oui OK
032 spk2>	 de la Porte+Dauphine jusqu'à la
033 spk2>	 Place+de+Clichy
034 spk3>	 Dauphine donc l' arrêt c' est to() euh stop au Place+de+Clichy
035 spk3>	 d'accord oui
036 spk2>	 oui
037 spk2>	 à la Place+de+Clichy vous prendrez en correspondance le métro ligne treize
038 spk3>	 alors ligne treize oui
039 spk2>	 direction Les+Courtilles
040 spk3>	 Les+Courtilles ouais
041 spk2>	 jusqu'à la mairie de Clichy
042 spk3>	 m() mairie de Clichy
043 spk2>	 de Clichy
044 spk2>	 et pour terminer
045 spk3>	 d'accord
046 spk2>	 à la Mairie+de+Clichy prenez le bus deux+cent+soixante-quatorze
047 spk3>	 bus deux+cent+soixante-quatorze
048 spk2>	 en direction de Voltaire-Villiers
049 spk2>	 direction v()
050 spk2>	 Voltaire
051 spk2>	 Villiers
052 spk3>	 ouais
053 spk2>	 et descendre à l' arrêt mairie de Levallois
054 spk3>	 descendre mairie de Levallois
055 spk2>	 tout+à+fait
056 spk3>	 c' est pas la porte à côté ça
057 spk3>	 moi
058 spk2>	 non
059 spk3>	 je pensais que il y avait que c' était plus près du métro que j' avais pas besoin de bus mais bon
060 spk2>	 hm
061 spk3>	 d'accord très+bien je vous remercie
062 spk2>	 je vous en prie
063 spk3>	 au+revoir
064 spk2>	 au+revoir madame

