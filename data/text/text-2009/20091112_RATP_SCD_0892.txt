
DIALOG=20091112_RATP_SCD_0892

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour madame
003 spk2>	 je j' essaie de joindre désespérément une société qui qui je pense qui est filiale à vous qui s' appelle Naxos
004 spk2>	 je sais pas
005 spk2>	 si vous
006 spk1>	 alors écoutez
007 spk1>	 là vous êtes à la RATP
008 spk2>	 ben oui
009 spk2>	 mais j' ai j' ai
010 spk1>	 là d'accord
011 spk2>	 c' est le les annuaires qui m' ont dit euh éventuellement d' essayer avec vous parce+que j' arrive pas à les
012 spk2>	 joindre
013 spk1>	 ah non
014 spk1>	 non non non là je peux pas+du+tout vous renseigner
015 spk2>	 et comment vous avez pas un siège social euh où je pourrais savoir c' est une filiale à vous
016 spk2>	 Naxos
017 spk1>	 je vais vous donner
018 spk1>	 le numéro de le de la du standard de la RATP
019 spk1>	 pour pouvoir
020 spk2>	 d'accord
021 spk1>	 vous renseigner donc
022 spk1>	 oui
023 spk2>	 vous allez appeler
024 spk1>	 le zéro+un+cinquante-huit
025 spk2>	 zéro+un+cinquante-huit
026 spk1>	 soixante-dix-huit
027 spk2>	 oui
028 spk1>	 vingt+vingt
029 spk2>	 vingt+vingt
030 spk2>	 d'accord merci beaucoup madame au+revoir
031 spk1>	 d'accord bonne journée au+revoir

