
DIALOG=20091112_RATP_SCD_0099

001 spk1>	 allo
002 spk2>	 euh ouais
003 spk1>	 allo
004 spk2>	 NNAAMMEE bonjour bonjour madame
005 spk1>	 bonjour euh parce+que je vous entend très mal
006 spk1>	 s'il+vous+plaît par+rapport aller jusque euh le à la place Ondrieu Athis+Cars
007 spk1>	 est-ce+qu' il y a des bus
008 spk1>	 à+part
009 spk2>	 ah
010 spk2>	 parce+que nous on a des euh Athis+Cars c' est pas nous qui les gérons les Athis+Cars je vais vous donner le numéro de téléphone de la compagnie
011 spk1>	 d'accord
012 spk2>	 alors attendez
013 spk1>	 parce+que
014 spk1>	 comme c' est marqué sur la feuille euh
015 spk1>	 votre numéro
016 spk2>	 oui mais là apparemment il y a
017 spk2>	 je sais pas ça fait plusieurs appels qu' on a
018 spk1>	 ouais
019 spk2>	 et euh alors Athis+Cars
020 spk2>	 Athis+Cars il faut contacter le zéro+huit+vingt-cinq
021 spk1>	 ouais
022 spk2>	 attendez je regarde parce+que je vois mal le numéro
023 spk2>	 alors Athis+Cars
024 spk2>	 zéro+huit+vingt-cinq+zéro+zéro
025 spk2>	 trente-huit trente-six
026 spk1>	 ouais
027 spk1>	 trente-huit trente-six
028 spk2>	 tout+à+fait
029 spk1>	 et la la RATP il y euh il y en a il y a euh il y a pas de soucis
030 spk2>	 à la RATP il y a pas de soucis on a que de le le dépôt de Vitry qui mais dans le sud de Paris où il y a des problèmes
031 spk1>	 d'accord mais il y euh de Massy à Juvisy il y il y a aucun problème
032 spk2>	 de
033 spk1>	 de Massy à Juvisy
034 spk2>	 bah quel bus vous prenez d'+habitude
035 spk1>	 le trois+cent+quatre-vingt-dix-neuf
036 spk2>	 ben il faut regarder parce+que s' il fait d() partie du dépôt de Vitry c' est possible qu' il y ait des perturbations trois+cent+quatre-vingt-dix-neuf hein
037 spk1>	 ouais
038 spk2>	 alors trois+cent+quatre-vingt-dix-neuf euh nan c' est bon
039 spk2>	 trafic normal
040 spk1>	 c' est bon
041 spk1>	 d'accord d'accord merci au+revoir
042 spk2>	 belle journée au+revoir
043 spk1>	 merci à vous aussi au+revoir
044 spk1>	 a()

