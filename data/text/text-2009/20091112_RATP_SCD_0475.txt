
DIALOG=20091112_RATP_SCD_0475

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour
003 spk2>	 je voudrais savoir déjà si le trente-huit euh roule le soir
004 spk1>	 hum
005 spk2>	 enfin après
006 spk2>	 après dix heures et demie onze heures
007 spk1>	 alors le trente-huit si c' est un bus euh de soirée oui oui oui
008 spk2>	 hm bon
009 spk2>	 et le moyen le+plus rapide pour aller jusqu'à la station Mouton-Duvernet j' ai regardé sur RATP on me donne le quatre-vingt-quinze et ensuite le soixante-huit pouvez -vous regarder s' il y a quelque+chose de+plus simple
010 spk1>	 mais en partant d'où
011 spk2>	 de du dix-huitième excusez -moi
012 spk1>	 oui
013 spk2>	 moi je le sais donc vous devriez le savoir
014 spk1>	 ah+ben je devrais le deviner
015 spk1>	 c' est sûr
016 spk2>	 voilà
017 spk1>	 je vois à+travers le téléphone
018 spk2>	 donc de la rue Joseph+de+Maistre dans le dix-huitième
019 spk1>	 hm
020 spk2>	 au quatre-vingt-six jusqu' au cinquante+et+un rue Hallé dans le quatorzième
021 spk1>	 hum d'accord à la hauteur de Mouton-Duvernet hein
022 spk2>	 voilà
023 spk1>	 et+donc plu
024 spk2>	 euh même euh
025 spk2>	 si on peut avoir un+peu plus près parce+que c' est quand+même euh
026 spk1>	 c' est quelle rue
027 spk2>	 la rue Hallé H A deux L É
028 spk1>	 hm d'accord
029 spk2>	 moi j' ai donc quatre-vingt-quinze et soixante-huit
030 spk1>	 mais ça c' est en+fin+de journée enfin en
031 spk1>	 en+début+de soirée
032 spk2>	 oui oui oui j' ai rendez-vous
033 spk2>	 à vingt heures là-bas
034 spk1>	 hm
035 spk2>	 donc euh si je prends l' autobus il faut que je compte une heure pour y aller
036 spk1>	 hm d'accord
037 spk1>	 alors moi
038 spk2>	 le métro on me dit
039 spk2>	 que la ligne le jeudi douze est une ligne est une journée à perturbation donc j' évite le métro
040 spk1>	 hm+hm non mais le l' itinéraire que le l' ordinateur enfin RATP hein moi j' ai le même hein là hein
041 spk2>	 d'accord
042 spk1>	 le quatre-vingt-quinze jusqu'à Place+de+Clichy
043 spk2>	 il y a à Place+de+Clichy il y a le soixante-huit
044 spk1>	 oui
045 spk2>	 parce+que là il me donne Auber
046 spk1>	 non à s() le soixante-huit il passe à enfin c' est son point de départ hein à Place+de+Clichy
047 spk2>	 ah+bon
048 spk2>	 et il est où sur la Place+de+Clichy
049 spk2>	 vous pouvez me guider
050 spk1>	 il est sur la rue de
051 spk1>	 Clichy
052 spk2>	 rue de Clichy
053 spk1>	 à la hauteur du quatre-vingts à+peu+près rue de Clichy
054 spk2>	 ah+ben c' est bien donc on peut le prendre au départ
055 spk1>	 ben oui hm c' est direct
056 spk2>	 eh+ben c' est parfait alors
057 spk2>	 merci beaucoup
058 spk1>	 hum je vous
059 spk1>	 en prie
060 spk1>	 bonne journée
061 spk2>	 au+revoir
062 spk1>	 au+revoir

