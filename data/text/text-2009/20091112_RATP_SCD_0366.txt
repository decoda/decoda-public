
DIALOG=20091112_RATP_SCD_0366

001 spk1>	 NNAAMMEE bonjour NNAAMMEE bonjour
002 spk2>	 oui bonjour est-ce+que je pourrais parler à madame NNAAMMEE s'il+vous+plaît
003 spk1>	 et cette personne travaille où dans quel service
004 spk2>	 j' ai euh un numéro de pour affaire là qui+c'est+qui m' a donné ça là
005 spk1>	 par+rapport+à un PV
006 spk2>	 une infraction
007 spk1>	 une infraction
008 spk2>	 s'il+vous+plaît monsieur
009 spk1>	 alors attendez parce+que comme là vous êtes sur la plateforme téléphonique générale c' était dans un métro un bus
010 spk2>	 un un métro
011 spk1>	 un métro
012 spk2>	 RER
013 spk1>	 RER
014 spk2>	 je pense que ça doit être ça
015 spk1>	 le RER+A vous vous rappelez pas
016 spk2>	 voilà c' est ça monsieur vous êtes super
017 spk1>	 alors attendez je vais vérifier
018 spk2>	 vous connaissez mieux que moi
019 spk1>	 non mais je demande parce+que c' est soit le RER ou le métro donc en+général ou les bus
020 spk2>	 je vous ai contrarié
021 spk1>	 alors je vais regarder
022 spk2>	 je vais vous donner le numéro de dossier
023 spk1>	 madame comment vous m' avez dit
024 spk2>	 madame NNAAMMEE
025 spk1>	 hum attendez je regarde un petit instant je vais vérifier où travaille cette personne pour vous mettre en+relation+avec elle hein
026 spk2>	 autrement c' était pour juste dire pourquoi que euh
027 spk1>	 ne quittez pas
028 spk1>	 bah oui oui mais là faut que je préfère que ça soit elle
029 spk2>	 allez -y et merci
030 spk1>	 ne quittez pas hein merci
031 spk1>	 s'il+vous+plaît
032 spk2>	 oui
033 spk1>	 je vous remercie d' avoir patienté
034 spk1>	 vous pouvez m' épeler son nom NNAAMMEE ça s' écrit
035 spk2>	 euh NNAAMMEE mais je ne sais pas c' est pas un zéro+huit que vient d' appeler là NNAAMMEE
036 spk1>	 non c' est pas un zéro+huit mais c' est un numéro qu' est quand+même pas euh qu' est pas le moins cher hein
037 spk2>	 oui mais c' est déjà c' est combien la minute
038 spk1>	 trente-quatre centimes
039 spk2>	 ah parce+que
040 spk1>	 oui oui oui c' est ouais mais le problème c' est que je n' ai pas la personne bon je vais vous mettre en relation quand+même
041 spk2>	 ah+bah laissez tomber parce+que moi je ne vais payer vingt euros pour euh
042 spk1>	 bah oui mais là faut que je trouve la personne et je vois pas
043 spk2>	 mais c' est pas grave laissez tomber
044 spk1>	 bon d'accord très+bien je vous en prie au+revoir
045 spk2>	 merci beaucoup monsieur au+revoir

