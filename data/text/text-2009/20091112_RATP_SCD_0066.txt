
DIALOG=20091112_RATP_SCD_0066

001 spk1>	 NNAAMMEE bonjour s'il+vous+plaît
002 spk2>	 oui bonjour madame
003 spk1>	 madame
004 spk2>	 excusez -moi de vous déranger moi j' ai fait le
005 spk2>	 le hum les touches pour euh RATP donc les renseignements
006 spk2>	 mais ils ont que des renseignements sur
007 spk1>	 oui
008 spk2>	 appuyer sur le A ou le B mais je voudrais les renseignements sur le RER+C
009 spk1>	 ah le C c' est la SNCF vous désirez quoi comme renseignement
010 spk1>	 un itinéraire
011 spk2>	 ben je voulais savoir juste
012 spk2>	 un départ de de train de Franconville pour aller vers euh Austerlitz
013 spk1>	 Franconville vous vers quelle heure madame
014 spk2>	 là Gare+Austerlitz
015 spk2>	 vers dix heures s'il+vous+plaît
016 spk1>	 vers dix heures un instant s'il+vous+plaît
017 spk2>	 merci beaucoup
018 spk1>	 s'il+vous+plaît bé écoutez vers dix heures vous avez neuf heures cinquante-cinq
019 spk2>	 oui
020 spk1>	 ou dix heures dix à Franconville
021 spk2>	 d'accord et qui
022 spk2>	 va à Austerlitz
023 spk1>	 qui va à Austerlitz tout+à+fait
024 spk2>	 je vous remercie beaucoup madame merci au+revoir
025 spk1>	 bonne journée madame au+revoir

