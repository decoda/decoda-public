
DIALOG=20091112_RATP_SCD_0522

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour euh NNAAMMEE je vous appelle parce+que je j' ai besoin de me rendre à Arcueil demain
003 spk1>	 oui
004 spk2>	 très tôt le matin
005 spk2>	 je voulais savoir s' il y aura encore des grèves demain
006 spk1>	 d'accord donc là vous souhaitez connaître l' état du trafic euh du RER
007 spk1>	 B pour
008 spk2>	 demain
009 spk1>	 la journée de demain
010 spk2>	 voilà
011 spk1>	 c' est ça
012 spk1>	 vous
013 spk2>	 ouais
014 spk1>	 patientez un instant je m' informe
015 spk1>	 madame
016 spk2>	 oui
017 spk2>	 je ne quitte pas
018 spk1>	 madame
019 spk2>	 oui
020 spk1>	 pour la
021 spk1>	 journée de demain hein nous n' avons pas plus de précision hein si la grève sera poursuivie ou pas
022 spk2>	 ben il y a euh normalement il y a des préavis qui sont déposés à l' avance quand+même
023 spk1>	 ah mais là étant+donné+que il s' agit d' une euh grève illimitée euh honnêtement je ne saurais vous dire pour la journée de demain pour l' instant on n' a pas d' information à ce à ce sujet
024 spk2>	 d'accord
025 spk1>	 et ou+alors il faudrait renouveler
026 spk2>	 quelle heure
027 spk1>	 votre appel un+peu plus+tard je dirais réessayez vers euh vingt heures si vous pouvez
028 spk2>	 vers vingt heures il y aura quelqu'un qui peut répondre
029 spk1>	 par+exemple hein je pense
030 spk1>	 ah oui bien+sûr notre service reste ouvert jusqu'à vingt+et+une heures hein madame
031 spk2>	 d'accord donc je rappellerai ce soir et sinon euh dans le cas où la les grèves sont prolongées je pour demain
032 spk1>	 oui
033 spk2>	 après+tout il y a ça m' étonnerait pas que ça se termine euh dimanche quoi euh est-ce+que il y a des bus qui sont prévus ou quoi
034 spk1>	 pour l' instant c' est pareil hein on n' a pas plus d' information à ce niveau là hein
035 spk1>	 est-ce+qu' il y aura un bus
036 spk2>	 en attendant il y a pas il y a pas
037 spk1>	 de remplacement ou pas parce+que là actuellement c' est pas prévu hein
038 spk2>	 c' est pas prévu donc et vous maintenez que il y a quand+même trois trains sur quatre
039 spk1>	 exactement trois trains sur quatre hein en heures de pointe
040 spk2>	 en heures de pointe oui ben le le matin euh sept heures et demie c' est une heure de pointe pour vous ou pas
041 spk1>	 sept heures et demie on va dire c' est limite oui
042 spk1>	 oui oui oui
043 spk2>	 limite
044 spk1>	 oui c' est limite hein ça serait plutôt à+partir+de huit heures hein
045 spk2>	 mouais je peux pas me permettre d' arriver en retard j' ai un examen
046 spk1>	 euh
047 spk1>	 ah oui effectivement donc là il faudrait
048 spk2>	 euh ouais est-ce+que
049 spk1>	 vraiment euh prévoir un+peu plus de temps hein être large hein on ne sait
050 spk1>	 jamais
051 spk2>	 ouais
052 spk2>	 vo() est-ce+que vous pouvez me dire euh ce ce matin nombre de RER qui avait
053 spk1>	 le nombre de RER euh
054 spk1>	 honnêtement je vais pas pouvoir vous indiquer
055 spk1>	 cette information
056 spk2>	 vous pouvez pas
057 spk1>	 hein hm
058 spk2>	 bon+ben
059 spk1>	 parce+que le trafic
060 spk1>	 il reste très perturbé depuis ce matin
061 spk1>	 hm
062 spk2>	 ouais bon+ben je pense que il y a je vais rappeler ce soir vers les vingt heures
063 spk1>	 dans+tous+les+cas on pourra vous renseigner davantage je pense que d'+ici+là on aura peut-être plus d' information
064 spk2>	 OK très+bien ben je rappellerai plus+tard merci
065 spk1>	 bonne journée madame
066 spk1>	 au+revoir
067 spk2>	 au+revoir

