
DIALOG=20091112_RATP_SCD_0764

001 spk1>	 allo
002 spk1>	 NNAAMMEE va vous répondre
003 spk2>	 NNAAMMEE bonjour
004 spk3>	 allo bonjour excusez -moi de vous déranger
005 spk3>	 je voudrais savoir est-ce+que il y a des des grèves sur le bus cent+quarante-huit et sur le s() trois+cent+vingt-deux
006 spk3>	 et est-ce+qu' il y a des grèves grèves surprises aujourd'hui
007 spk2>	 alors vous voulez savoir s' il y a des grèves sur le bus cent+quarante-huit ou le trois+cent+vingt-deux
008 spk3>	 oui
009 spk2>	 pour aujourd'hui alors
010 spk3>	 euh sur les
011 spk3>	 deux lignes là
012 spk2>	 d'accord je vais m' informer monsieur un instant
013 spk2>	 s'il+vous+plaît
014 spk3>	 merci
015 spk2>	 monsieur s'il+vous+plaît
016 spk2>	 écoutez
017 spk3>	 bonjour oui
018 spk2>	 c' est la même personne hein non moi je n' ai pas d' avis de grève euh
019 spk3>	 il y a pas d' avis de grèves
020 spk2>	 sur le
021 spk2>	 non non du+tout
022 spk3>	 euh merci merci parce+que j' ai un handicap qui voyage sur cette ligne là
023 spk2>	 oui
024 spk3>	 et merci merci de l' information
025 spk2>	 à votre service monsieur
026 spk2>	 bonne fin de journée
027 spk3>	 allez
028 spk2>	 au+revoir
029 spk3>	 merci à vous aussi
030 spk2>	 merci
031 spk3>	 au+revoir
032 spk3>	 merci

