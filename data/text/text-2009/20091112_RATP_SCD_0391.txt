
DIALOG=20091112_RATP_SCD_0391

001 spk1>	 allo
002 spk2>	 NNAAMMEE bonjour
003 spk1>	 bonjour ça serait pour euh pour une contestation s'il+vous+plaît
004 spk2>	 oui
005 spk1>	 euh j' ai reçu hier un un procès-verbal euh d' infraction
006 spk2>	 oui
007 spk1>	 parce+que j' allais euh j' allais chercher euh j' allais chercher mon sac chez un ami j' avais oublié mon sac hier soir chez un ami
008 spk1>	 j' allais le chercher j' avais la
009 spk2>	 hm
010 spk1>	 la carte Famille+Nombreuse dedans
011 spk2>	 oui
012 spk1>	 euh donc euh j' ai j' ai mis le ticket euh tarif réduit je me suis fait contrôler j' avais pas la carte euh Famille+Nombreuse alors j' ai été j' ai été la chercher hier soir c' est bon je l' ai récupérée ça serait pour savoir si je pouvais contester j' avais mis le ticket mais c' est juste que j' avais pas ma carte Famille+Nombreuse
013 spk2>	 oui pour justifier c' est ça
014 spk1>	 ouais je peux faire une photocopie il n' y a pas de souci
015 spk2>	 d'accord
016 spk1>	 d'accord alors euh un instant s'il+vous+plaît
017 spk2>	 oui
018 spk2>	 monsieur
019 spk1>	 oui
020 spk2>	 vous avez euh réglé le PV ou pas
021 spk1>	 euh non je n' avais pas d' argent sur moi
022 spk2>	 d'accord donc il n' y a pas de souci donc euh vous allez vous déplacer euh rue Belliard au trente-et-un rue Belliard avec votre carte de Famille+Nombreuse
023 spk1>	 euh c' est c' est écrit sur le euh le l' adresse sur le euh procès-verbal
024 spk2>	 au dos du PV euh normalement oui mais euh je vais peut-être vous redonner quand+même
025 spk1>	 ouais je vais
026 spk2>	 on ne sait jamais
027 spk1>	 je vais euh noter alors oui vous avez dit
028 spk2>	 alors c' est le service après verbalisation
029 spk1>	 service
030 spk2>	 après verbalisation verbalisation excusez -moi
031 spk1>	 oui
032 spk2>	 c' est trente-et-un rue Belliard dans le dix-huitième
033 spk1>	 c' est le trente euh trente+et+un
034 spk2>	 rue Belliard B E deux L I A R D dans le dix-huitième à Paris
035 spk1>	 oui
036 spk2>	 c' est au métro Porte+de+Clignancourt
037 spk1>	 métro Porte de Clignancourt
038 spk2>	 alors du lundi au vendredi c' est ouvert de huit heures trente à dix-neuf heures
039 spk2>	 c' est plus rapide hein de vous de vous déplacer hein donc là vous avez une petite semaine bon vous le faites dès maintenant c' est bien
040 spk1>	 huit heures trente dix-neuf heures
041 spk2>	 oui huit heures trente dix-neuf heures
042 spk1>	 OK ouais parce+que je l' ai eu je l' ai eu hier ils m' ont conseillé de d' appeler aujourd'hui
043 spk2>	 oui tout+à+fait
044 spk1>	 avant avant+que ça aille euh je ne sais plus où ils m' ont dit
045 spk2>	 tout+à+fait mais+alors par+contre vous vous déplacez et+puis vous réclamez votre mesure de d' indulgence vous oubliez pas votre carte Famille+Nombreuse
046 spk1>	 ouais
047 spk2>	 et tout devrait rentrer dans l' ordre
048 spk1>	 OK bah je vous remercie euh vous pensez que ça va prendre combien+de temps là-bas
049 spk2>	 ah je ne pense qu' il y en ait pour longtemps hein il ne faut pas arriver à dix-neuf heures si euh
050 spk1>	 ouais enfin non si j' arrive cet après-midi début d' après-midi
051 spk2>	 un quart d' heure vingt minutes maxi
052 spk1>	 ah OK
053 spk2>	 voilà
054 spk1>	 OK bah je vous remercie en+tout+cas
055 spk2>	 bah je vous en prie monsieur
056 spk1>	 merci au+revoir
057 spk2>	 je vous souhaite une bonne journée au+revoir

