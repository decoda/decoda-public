
DIALOG=20091112_RATP_SCD_0155

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour je téléphone pour savoir si le trafic est rétabli sur la ligne numéro treize du du
003 spk1>	 non il est toujours perturbé
004 spk1>	 madame
005 spk2>	 mais à euh de de quelle façon
006 spk2>	 énormément ou
007 spk1>	 oh euh
008 spk1>	 c' est une panne de matériel donc euh si vous voulez
009 spk2>	 non mais ça ne fonctionne pas+du+tout du+tout
010 spk2>	 ça veut dire
011 spk1>	 ça marche ça marche
012 spk2>	 ah ça marche
013 spk2>	 mais combien combien+de métro
014 spk1>	 ça marche si vous voulez euh
015 spk1>	 c' est progressif
016 spk1>	 donc là j' ai j' ai pas plus de détails par+contre
017 spk2>	 non mais je veux dire que si j' arrive euh dans une station je peux prendre un
018 spk2>	 un métro
019 spk1>	 oui le le
020 spk1>	 métro fonctionne
021 spk2>	 ah+bon très+bien très+bien le métro fonctionne donc on peut aller d' un endroit à un
022 spk2>	 autre
023 spk1>	 oui quand+même quand+même
024 spk2>	 non mais je croyais que c' était au euh de la façon dont c' était écrit sur le site on je je pensais qu' il y avait pas+du+tout
025 spk2>	 de métro quoi
026 spk1>	 non non
027 spk1>	 perturbé euh ça roule hein
028 spk2>	 bon ça roule quand+même
029 spk1>	 hm
030 spk2>	 très+bien je vous remercie
031 spk2>	 madame
032 spk1>	 je vous en
033 spk1>	 prie bonne journée au+revoir
034 spk2>	 bonne journée merci au+revoir

