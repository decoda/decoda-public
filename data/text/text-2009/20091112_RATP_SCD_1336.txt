
DIALOG=20091112_RATP_SCD_1336

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 euh bonjour madame excusez -moi de vous déranger
004 spk3>	 j' ai des gros problèmes je suis sur la ligne B du RER
005 spk2>	 oui
006 spk3>	 bon et comme vous savez il y a bien des métros mais ils sont toujours pleins et on peut pas les avoir il y a rien à faire
007 spk2>	 oui
008 spk3>	 alors euh qu'est-ce+que vous me conseillez je veux aller je veux pas mettre non+plus trois heures pour aller dans Paris hein c' est c' est vous êtes vraiment euh incroyables hein bouh il y en a marre de tout ça il y en a marre
009 spk2>	 donc là vous êtes à quelle station madame
010 spk3>	 bah oui là je suis à Laplace
011 spk2>	 à Laplace donc vous voulez vous allez vous voulez revenir sur Paris mais dans+ce+cas-là pas avec le RER avec un bus
012 spk3>	 bah ou+alors j' ai le trois+cent+vingt-trois mais ça va m' amener euh ou ça va m' amener Porte+de+Châtillon mais euh euh pas Porte+de+Châtillon ça va m' amener euh
013 spk3>	 le trois+cent+vingt-trois ça va m' amener euh Châtillon-Montrouge
014 spk3>	 bon Châtillon-Montrouge euh qu'est-ce+que je peux prendre pour aller à au métro Couronnes
015 spk3>	 ligne deux à la Gare+du+Nord
016 spk2>	 alors quand vous êtes à Châtillon avec le euh bus trois cent vingt-trois
017 spk3>	 oui
018 spk2>	 eh+bah alors maintenant vous voulez aller jusqu'à la Gare+du+Nord
019 spk3>	 bah euh oui
020 spk2>	 alors bah je vais regarder
021 spk3>	 bah oui parce+que
022 spk2>	 madame un instant s'il+vous+plaît
023 spk2>	 madame s'il+vous+plaît
024 spk3>	 oui
025 spk2>	 alors bah ce+que vous pourriez faire c' est dans+ce+cas-là à Châtillon prendre le bus cent quatre-vingt-quatorze en direction et jusqu'à Porte+d'Orléans et à Porte+d'Orléans vous récupérez le métro ligne quatre direction euh enfin Porte+d'Orléans direction Porte+de+Clignancourt et vous allez jusqu'à la Gare+du+Nord
026 spk3>	 ah oui on
027 spk2>	 vous voyez là ça sera plus rapide parce+que en bus ça va vous faire trop long
028 spk3>	 peut faire comme+ça aussi oui ça serait ouais non ou+alors à+ce+moment-là
029 spk3>	 je vais euh je vais jusqu'à euh La+Vache+Noire et de La+Vache+Noire je prends le cent+quatre-vingt-sept ou le cent+quatre-vingt-huit je vais jusqu'à la Porte+d'Orléans
030 spk2>	 exactement et après le métro
031 spk3>	 oui
032 spk2>	 euh
033 spk2>	 ligne quatre
034 spk3>	 non mais c' est vrai que c' est pas drôle
035 spk2>	 oui ah non non
036 spk3>	 quand+même hein
037 spk2>	 mais je vous comprends madame
038 spk3>	 c' est pas drôle
039 spk2>	 oui oui tout+à+fait
040 spk3>	 je sais bien que vous n' y êtes
041 spk2>	 hm
042 spk3>	 pour rien
043 spk3>	 mais c' est
044 spk2>	 oui
045 spk3>	 vraiment
046 spk3>	 pas drôle hein
047 spk2>	 oui oui non non vous en faites pas
048 spk3>	 mais ça peut durer encore jusque combien+de temps
049 spk2>	 bah disons que c' est une grève illimitée et voyez nous à l' heure actuelle on a même les informations pour demain si c' est reconduit
050 spk3>	 et euh on le saura
051 spk2>	 donc euh
052 spk3>	 demain matin si c' est reconduit
053 spk2>	 ah oui
054 spk3>	 ou pas
055 spk2>	 bah oui tout+à+fait oui oui
056 spk3>	 mais dans quoi comment
057 spk2>	 mais là on attend on attend
058 spk2>	 on attend les informations concernant la direction si ils ont des euh un communiqué à nous donner pour le moment on a rien
059 spk3>	 il faut vous appeler ou on appelle ou on
060 spk2>	 bah
061 spk3>	 écoute
062 spk3>	 à la radio
063 spk2>	 euh bah
064 spk3>	 même à la radio
065 spk2>	 si vous voulez
066 spk3>	 ils nous disent rien
067 spk2>	 oui ou même sur le site nous si on a quelque+chose vous
068 spk3>	 moi j' ai pas internet
069 spk2>	 pouvez
070 spk2>	 alors dans+ce+cas-là vous pouvez nous appeler à ce numéro et on vous dira nous les informations que nous avons madame
071 spk3>	 ah oui si+jamais
072 spk2>	 hm
073 spk3>	 je vous ai
074 spk3>	 parce+que ça doit être difficile le matin
075 spk2>	 ah oui
076 spk3>	 en+plus
077 spk2>	 oui oui
078 spk3>	 oh aïe aïe aïe aïe aïe aïe bon moi je vais faire comme+ça je vais aller jusqu'à la Gare+du+Nord et de Gare+du+Nord je change et je prends la ligne deux
079 spk2>	 hm
080 spk3>	 ça va me durer une heure et demie bon bien
081 spk3>	 merci beaucoup
082 spk3>	 madame
083 spk2>	 mais je vous en prie
084 spk2>	 madame
085 spk3>	 au+revoir madame
086 spk2>	 bonne soirée madame
087 spk2>	 au+revoir

