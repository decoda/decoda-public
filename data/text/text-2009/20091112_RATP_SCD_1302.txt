
DIALOG=20091112_RATP_SCD_1302

001 spk1>	 NNAAMMEE bonsoir
002 spk2>	 euh oui bonsoir
003 spk2>	 euh je vous appelle en+fait c' était pour savoir si euh il allait il allait encore avoir une grève euh demain sur le B
004 spk1>	 on n' a pas+encore de communiqué
005 spk1>	 euh la direction ne
006 spk2>	 vous n' avez pas de communiqué
007 spk1>	 nous a rien dit encore
008 spk2>	 mais vous pensez que c' est possible ou pas
009 spk1>	 s() comme elle vu+qu' elle était illimitée il se peut qu' elle soit reconduite mais pour l' instant on sait pas
010 spk2>	 OK et vous pensez qu' elle va être reconduite aussi
011 spk2>	 semaine prochaine
012 spk1>	 ça je peux pas vous je peux pas parler pour eux je sais pas+du+tout
013 spk2>	 d'accord
014 spk2>	 euh
015 spk1>	 moi ce+que je peux
016 spk1>	 vous prenez le RER demain non
017 spk2>	 ben heureusement que
018 spk1>	 moi je vous inviterais à regarder
019 spk1>	 si vous avez internet ce soir hein
020 spk2>	 ben c' est ce+que je fais mais bon
021 spk2>	 il y a pas juste il y des infos pour aujourd'hui quoi mais
022 spk1>	 ouais non parce+que ben non nous euh nous la direction ne
023 spk1>	 ne nous a rien communiqué non+plus
024 spk2>	 d'accord
025 spk2>	 et je
026 spk1>	 hm
027 spk2>	 voulais savoir
028 spk2>	 là paraît il
029 spk1>	 oui
030 spk2>	 que il va y avoir des remboursements
031 spk1>	 oui alors ça pour l' instant c' est en négociation tant+que la grève est pas finie on saura pas et dès+que ça sera euh fini que il y aura eu une négo() une négociation de faite on on en sau() on saura euh le tarif du remboursement
032 spk1>	 pour l' instant il y a rien
033 spk2>	 ah d'accord
034 spk1>	 euh pour l' instant c' est que des
035 spk1>	 que des
036 spk1>	 on-dit hein pour l' instant il y a rien de fait encore
037 spk2>	 ah oui d'accord
038 spk1>	 oui
039 spk2>	 OK
040 spk1>	 voilà il faut attendre que tout soit euh mis à jour pour euh pour ça
041 spk2>	 et on devrait le savoir quand ça
042 spk1>	 peut-être la semaine prochaine
043 spk2>	 d'accord
044 spk1>	 hm
045 spk1>	 mais pour l' instant il y a rien non+plus
046 spk2>	 OK
047 spk1>	 voilà
048 spk1>	 je vous en
049 spk2>	 bon+ben merci
050 spk1>	 prie bonne soirée au
051 spk1>	 revoir
052 spk2>	 au
053 spk2>	 revoir

