
DIALOG=20091112_RATP_SCD_0540

001 spk2>	 merci
002 spk1>	 NNAAMMEE bonjour bonjour madame
003 spk2>	 oui bonjour madame je voudrais un renseignement s'il+vous+plaît
004 spk1>	 oui
005 spk2>	 alors en faisant du range du rangement j' ai retrouvé des des tickets de métro euh il y a marqué euh bus RER ils sont verts est-ce+qu' ils bénéficient toujours valables
006 spk1>	 ils sont toujours valables par+contre ils risquent d' être démagnétisés hein n' ayant pas servi
007 spk2>	 ah oui parce+que ça fait longtemps
008 spk1>	 depuis
009 spk2>	 que je les ai
010 spk2>	 que je les ai retrouvés
011 spk1>	 si+jamais vous avez un souci au guichet vous allez les vous les faites échanger
012 spk1>	 hein ah ils sont
013 spk2>	 on peut les faire échanger mais ils sont ils sont verts ils sont
014 spk1>	 toujours valables il y a aucune date dessus mais elle est c' est possible qu' ils ne fonctionnent plus hein
015 spk2>	 d'accord donc
016 spk2>	 je peux les échanger de+toute+façon contre euh
017 spk1>	 hm tout+à+fait tout+à+fait
018 spk2>	 bon et sinon un ticket de métro à l' heure actuelle euh ça coûte combien
019 spk1>	 ben un ticket euh c' est un euro soixante
020 spk2>	 un euro soixante d'accord OK bon+ben enfin de+toute+façon je je les garde je les jette pas
021 spk2>	 je peux quand+même comme j' ai
022 spk1>	 ah oui oui tout+à+fait
023 spk2>	 comme je vais à Paris le mois prochain je pourrai les faire échanger
024 spk1>	 tout
025 spk2>	 pour
026 spk2>	 m' en servir
027 spk1>	 tout+à+fait oui oui
028 spk2>	 eh eh+ben merci de votre accueil
029 spk2>	 madame au+revoir
030 spk1>	 merci madame bonne journée au+revoir

