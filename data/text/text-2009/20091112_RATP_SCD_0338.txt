
DIALOG=20091112_RATP_SCD_0338

001 spk1>	 service d' accueil VGC bonjour
002 spk2>	 bonjour
003 spk1>	 bonjour madame
004 spk2>	 euh NNAAMMEE INGSVO une petite question pour les renouvellements de carte euh ayant-droit
005 spk1>	 oui
006 spk2>	 euh il faut utiliser le formulaire classique carte de famille mais est-ce+qu' il faut remettre une photo
007 spk1>	 ah bonne question c' est quelqu'un euh oui c' est un une carte pour les Famille+d'Agent c' est ça
008 spk2>	 voilà c' est ça oui
009 spk1>	 d'accord et vous êtes quel service vous
010 spk2>	 euh INGSVO
011 spk1>	 INGSVO alors c' est quel service ça c' est ingénierie c' est ça
012 spk2>	 ingénierie voilà
013 spk1>	 ah oui d'accord alors bonne question normalement euh alors ce+que je vais faire parce+que là vous êtes sur l' accueil du service VGC
014 spk2>	 ah d'accord
015 spk1>	 euh je vais tenter d' avoir la la réponse auprès d' un d' un agent qui est chargé des hein de la fabrication des titres de transport hein
016 spk2>	 d'accord
017 spk1>	 en+fait vous avez le formulaire c' est ça
018 spk2>	 voilà
019 spk1>	 et vous voulez savoir si euh il faut remettre une photo
020 spk2>	 oui parce+que
021 spk1>	 pour faire la demande
022 spk2>	 comme pour les agents y a déjà la photo et ils la scannent
023 spk1>	 oui
024 spk2>	 je sais pas s' ils font pareil pour les les ayant-droit
025 spk1>	 pour les ayant-droit
026 spk2>	 hm
027 spk1>	 ouais c' est un ro() un re() un renouvellement c' est ça
028 spk2>	 oui voilà
029 spk1>	 d'accord je vais poser la question quand+même quittez pas hein
030 spk2>	 merci
031 spk1>	 merci+bien
032 spk1>	 s'il+vous+plaît
033 spk2>	 oui
034 spk1>	 je vous remercie d' avoir patienté alors je n' ai personne au service ils sont en contact clientèle moi je sais que pour mon ami euh personnellement euh je sais que j' avais pas remis de photo auprès+de mon attachement
035 spk2>	 hm+hm
036 spk1>	 euh donc ils avaient récupéré la
037 spk2>	 l' ancienne
038 spk1>	 la carte
039 spk2>	 puis ils ont scanné
040 spk1>	 oui ils ont scanné euh bon alors normalement je pense pas que ça puisse poser de problème je vous aurais dit quand+même de joindre une photo au+cas+où
041 spk2>	 mais bon au pire
042 spk1>	 hein
043 spk2>	 hein bon dans le bénéfice du doute ils joignent ils joignent la photo
044 spk1>	 voilà
045 spk2>	 et+puis comme elle est retournée
046 spk1>	 voilà voilà exactement hein de+toute+façon s' ils en ont pas besoin à la limite ils hein ils peuvent éventuellement la retourner euh
047 spk2>	 et c' est le même formulaire hein ils ont pas
048 spk1>	 oui
049 spk2>	 de formulaire pour deux renouvellements
050 spk1>	 non non non non
051 spk2>	 que le formulaire carte de famille
052 spk1>	 tout+à+fait exactement
053 spk2>	 d'accord bah je vous remercie
054 spk1>	 je vous en prie
055 spk2>	 je vous souhaite une bonne journée
056 spk1>	 merci vous de+même au+revoir
057 spk2>	 au+revoir
058 spk1>	 au+revoir

