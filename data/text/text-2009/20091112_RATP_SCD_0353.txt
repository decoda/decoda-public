
DIALOG=20091112_RATP_SCD_0353

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 bonjour madame alors je sais pas si c' est le bon numéro mais euh voilà en+fait euh ma directrice vient de prendre le les transports là le bus soixante euh soixante-huit oui
003 spk1>	 oui
004 spk2>	 et elle a oublié son ordinateur portable euh dans le bus sûrement c' est dans ce bus là
005 spk1>	 et y a combien+de temps
006 spk2>	 euh là tout+de+suite là y a à+peine euh une demi-heure
007 spk1>	 vous savez où elle est descendue
008 spk2>	 euh elle est descendue à Pyramide
009 spk1>	 d'accord il allait dans quelle direction
010 spk2>	 direction euh enfin direction Paris il me semble que c' est euh bah le contraire de Clichy l'autre
011 spk1>	 l' autre côté d'accord bah je vais regarder donc c' est sur le soixante-huit un ordinateur de quelle marque vous savez
012 spk2>	 euh je sais pas+du+tout mais voilà il était dans une housse et il est assez grand
013 spk1>	 d'accord bah je vais me renseigner vous patientez un instant j' appelle le soixante-huit
014 spk2>	 d'accord merci
015 spk3>	 Châtillon bonjour
016 spk1>	 bonjour Châtillon Service+Clientèle+à+Distance
017 spk3>	 oui
018 spk1>	 j' ai une dame en ligne qui a laissé son ordinateur dans le soixante-huit
019 spk3>	 ah un ordinateur portable on a récupéré ça
020 spk1>	 comment
021 spk3>	 ouais on l' a
022 spk1>	 vous l' avez magnifique il arrivera quand à Place+de+Clichy ou à moins qui soit déjà arrivé non
023 spk3>	 à la place de Clichy euh ouais mais je suis on est pas d'accord pour lui rendre
024 spk1>	 ah+bon
025 spk3>	 non non mais de+toute+façon il est dans sur Châtillon là pour nous
026 spk1>	 ah oui direction Châtillon effectivement
027 spk3>	 ouais donc faut que la dame vienne à Châtillon pour le récupérer hein
028 spk1>	 d'accord jusqu'à quelle heure
029 spk3>	 ah+bah dix-huit heures trente grand maxi parce+qu' après il est embarqué et+puis il est aux objets trouvé hein
030 spk1>	 d'accord
031 spk3>	 d'accord
032 spk1>	 OK bon+bah je vais lui dire je te remercie beaucoup
033 spk3>	 merci
034 spk1>	 au+revoir
035 spk3>	 salut
036 spk1>	 s'il+vous+plaît
037 spk2>	 oui allo
038 spk1>	 oui alors il a été retrouvé
039 spk2>	 d'accord
040 spk1>	 il va falloir que cette personne aille avant dix-huit heures quinze à Châtillon-Montrouge
041 spk2>	 oui
042 spk1>	 au terminus avec une pièce d' identité
043 spk2>	 d'accord OK donc le chauffeur l' a retrouvé
044 spk1>	 oui
045 spk2>	 OK
046 spk1>	 d'accord
047 spk2>	 donc il était il était bien dans le bus
048 spk1>	 oui
049 spk2>	 d'accord bah je vous remercie madame
050 spk1>	 je vous en prie bonne journée
051 spk2>	 au+revoir

