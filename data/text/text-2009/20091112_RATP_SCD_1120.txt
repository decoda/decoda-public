
DIALOG=20091112_RATP_SCD_1120

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 bonjour mademoiselle
004 spk3>	 pour aller de Damrémont-Championnet au au cent+cinq boulevard Malesherbes quel est le+plus court
005 spk2>	 alors vous partez donc de la station
006 spk3>	 Damrémont Championnet
007 spk2>	 Damrémont Championnet
008 spk3>	 la place que est à la porte Montmartre
009 spk2>	 hm d'accord
010 spk3>	 et je sais que le cent+cinq boulevard Malesherbes j' ai regardé sur le plan c' est à la hauteur de Villiers c' est pas bien pratique
011 spk2>	 d'accord
012 spk3>	 c' est à Villiers
013 spk2>	 d'accord mais
014 spk3>	 il est
015 spk2>	 vous vous
016 spk3>	 grand le boulevard Malesherbes mais là le cent+cinq il n' y a rien à faire
017 spk2>	 vous voulez le faire ce déplacement en bus
018 spk3>	 bah qu'est-ce+qu' il y a rien d' autre
019 spk2>	 alors je regarder
020 spk3>	 vous savez
021 spk3>	 où on est nous on est pas gâté on a qu' un ou deux bus et on attend longtemps euh on sait pas quand on il y a les métros qui doublent les bus c' est bien
022 spk2>	 alors je vais regarder un instant
023 spk3>	 merci beaucoup
024 spk2>	 s'il+vous+plaît
025 spk2>	 s'il+vous+plaît
026 spk3>	 oui
027 spk2>	 donc là je vois que vous pourriez prendre deux bus pour vous
028 spk3>	 ah oui
029 spk2>	 rendre
030 spk2>	 donc au cent+cinq boulevard Malesherbes
031 spk3>	 oui
032 spk2>	 il faudrait partir avec en premier avec le bus quatre-vingt-quinze
033 spk3>	 oui voilà qui va à la place Clichy
034 spk2>	 exactement donc l' arrêt Place+Clichy-Caulaincourt
035 spk3>	 ouais
036 spk2>	 et là
037 spk2>	 vous allez pouvoir récupérer le bus trente
038 spk3>	 et le trente il est sur la place au talon
039 spk2>	 Place+de+Clichy
040 spk3>	 dune petite rue
041 spk3>	 je crois
042 spk2>	 hm+hm
043 spk3>	 c' est ça hein
044 spk2>	 il
045 spk2>	 tout+à+fait
046 spk3>	 bon l' impasse juste avant le boulevard des Batignolles
047 spk2>	 et là vous le prenez et vous allez jusqu' donc le
048 spk3>	 jusqu'à Villiers
049 spk2>	 prendre
050 spk2>	 jusqu'à Malesherbes non moi j' ai Malesherbes-Courcelles
051 spk3>	 ah+bon
052 spk2>	 il paraît
053 spk2>	 oui
054 spk3>	 l' arrêt alors arrêt Malesherbes alors le trente arrêt Malesherbes parce+que c' est pas pour moi c' est une personne qu' est pas habituée alors
055 spk2>	 oui
056 spk3>	 moi je me débrouille je fais Paris toute+la journée je m' en fous moi
057 spk2>	 hm+hm
058 spk3>	 Malesherbes
059 spk2>	 Courcelles
060 spk3>	 Courcelles ouais c' est au après Villiers alors
061 spk2>	 oui ça sera juste à l' angle comme+ça
062 spk3>	 ah+bon d'accord bon+bah Malesherbes-Courcelles et on arrive là et dans l' autre sens et bien on revient en face et on revient ici
063 spk3>	 c' est ça hein
064 spk2>	 alors bah je vais regarder dans l' autre sens dans ce
065 spk3>	 oh dans l' autre sens c' est pareil hein
066 spk2>	 cas là si vous avez des bus
067 spk2>	 euh parce+que des+fois vous pouvez avoir des lignes de bus différentes hein
068 spk3>	 oui mais là ça m' étonnerait hein
069 spk2>	 alors attendez je vais regarder un instant
070 spk3>	 merci
071 spk2>	 s'il+vous+plaît
072 spk3>	 oui
073 spk2>	 ah vous voyez dans l' autre sens non vous pourrez pas prendre ce bus il faudra prendre le quatre-vingt-quatorze
074 spk3>	 ah+bon il va pas
075 spk2>	 oui
076 spk3>	 le euh
077 spk3>	 trente il passe pas là
078 spk2>	 bah écoutez il a pas le même itinéraire
079 spk3>	 ah mais il a été changé alors
080 spk2>	 hm+hm
081 spk3>	 avant on prenait le trente il
082 spk2>	 en direction de oui
083 spk3>	 passait devant le euh devant pas loin+de Courcelles
084 spk2>	 oui
085 spk3>	 ah+bon
086 spk2>	 mais non
087 spk2>	 il a pas le même itinéraire dans l' autre
088 spk3>	 alors il est
089 spk2>	 sens
090 spk3>	 alors en face c' est sur le même de+même au même endroit en face il y a le quatre-vingt-quatorze
091 spk2>	 c' est le quatre-vingt-quatorze que vous prenez en direction de Gare+Montparnasse
092 spk3>	 ah+bon faut
093 spk2>	 oui
094 spk3>	 oui
095 spk3>	 il faut en direction il revient à la place Clichy celui-là là
096 spk2>	 vous allez jusqu'à la Gare+Saint-Lazare et vous récupérez le quatre-vingt-quinze
097 spk3>	 ah+bon alors quatre-vingt-quatorze direction euh
098 spk2>	 Montparnasse
099 spk3>	 gare euh
100 spk3>	 Montparnasse c' est rigolo ça on on descend en+bas
101 spk2>	 hm
102 spk3>	 oh dis donc avant le trente il passait tout+le long tout+le long tout+le long hein je l' ai déjà pris vingt fois trente fois c' est ma ville ici vous savez
103 spk2>	 hm+hm
104 spk3>	 j' y suis né hein
105 spk3>	 alors direction Montparnasse et là on récupère le quatre-vingt-quinze
106 spk2>	 oui
107 spk3>	 qui s' arrête à Saint-Lazare c' est ça
108 spk2>	 oui tout+à+fait
109 spk3>	 bon+bah ça va aller c' est surtout au retour que c' est emmerdant mais à l' aller ça va aller
110 spk3>	 bon+bah je vais me débrouiller avec elle
111 spk2>	 hm
112 spk3>	 je lui expliquerai
113 spk3>	 merci hein
114 spk2>	 mais je vous en prie
115 spk3>	 allez merci au+revoir madame au+revoir
116 spk2>	 bonne fin de journée au+revoir

