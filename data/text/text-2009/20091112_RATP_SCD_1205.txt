
DIALOG=20091112_RATP_SCD_1205

001 spk1>	 NNAAMMEE bonjour je vous écoute
002 spk2>	 allo
003 spk1>	 oui bonjour monsieur
004 spk2>	 oui bonjour madame je m' excuse ou mademoiselle excusez -moi oui je voudrais demander un renseignement
005 spk1>	 oui
006 spk2>	 c' était de euh euh le métro de euh euh je je prends le métro à Bourse
007 spk1>	 oui
008 spk2>	 pour aller
009 spk2>	 parce+que je suis tout+à+fait dans euh j' habite Versailles je quand je prends le métro à Bourse pour aller à La+Défense je change bien à Opéra c' est bien ça j' ai vu ça sur le plan là
010 spk1>	 alors on va faire l' itinéraire monsieur au+départ+de Bourse pour aller à La+Défense
011 spk2>	 oui
012 spk1>	 on va regarder
013 spk1>	 hum non
014 spk1>	 non parce+que euh Bourse euh la euh à Opéra vous n' avez pas la ligne une pour aller à La+Défense ou à+moins+que vous bah non non vous voulez aller à La+Défense métro ou RER
015 spk2>	 bah c'est-à-dire que j' ai j' ai un rendez-vous à enfin une personne qui vient me chercher à la grande Arche où c' est
016 spk1>	 oui
017 spk2>	 qu' il y a
018 spk2>	 un grand restaurant McDonald's
019 spk2>	 et euh donc c' est euh euh il m' avait dit euh il m' avait dit comme+ça vous vous d' essayer prendre euh de changer à Opéra prendre le comment le
020 spk1>	 ah oui bah à+ce+moment-là avec le RER
021 spk2>	 oui
022 spk1>	 oui
023 spk2>	 le RER
024 spk1>	 oui c' est possible hm
025 spk2>	 ah+bon
026 spk1>	 en prenant le RER oui oui c' est possible il y a pas de souci
027 spk2>	 mais euh il y a euh il faut prendre un ticket comment ça se passe euh
028 spk1>	 alors il faut prendre un billet au+départ+de Bourse pour La+Défense RER et ça vous coûtera deux euros vingt-cinq
029 spk2>	 ah+bon d'accord alors
030 spk1>	 hm
031 spk2>	 il faut
032 spk2>	 demander ça au à la caisse alors
033 spk1>	 tout+à+fait monsieur
034 spk2>	 ah+bon d'accord deux euros vingt-cinq
035 spk1>	 oui
036 spk2>	 oui oui
037 spk2>	 bah c' est c' est pas et autrement euh ça met combien+de temps pour euh
038 spk1>	 oh+bah là ça met vingt minutes hein monsieur il y en a pas pour longtemps hein c' est rapide avec le RER
039 spk2>	 bon alors vingt minutes
040 spk1>	 oui
041 spk2>	 bon+bah
042 spk2>	 c' est bon alors
043 spk1>	 oui
044 spk2>	 bon+bah je m' excuse d' avoir dérangé hein
045 spk1>	 mais il y a pas de mal monsieur à votre
046 spk2>	 ouais
047 spk1>	 service
048 spk2>	 au+revoir madame
049 spk1>	 au+revoir monsieur
050 spk2>	 merci

