
DIALOG=20091112_RATP_SCD_0863

001 spk1>	 hm NNAAMMEE bonjour
002 spk1>	 oui NNAAMMEE bonjour
003 spk2>	 oui bonjour je suis le collège Bobée à Yvetot en Seine-Maritime
004 spk1>	 oui
005 spk2>	 et on a plusieurs voyages qui se font au+cours+de l' année euh donc sur Paris pour utiliser le métro
006 spk1>	 oui
007 spk2>	 on aurait voulu deux cent tickets comment on doit p() procéder
008 spk1>	 deux cent tickets
009 spk2>	 oui
010 spk1>	 euh
011 spk1>	 vous pouvez directement peut-être faire une commande
012 spk2>	 mais oui c' est ce+qu' on voudrait faire
013 spk1>	 d'accord
014 spk1>	 deux cent tickets
015 spk2>	 oui
016 spk1>	 parce+que ça va dépendre du nombre du du tarif global
017 spk2>	 oui
018 spk1>	 ouais
019 spk1>	 enfin je fais juste un calcul hein vous pouvez
020 spk1>	 patienter merci
021 spk2>	 oui oui oui
022 spk2>	 oui oui oui
023 spk1>	 non madame le montant n' est pas assez euh important il va falloir vous rendre directement au guichet en arrivant sur place
024 spk2>	 ah oui d'accord donc mais c' est donc euh c' est pas très facile pour nous parce+que comme on enfin quand on arrive de province bon en+général descendre du train c' est toujours un+peu rapide
025 spk1>	 oui
026 spk2>	 il y a pas moyen de faire autrement
027 spk1>	 hum non par+rapport+à ce montant non à ce montant là ça sera pas possible non
028 spk2>	 et il faudrait quel montant
029 spk1>	 minimum cent cinquante euros
030 spk2>	 et là on est à combien
031 spk1>	 on est à cent seize
032 spk2>	 ah+ben je peux aller jusqu'à cent cinquante euros hein
033 spk1>	 ah d'accord
034 spk1>	 vous pouvez faire une
035 spk2>	 ah oui
036 spk1>	 commande euh supérieure
037 spk2>	 voilà
038 spk1>	 alors je vais
039 spk2>	 euh
040 spk1>	 essayer de vous mettre en relation directement avec le service
041 spk1>	 pour une euh commande
042 spk2>	 donc ça c' est une
043 spk2>	 c' est donc on a un tarif réduit pour les élèves
044 spk1>	 euh oui bien+sûr
045 spk2>	 d'accord
046 spk1>	 oui oui oui
047 spk2>	 oui oui bon c' est bon oui oui non mais hm
048 spk2>	 je peux peux je vous donnais deux cent parce+que bon euh c' est pareil j' ai pas fait un chiffre c' était juste à+peu+près ce+qu' on utilise chaque année mais sinon on les repasse sur une année d'+après et c' est tout hein
049 spk1>	 d'accord
050 spk1>	 m() donc
051 spk2>	 oui
052 spk1>	 euh vous avez jamais p()
053 spk2>	 ben si juste
054 spk1>	 passé une commande ah oui
055 spk2>	 euh commande non pas commande si vous voulez j' avais toujours quelqu'un qui allait sur Paris qui prenait
056 spk1>	 ah d'accord
057 spk2>	 donc une feuille
058 spk2>	 qu' on faisait mais maintenant ça devient un+peu plus compliqué donc
059 spk2>	 euh
060 spk1>	 ben la
061 spk1>	 feuille vous pouvez de la retirer directement
062 spk2>	 ben oui mais il
063 spk1>	 euh par+rapport+au
064 spk1>	 au transport
065 spk1>	 hein sur internet
066 spk2>	 oui mais voilà su
067 spk1>	 euh
068 spk2>	 ah
069 spk1>	 la demande de groupe de transport c' est cette feuille dont vous parlez
070 spk2>	 d() enfin je sais pas si on l' a eu sur internet moi j' avais quelqu'un qui me l' a ramené de Paris on lui je lui disais de+toute+façon jusqu'alors ça posait pas de problèmes mais là maintenant on a dit qu' il fallait qu' on fasse la photocopie de la du professeur qui les accompagnait et joindre euh la feuille par+lequel il faisait tel voyage alors+que quand je fais u() un ensemble de deux cent tickets bon+ben moi les voyages ils se font au+fur+et+à+mesure si vous voulez
071 spk1>	 d'accord je comprends euh je vais essayer de vous mettre en relation directement avec la personne
072 spk1>	 euh
073 spk2>	 d'accord
074 spk1>	 qui fait les commandes
075 spk1>	 vous pouvez patienter merci
076 spk2>	 d'accord oui
077 spk1>	 ah
078 spk3>	 vous pouvez demander à être dirigé vers la messagerie vocale de votre correspondant en composant le zéro+soixante-douze vous pouvez demander à être dirigé vers la messagerie vocale de votre correspondant en composant le zéro+soixante-douze vous pouvez demander à être dirigé vers la messagerie vocale de votre correspondant en composant le zéro+soixante-douze vous pouvez demander à être dirigé vers la messagerie vocale de votre correspondant en composant le
079 spk1>	 oui m()
080 spk1>	 madame c' est toujours moi
081 spk2>	 oui oui oui
082 spk1>	 donc la personne n' est pas disponible pour l' instant
083 spk1>	 il vous est
084 spk2>	 bon
085 spk1>	 possible de rappeler
086 spk2>	 euh euh je rappelle aujourd'hui
087 spk2>	 ou plutôt un autre jour
088 spk1>	 ou
089 spk1>	 demain matin
090 spk1>	 si vous pouvez
091 spk2>	 bon
092 spk2>	 d'accord de ben d'accord je rappellerai demain mais je refais toujours mon zéro+un
093 spk2>	 oui
094 spk1>	 cinquante-huit
095 spk1>	 euh
096 spk2>	 ou ou un
097 spk2>	 autre numéro
098 spk1>	 ou le trente-deux+quarante-six directement
099 spk2>	 attendez ah oui le trente-deux+quarante-six je l' ai fait puis on enfin il y a quelqu'un qui m' a répondu qu' il fallait que je fasse euh
100 spk2>	 trois+cent+soixante-dix-sept
101 spk1>	 ah d'accord
102 spk2>	 pour euh sa pour
103 spk1>	 alors sinon
104 spk1>	 effectivement euh zéro+un+cinquante-huit euh
105 spk2>	 ben je refais ce
106 spk1>	 soixante-dix-sept trois fois
107 spk2>	 d'accord donc je refais ça et+puis je ré() je réexplique mon petit
108 spk2>	 problème
109 spk1>	 exactement
110 spk2>	 bon+ben je vous remercie
111 spk2>	 beaucoup au+revoir monsieur merci
112 spk1>	 je vous en prie madame bonne journée au+revoir
113 spk2>	 au+revoir

