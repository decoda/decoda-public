
DIALOG=20091112_RATP_SCD_0178

001 spk1>	 NNAAMMEE bonjour
002 spk3>	 oui bonjour
003 spk3>	 voilà ma fille s' est faite contrôler dans le bus elle avait pas sa carte Imagine+R sur elle
004 spk1>	 oui
005 spk3>	 et du+coup ben elle a une euh un une contravention de soixante euros
006 spk1>	 oui
007 spk3>	 donc je voudrais savoir euh euh que est-ce+que il y a un moyen de pas payer ou
008 spk3>	 puisqu' on paie déjà déjà la carte Imagine+R par mois euh quarante et quelques euros
009 spk1>	 donc à+ce+moment-là il faut faire une réclamation madame euh à l' adresse qui est au dos du PV donc vous devez avoir normalement rue Jules+Vallès
010 spk3>	 alors rue
011 spk3>	 oui
012 spk1>	 c' était au bus ou au métro bus
013 spk3>	 au bus ce jour là elle pas pris sa carte alors moi j' ai
014 spk3>	 un ticket bleu là RATP
015 spk3>	 service de recouvrement Jules+Vallès
016 spk1>	 oui tout+à+fait
017 spk3>	 soixante-quinze+mille+onze
018 spk1>	 tout+à+fait donc il faut faire une réclamation à cette adresse là
019 spk1>	 en y a() en y mettant la photocopie
020 spk1>	 de la carte Imagine+R en en faisant une demande d' indulgence
021 spk1>	 et à+ce+moment-là vous aurez une réponse sous vingt+et+un jours
022 spk3>	 pardon
023 spk1>	 je vous en prie
024 spk3>	 d'accord
025 spk3>	 donc il faut faire une lettre avec euh
026 spk1>	 oui tout+à+fait
027 spk3>	 avec le
028 spk3>	 d'accord bah écoutez je vous remercie beaucoup
029 spk1>	 je vous en prie bonne journée madame
030 spk3>	 merci à vous aussi au+revoir
031 spk1>	 au+revoir

