
DIALOG=20091112_RATP_SCD_0351

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 euh oui bonjour madame je pour avoir une information sur un bus en+particulier
003 spk1>	 oui
004 spk2>	 qui est signalé perturbé
005 spk1>	 quel bus
006 spk2>	 c' est le cent+trente-et-un en direction de Porte+d'Italie
007 spk1>	 alors bon euh oui il est plus que perturbé puisqu' il y a un mouvement de grève dessus
008 spk2>	 ah d'accord parce+qu' il y a marqué un train sur quatre et comme+ça fait euh un train
009 spk1>	 un bus sur quatre
010 spk2>	 un bus sur quatre et comme+ça fait une heure que j' attends je voudrais être sûr qu' il y en a ou pas
011 spk1>	 euh non y a que trente pourcent des voitures qui roulent hein
012 spk2>	 d'accord
013 spk1>	 sur tout pas que sur cette ligne là sur tout+le dépôt de Vitry parce+que
014 spk2>	 d'accord
015 spk1>	 c' est le dépôt qui fait grève
016 spk2>	 ah d'accord donc vaut mieux pas attendre encore euh
017 spk1>	 non
018 spk2>	 euh ah+bah voilà OK comme+ça je suis sûr bah je vous remercie bien
019 spk1>	 je vous en prie au+revoir
020 spk2>	 bonne journée au+revoir

