
DIALOG=20091112_RATP_SCD_0563

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour madame
003 spk1>	 bonjour madame
004 spk2>	 je vous remercie de prendre mon appel
005 spk2>	 voilà euh je voulais savoir auprès+de qui je pourrais euh m' adresser à la RATP je suis agent SNCF et pour des euh euh nous avons eu un déplacement professionnel si vous voulez je travaillais avant au dix Place+de+Budapest et maintenant je suis euh avenue d' Ivry mon temps de trajet euh par+rapport+à mon domicile a augmenté beaucoup et avec l' estimation que l'on fait sur le site la personne de notre direction des ressources humaines me dit que mon temps de trajet euh est le même alors là moi je ne suis pas d'accord est-ce+que ce serait ce serait possible d' avoir une attestation ou que quelqu'un de la RATP me fasse une étude fiable que je puisse donner à mon employeur
006 spk1>	 ah les études vous pouvez imprimer vous pouvez euh imprimer sur internet
007 spk2>	 oui mais par+exemple vous voyez je suis euh où j' habite je prends la station euh le métro volontaires hein
008 spk1>	 oui oui
009 spk2>	 moi je suis
010 spk2>	 à quatre minutes de mon domicile montre en main de à la station volontaire où j' habite rue Dargue et sur l' estimation euh on est déjà à neuf minutes
011 spk1>	 hm+hm
012 spk2>	 alors avant de de déménager je j' étais donc de+chez moi au trente-neuf rue Dargue dans métro volontaires au dix Place+de+Budapest je mettais vingt minutes de porte à porte c'est-à-dire de en quittant mon domicile jusqu'à euh au dix Place+de+Budapest mon employeur la SNCF maintenant je suis avenue d' Ivry
013 spk2>	 j' ai trois changements je change je prends le métro comme d'+habitude
014 spk2>	 à volontaires
015 spk1>	 à volontaires
016 spk1>	 toujours oui
017 spk2>	 ensuite
018 spk2>	 je change à Pasteur à Pasteur je vais jusqu'à Place+d'Italie là j' ai tous+les couloirs et ensuite je reprends jusqu'à la ligne sept jusqu'à euh porte d' Ivry sachant+que il y a une fourche euh à à Place+d'Italie des rames qui vont sur euh à sur Villejuif et d' autres qui vont euh sur euh
019 spk2>	 sur Ivry
020 spk1>	 sur Ivry
021 spk2>	 ce matin par+exemple j' ai attendu six minutes bon et on me dit que le temps de trajet est le même alors je conteste parce+que quand on fait avec l' estimation euh sur votre site euh il y a que huit minutes de dé() d()
022 spk2>	 comment de décalage
023 spk2>	 si vous voulez
024 spk1>	 non nous on n()
025 spk1>	 peut rien vous faxer nous hein là c' est tout+à+fait c' est v() v() faut le site+internet Corr() il va dans votre sens à vous
026 spk1>	 les résultats que vous avez
027 spk1>	 sur le sur internet
028 spk2>	 oui
029 spk1>	 c' est ça va bien dans votre sens à vous
030 spk2>	 non malheureusement non non ça ça me fait que
031 spk1>	 non c' est l' inverse ah nous le problème on
032 spk2>	 ça me fait que huit minutes c'est-à-dire que quand je je on fait trente-neuf rue Dargue jusqu' au dix Place+de+Budapest ça nous fait trente minutes
033 spk1>	 hm+hm
034 spk2>	 trente minutes
035 spk2>	 alors+que quand on fait euh trente-neuf rue Drague mon domicile jusqu' au seize avenue d' Ivry où je travaille avec trois changements ça fait trente-huit minutes donc il y a

