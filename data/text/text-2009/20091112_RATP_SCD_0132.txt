
DIALOG=20091112_RATP_SCD_0132

001 spk2>	 NNAAMMEE bonjour
002 spk1>	 oui bonjour
003 spk2>	 bonjour
004 spk1>	 j' ai perdu euh un sac hier sur la ligne six
005 spk2>	 oui
006 spk1>	 aux alentours de vingt+et+une heures euh et euh je voulais savoir euh comment être en en rapport en+relation+avec les objets trouvés et euh à quelle adresse je pouvais aller
007 spk2>	 un sac quelle couleur
008 spk1>	 attendez excusez -moi
009 spk1>	 vous me dites
010 spk2>	 un sac de quelle couleur
011 spk1>	 alors il est en cuir de couleur
012 spk1>	 kaki
013 spk1>	 non comment il est
014 spk1>	 hein vert kaki hein c' est ça hein vert kaki ouais
015 spk2>	 qu'est-ce+qu' il y avait à l' intérieur
016 spk1>	 un appareil photo numérique
017 spk2>	 hm d'accord patientez je vérifie
018 spk1>	 merci
019 spk3>	 qui ce sac
020 spk2>	 monsieur
021 spk2>	 sur le sac
022 spk1>	 oui
023 spk2>	 il y avait une marque
024 spk1>	 Mandarina+Duck
025 spk2>	 alors il y a un sac qui a été retrouvé qui se trouve euh
026 spk2>	 alors sur la ligne cinq station Place+d'Italie
027 spk1>	 Place+d'Italie ligne cinq
028 spk2>	 oui
029 spk1>	 d'accord euh
030 spk2>	 donc il faut y aller le+plus vite possible avec une pièce d' identité pour récupérer votre sac
031 spk1>	 d'accord
032 spk2>	 parce+que ensuite
033 spk2>	 il va être transféré à la préfecture rue des Morillons
034 spk1>	 il va être transféré en préfecture après
035 spk1>	 OK
036 spk2>	 ouais
037 spk1>	 hm
038 spk2>	 donc euh ligne cinq Place+d'Italie
039 spk1>	 Place+d'Italie je me je me je m' adresse à l' accueil c' est ça euh à la personne qui donne les infos là
040 spk2>	 oui tout+à+fait au guichet là oui
041 spk1>	 OK ce serait celui-là
042 spk2>	 ben oui moi j' ai un sac euh sac à main Mandarina+Duck
043 spk2>	 kaki
044 spk1>	 Mandarina+Duck
045 spk1>	 OK et il y a l' appareil dedans
046 spk2>	 euh moi j' ai pas d' appareil photo de répertorié dedans j' ai des livres et des vêtements
047 spk1>	 des livres et vêtements pas d' appareil OK hm
048 spk2>	 bon peut-être qu' il y a il y a toujours l' appareil mais qui n' a pas été indiqué sur l' ordinateur hein
049 spk1>	 OK OK très+bien ben je vous remercie et je me rends là-bas
050 spk1>	 tout+de+suite je vous remercie au+revoir
051 spk2>	 hm d'accord bonne journée au+revoir

