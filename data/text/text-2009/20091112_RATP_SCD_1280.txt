
DIALOG=20091112_RATP_SCD_1280

001 spk1>	 NNAAMMEE bonsoir
002 spk1>	 NNAAMMEE bonsoir
003 spk2>	 oui bonsoir monsieur
004 spk2>	 je voudrais savoir si sur la ligne B du RER il y a un préavis de grève pour demain
005 spk1>	 bah malheureusement on a aucune information alors euh je pense que c' est hum euh on aurait eu une information comme+quoi le euh préavis n' aurait pas été reconduit je pense qu' on l' aurait eu
006 spk1>	 a+priori comme c' est on a pas d' informations pour le moment je peux pas vous dire que c' est
007 spk2>	 bon
008 spk2>	 mais euh et ah
009 spk1>	 c' est oui ou non mais malheureusement on a pas l' information
010 spk2>	 non je sinon euh j' avais vu des panneaux et comme+quoi la la connexion à Gare+du+Nord était rétablie est-ce+que c' est c' est vrai
011 spk1>	 oui aux heures de pointe oui il y a aucun problème
012 spk2>	 qu' est ce euh heures de pointe disons qu' est euh qu'est-ce+que vous appelez heures de pointe
013 spk1>	 bah c'est-à-dire le euh le matin
014 spk1>	 à+partir+de sept heures
015 spk2>	 le matin
016 spk1>	 jusqu'à neuf heures par+exemple
017 spk2>	 ouais d'accord
018 spk1>	 et euh
019 spk1>	 le aux heures de pointe le soir de dix-sept heures à euh
020 spk2>	 ouais ouais
021 spk1>	 dix-neuf heures on va dire
022 spk2>	 ouais ouais donc disons que je euh six heures et demie du six heures et quart six heures et demie c' est bon
023 spk1>	 oui
024 spk2>	 comme heures de pointe
025 spk1>	 oui oui oui oui
026 spk2>	 bon d'accord
027 spk2>	 je vous remercie
028 spk1>	 et je vous en prie vous de+même au+revoir
029 spk2>	 monsieur bonne soirée au+revoir

