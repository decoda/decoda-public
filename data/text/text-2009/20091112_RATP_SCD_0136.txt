
DIALOG=20091112_RATP_SCD_0136

001 spk1>	 NNAAMMEE vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 bonjour madame excusez -moi de vous déranger euh j' ai déjà eu eu fait c() fait connaissance de votre euh de votre service et ça a toujours très été très bien marché
004 spk2>	 oui
005 spk3>	 et là
006 spk3>	 demain je dois aller voir ma ma maman qui est en maison de retraite au Mans donc je prends toujours la ligne treize puisque je viens de Bondy
007 spk2>	 hm
008 spk3>	 et
009 spk2>	 là on me dit que enfin j' ai entendu que la que la ligne treize ne par ne marchait pas+du+tout
010 spk2>	 ah si si si
011 spk3>	 pour aller à Montparnasse
012 spk3>	 comment vous pouvez
013 spk3>	 me conduire
014 spk2>	 non mais là
015 spk2>	 c' était une panne de matériel hein madame hein
016 spk2>	 aujourd'hui
017 spk3>	 oui alors
018 spk3>	 ben oui mais
019 spk2>	 et là le
020 spk2>	 trafic est perturbé mais ça roule
021 spk3>	 ah+bon ça roule
022 spk2>	 oui
023 spk2>	 ça fonctionne oui
024 spk2>	 il y a juste un
025 spk2>	 c' est du retard
026 spk3>	 ben oui mais il faut pas que j' aie beaucoup+de retard non+plus
027 spk2>	 oui mais demain demain est une autre journée
028 spk3>	 ah+ben vous me
029 spk2>	 non beh
030 spk3>	 vous êtes gentille vous
031 spk2>	 non non chaque jour
032 spk2>	 est différent madame
033 spk3>	 ah+ben c' est gentil mais si toutefois je ne peux pas l' avoir
034 spk3>	 cette treize
035 spk2>	 non mais on peut
036 spk2>	 pas savoir à l' avance s' il va y avoir une panne de matériel demain
037 spk3>	 oui mais euh pour aller à Montparnasse à+part la treize qu'est-ce+que je peux prendre comme ligne de métro
038 spk2>	 en partant de Montparnasse
039 spk2>	 ben vous pouvez
040 spk3>	 oui non en
041 spk3>	 partant de de Bondy bon j' ai le RER+E
042 spk3>	 jusqu'à Saint-Lazare
043 spk2>	 jusqu'à Haussmann
044 spk3>	 oui
045 spk2>	 non vous pouvez prendre
046 spk3>	 mais+alors après
047 spk2>	 la douze aussi
048 spk3>	 ah la douze
049 spk2>	 oui en direction mairie d' Issy
050 spk3>	 et elle va à Montparnasse
051 spk2>	 tout+à+fait
052 spk3>	 ah d'accord
053 spk3>	 bon et pour revenir
054 spk3>	 par euh
055 spk2>	 ou+alors
056 spk2>	 éventuellement
057 spk3>	 oui
058 spk2>	 vous descendez à Magenta et vous avez la ligne quatre du métro direction Porte+d'Orléans
059 spk3>	 ah+ben attendez je note la ligne quatre
060 spk3>	 jusqu'à Porte+d'Orléans porte ligne euh Porte+d'Orléans
061 spk2>	 Magenta jusqu'à porte en direction de Porte+d'Orléans il y a Montparnasse
062 spk3>	 ah d'accord et pour revenir alors je dois prendre la quatre aussi
063 spk3>	 ou la douze
064 spk2>	 comme vous voulez
065 spk2>	 ou la douze à Montparnasse oui
066 spk3>	 Montparnasse
067 spk2>	 oui
068 spk2>	 hm
069 spk3>	 bon+ben écoutez c' est très gentil à vous
070 spk3>	 merci beaucoup madame bon courage
071 spk2>	 je vous en prie bonne journée
072 spk2>	 au+revoir
073 spk3>	 au+revoir madame

