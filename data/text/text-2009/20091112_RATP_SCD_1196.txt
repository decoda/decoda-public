
DIALOG=20091112_RATP_SCD_1196

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonsoir
003 spk2>	 moi je voudrais avoir des petits renseignements sur la ligne cent+soixante-douze pour demain s'il+vous+plaît
004 spk1>	 oui qu'est-ce+que vous voudriez comme renseignements madame
005 spk2>	 ben euh aujourd'hui ça a été la galère euh pour aller et venir donc euh je voudrais savoir si euh la grève se poursuit demain
006 spk1>	 ah
007 spk2>	 et à raison
008 spk2>	 de quelle euh
009 spk1>	 ah non pour n()
010 spk2>	 combien+de bus
011 spk1>	 alors pour la la grève que vous aviez aujourd'hui sur le dépôt de bus de de Vitry
012 spk1>	 elle n' est
013 spk2>	 oui
014 spk1>	 pas reconductible demain hein demain tout est normal au+niveau+de la circulation
015 spk2>	 ah+bon
016 spk2>	 ben d'accord
017 spk2>	 donc
018 spk1>	 oui
019 spk2>	 c' était ça ma question
020 spk1>	 oui
021 spk2>	 en+fait voilà
022 spk2>	 parce+que je suppose que la ligne cent+soixante-douze elle était affectée par ça
023 spk1>	 ah oui oui tout+à+fait c' était tout+le dépôt de bus de Vitry
024 spk2>	 et demain ils repartent euh demain matin sans problème
025 spk1>	 oui exactement madame
026 spk2>	 OK d'accord merci
027 spk2>	 beaucoup
028 spk1>	 mais je vous
029 spk1>	 en prie
030 spk2>	 bonne soirée
031 spk1>	 bonne
032 spk2>	 au+revoir
033 spk1>	 à vous aussi madame au+revoir

