
DIALOG=20091112_RATP_SCD_0220

001 spk1>	 un instant va vous répondre
002 spk1>	 bonjour
003 spk2>	 oui bonjour
004 spk2>	 je voudrais parler madame euh à madame
005 spk1>	 elle travaille où cette dame
006 spk2>	 au centre clientèle du bus Quai+de+Seine
007 spk1>	 c' est quelle ligne de bus madame s'il+vous+plaît
008 spk2>	 quelle ligne de bus
009 spk1>	 oui
010 spk2>	 le soixante-deux
011 spk1>	 alors c' est à quel sujet
012 spk2>	 ben c' est euh sujet d' un accident que j' ai eu euh euh dans l' autobus c'est-à-dire que l() l' autobus a démarré et et j' ai pas eu le temps de me tenir je je me suis affalée et je suis en pourparlers avec eux
013 spk1>	 d'accord
014 spk2>	 et il y a
015 spk1>	 donc le bus soixante deux
016 spk2>	 euh référence bus Quai+de+Seine numéro euh
017 spk1>	 je vais voir si madame est là
018 spk1>	 vous patientez un instant merci
019 spk2>	 merci oui
020 spk3>	 RATP Centre+Bus des Quai+de+Seine bonjour
021 spk1>	 oui bonjour c' est Service+Clients+à+Distance j' ai une dame en ligne qui a eu un accident sur le soixante-deux enfin le machiniste aurait freiné elle serait tombée
022 spk3>	 oui
023 spk1>	 et elle m' a
024 spk1>	 dit qu' elle serait en pourparlers avec vous et c' est qui gérerait ça son dossier actuellement
025 spk3>	 oui
026 spk1>	 et elle voudrait savoir ce+qu' il en est la dame ben je suis en double là si je peux te la passer
027 spk3>	 ouais ben tu tu peux me la passer là
028 spk1>	 je te remercie beaucoup
029 spk3>	 je t' en prie
030 spk1>	 au+revoir
031 spk3>	 au+revoir

