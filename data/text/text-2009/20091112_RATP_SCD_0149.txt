
DIALOG=20091112_RATP_SCD_0149

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 oui bonjour
004 spk2>	 bonjour madame
005 spk3>	 je désirais avoir un horaire du cent+seize
006 spk3>	 à partir
007 spk2>	 oui
008 spk3>	 de la gare euh Saint-Maur RER
009 spk2>	 oui
010 spk3>	 en direction de gare de Nogent-Le+Perreux
011 spk2>	 d'accord alors
012 spk2>	 Saint-Maur-Créteil Nogent-Le+Perreux hein
013 spk3>	 voilà c' est ça j' ai un horaire euh de l' année dernière mais
014 spk3>	 je voudrais vérifier
015 spk2>	 d'accord
016 spk2>	 vers quelle heure vous voulez
017 spk3>	 ben écoutez je voudrais vérifier si le quatorze heures trois et le quatorze heures vingt-sept c' est à+peu+près les mêmes horaires
018 spk2>	 d'accord
019 spk2>	 un petit instant hein madame je recherche
020 spk3>	 merci beaucoup
021 spk2>	 madame
022 spk3>	 oui
023 spk2>	 oui excusez -moi vous êtes à quelle gare exactement Saint-Maur-Créteil
024 spk3>	 Champigny-Saint-Maur
025 spk2>	 ah oui Champigny d'accord
026 spk2>	 et vous allez à Nogent-Le+Perreux hein
027 spk3>	 voilà
028 spk2>	 d'accord et vous m' avez dit vers
029 spk2>	 quatorze heures
030 spk3>	 quatorze heures
031 spk3>	 trois quatorze heures vingt-sept dites donc ai eu un j' ai t() eu tout+le disque après
032 spk3>	 après+que je vous ai eu là
033 spk2>	 oui c' est normal hein
034 spk3>	 ah+bon d'accord
035 spk2>	 donc le cent+seize alors vers euh quatorze
036 spk3>	 quatorze il y avait
037 spk3>	 quatorze heures trois et quatorze heures vingt-sept au départ
038 spk2>	 ouais alors celui-là je l' ai pas
039 spk2>	 on va faire quatorze heures
040 spk2>	 quinze heures même comme+ça on sera tranquilles
041 spk2>	 voilà cent+seize direction Bois-Perrier alors j' ai quatorze heures cinq
042 spk3>	 quatorze heures cinq
043 spk2>	 et ensuite quatorze heures trente-deux
044 spk3>	 et quatorze heures trente-deux d'accord et pendant+que je vous ai c' est possible de regarder le retour
045 spk2>	 oui bien
046 spk2>	 sûr
047 spk3>	 hein
048 spk3>	 d'accord
049 spk3>	 alors ça serait euh ben disons à+partir+de la gare de Nogent-Le+Perreux
050 spk3>	 là
051 spk2>	 ouais
052 spk3>	 voilà euh vers euh alors attendez euh parce+que j' ai rendez-vous euh vers seize heures
053 spk2>	 alors seize heures pile et ensuite seize heures vingt-huit
054 spk3>	 seize heures pile et seize heures vingt-huit d'accord et celui d'avant
055 spk2>	 quinze quarante
056 spk3>	 quinze quarante quinze quarante seize heures et seize heures euh
057 spk2>	 vingt-huit
058 spk3>	 vingt-huit
059 spk3>	 je vous remercie
060 spk2>	 je vous en prie
061 spk3>	 au+revoir
062 spk2>	 bonne journée madame

