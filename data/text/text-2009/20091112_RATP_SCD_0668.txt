
DIALOG=20091112_RATP_SCD_0668

001 spk1>	 NNAAMMEE
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 merci
004 spk2>	 oui bonjour madame bonjour monsieur
005 spk3>	 bonjour
006 spk3>	 ce serait pour connaître euh les horaires euh du RER+C
007 spk2>	 oui
008 spk3>	 euh en partance de comment de Boulevard+Victor+Garigliano
009 spk2>	 oui
010 spk3>	 en direction de Savigny-sur-Orge en semaine et le samedi
011 spk2>	 alors donc en partant donc du
012 spk2>	 Boulevard+Victor pour
013 spk2>	 aller vers
014 spk3>	 oui
015 spk2>	 Savigny-sur-Orge hein
016 spk3>	 c' est ça
017 spk2>	 d'accord vers quelle heure en semaine
018 spk3>	 en semaine le matin bon vers dix heures quelque+chose
019 spk3>	 comme+ça
020 spk2>	 vers dix heures
021 spk2>	 d'accord très+bien
022 spk2>	 je vais vous
023 spk3>	 et
024 spk2>	 informer alors Savigny-sur-Orge au+départ+de Boulevard+Victor en semaine vous m' avez dit vers dix heures hein
025 spk3>	 oui
026 spk2>	 alors vous avez un départ de Boulevard+Victor à dix heures huit
027 spk3>	 dix heures
028 spk3>	 huit
029 spk2>	 alors c' est toutes
030 spk2>	 les trente minutes hein
031 spk3>	 ah oui
032 spk2>	 dix heures huit alors avant c' est neuf heures trente-huit
033 spk3>	 oui
034 spk2>	 et après dix heures huit c' est dix heures trente-huit
035 spk3>	 dix heures
036 spk3>	 trente-huit
037 spk2>	 ouais il faut compter
038 spk2>	 quarante minutes jusqu'à Savigny-sur-Orge
039 spk3>	 quarante
040 spk3>	 minutes
041 spk2>	 voilà
042 spk2>	 donc c' est en direction de Saint-Martin+d'Étampes hein
043 spk3>	 ah oui
044 spk2>	 et+donc
045 spk3>	 hm+hm
046 spk2>	 vous voulez les horaires euh suivants ou pour le samedi
047 spk3>	 oui samedi
048 spk2>	 pour le samedi
049 spk2>	 toujours vers la même heure
050 spk3>	 oh oui
051 spk3>	 oui
052 spk2>	 d'accord
053 spk2>	 alors le samedi
054 spk2>	 alors ouais c' est les mêmes fréquences hein dix heures huit dix heures trente-huit onze heures huit
055 spk3>	 c' est les mêmes
056 spk3>	 fréquences
057 spk2>	 c' est les mêmes
058 spk2>	 fréquences tout+à+fait
059 spk3>	 bon entendu et+puis alors je voulais vous demander euh qu'est-ce+que c' est oui euh là pour euh le si par+exemple je ne vais que hum comme c' est le euh euh que à la gare
060 spk3>	 d' Austerlitz
061 spk2>	 oui
062 spk3>	 est-ce+que ce je mon mon mon mon ticket là euh
063 spk3>	 magnétisé
064 spk2>	 oui
065 spk3>	 est-ce+que je peux le passer dans la machine il sera pas abîmé
066 spk2>	 euh mais de+toute+façon vous voyagez votre ticket c' est un ticket de métro parisien normalement
067 spk2>	 ou un ticket
068 spk3>	 hm+hm
069 spk2>	 pour la
070 spk2>	 destination euh Savigny
071 spk3>	 ah+ben j' ai j' ai
072 spk3>	 j' ai un ticket pour parisien et+puis si je vais à Savigny je reprends un
073 spk3>	 ticket RER hein
074 spk2>	 ah oui oui il sera valable
075 spk2>	 effectivement euh si vous arrivez à Austerlitz mais après euh pour euh continuer votre trajet jusqu'à Savigny il faut repoinçonner votre ticket de qui est valable de d' Austerlitz à à Savigny après
076 spk3>	 ben c' est ça oui
077 spk3>	 il faut que
078 spk2>	 oui
079 spk3>	 je prenne un ticket de
080 spk3>	 RER c' est ça hein
081 spk2>	 exactement tout+à+fait madame c' est bien ça
082 spk3>	 bon merci+bien
083 spk2>	 je vous en prie
084 spk3>	 non parce+que quelque fois ça dimanche euh on dit qu' on s' enlève le
085 spk3>	 magnétisme
086 spk2>	 oui
087 spk2>	 c' est vrai ça peut arriver
088 spk3>	 pour euh certains euh appareils
089 spk2>	 oui
090 spk3>	 alors je bon je sais que c' est bon pour le tram c' est bon pour le métro
091 spk2>	 oui
092 spk2>	 mais pas
093 spk3>	 mais bon
094 spk2>	 pour le RER
095 spk3>	 pas pour
096 spk3>	 le RER il faut pas passer dedans
097 spk2>	 non non c' est pas la même tarification non
098 spk3>	 ah non justement même+si on va que dans Paris
099 spk2>	 alors non si vous faites que du Paris
100 spk3>	 si je fais que du
101 spk3>	 Paris
102 spk2>	 c' est bon
103 spk2>	 avec le ticket que vous avez c' est bon
104 spk3>	 en le passant là
105 spk2>	 voilà
106 spk2>	 c' est bon par+contre si
107 spk3>	 il y a pas de problème ah oui
108 spk2>	 vous quittez Paris
109 spk2>	 maintenant
110 spk3>	 ah oui
111 spk2>	 il sera plus valable
112 spk3>	 ah+ben ça c' est sûr
113 spk3>	 ah oui
114 spk2>	 d'accord
115 spk3>	 oui d'accord
116 spk2>	 tout simplement
117 spk3>	 bon merci
118 spk3>	 monsieur
119 spk2>	 je vous en prie madame
120 spk3>	 au+revoir
121 spk3>	 monsieur au+revoir
122 spk2>	 bonne soirée au madame
123 spk2>	 au+revoir

