
DIALOG=20091112_RATP_SCD_0176

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 oui bonjour madame madame je voudrais avoir un renseignement
004 spk2>	 oui
005 spk3>	 euh j' ai mon garçon handicapé mental qui a une carte Améthyste et celle-ci s' est démagnétisée avant j' allais trente rue Championnet la changer tous+les ans évidemment est-ce+que c' est toujours valable
006 spk2>	 oui bien+sûr
007 spk3>	 et c' est euh quel jour euh de réception
008 spk2>	 alors euh les jours de réception pour euh les cartes euh Améthyste je regarde madame
009 spk2>	 hein je vous demande un instant
010 spk3>	 merci madame
011 spk2>	 s'il+vous+plaît
012 spk3>	 oui
013 spk2>	 alors euh la réception c' est le lundi le mercredi et le vendredi
014 spk3>	 oui
015 spk2>	 de quatorze heures à seize heures
016 spk3>	 de quatorze heures à seize heures le vendredi également
017 spk2>	 oui ah+ben tant+mieux ça m' arrange parce+que j' habite à quatre-vingts kilomètres de Paris
018 spk3>	 alors
019 spk2>	 parce+que sinon euh
020 spk2>	 il faut vous rendre auprès+de votre service hein qui vous euh a délivré le titre hein
021 spk3>	 ben non c' est la mairie du douzième
022 spk2>	 ah
023 spk3>	 et bon+ben ils veulent pas
024 spk2>	 ah+bon d'accord
025 spk3>	 oui
026 spk3>	 c' est la raison pour+laquelle tous+les ans j' allais cavaler chez vous
027 spk2>	 oui parce+que euh c' est la mai c' est la mairie de Paris qui vous l' a
028 spk2>	 qui vous l' a attribuée c' est pour ça
029 spk3>	 oui qui enfin euh on l' a payée hein évidemment une carte Améthyste hein
030 spk2>	 hm+hm
031 spk3>	 hein
032 spk3>	 mais enfin non ils ne veulent pas euh changer les coupons
033 spk2>	 hm d'accord
034 spk1>	 c' est vrai que sur Paris non effectivement c' est seulement si c' est euh carte Émeraude effectivement
035 spk3>	 ben oui c' est toujours pareil
036 spk2>	 ouais ouais
037 spk3>	 alors c' est toujours trente rue Championnet
038 spk2>	 trente-quatre rue Championnet
039 spk3>	 trente-quatre rue c()
040 spk2>	 oui
041 spk3>	 merci madame
042 spk2>	 je vous en prie madame
043 spk3>	 au+revoir
044 spk2>	 bonne journée au+revoir

