
DIALOG=20091112_RATP_SCD_0683

001 spk1>	 NNAAMMEE bonjour
002 spk1>	 NNAAMMEE bonjour
003 spk2>	 oui bonjour monsieur
004 spk1>	 bonjour monsieur
005 spk2>	 euh voilà je cherche à me à euh je voudrais un un itinéraire s'il+vous+plaît
006 spk1>	 oui
007 spk1>	 je vous écoute
008 spk2>	 et
009 spk2>	 alors je voudrais je voudrais euh deux+cent+quatorze boulevard Galliéni à Villeneuve-la-Garenne
010 spk1>	 alors deux cent quatorze
011 spk1>	 boulevard
012 spk2>	 Galliéni
013 spk1>	 Galliéni
014 spk2>	 à Villeneuve-la-Garenne
015 spk1>	 à Villeneuve-la-Garenne
016 spk2>	 jusqu' au cent+vingt-neuf rue Jules+Guesde
017 spk1>	 alors jusqu' au
018 spk1>	 cent vingt-neuf rue
019 spk1>	 Jules
020 spk2>	 au
021 spk1>	 Guesde
022 spk2>	 oui à Levallois-Perret
023 spk1>	 vous m' avez dit cent vingt combien excusez -moi
024 spk2>	 le le cent+vingt-neuf
025 spk1>	 d'accord rue Jules+Guesde très+bien donc vous partez du boulevard Galliéni
026 spk2>	 c' est ça
027 spk1>	 pour aller au cent+vingt-deux rue Jules Jules+Guesde à Levallois hein
028 spk2>	 au cent vingt-neuf
029 spk1>	 cent+vingt-neuf pardon vous patientez un instant
030 spk1>	 je me renseigne
031 spk2>	 oui
032 spk1>	 hein
033 spk2>	 merci
034 spk1>	 s' il s'il+vous+plaît
035 spk2>	 oui monsieur donc merci d' avoir patienté donc vous allez emprunter à+partir+de la parce+que vous êtes pas très loin+de la mairie de Villeneuve-la-Garenne
036 spk2>	 monsieur euh
037 spk2>	 je suis juste en face
038 spk1>	 voilà
039 spk1>	 vous allez prendre le bus trois+cent+soixante-dix-huit
040 spk2>	 oui c' est le+plus pratique euh c' est le+plus pratique
041 spk2>	 enfin le+plus rapide
042 spk1>	 parce+que de là oui
043 spk1>	 c' est le+plus pratique avec deux bus pour vraiment être dans la rue Jules+Guesde
044 spk2>	 d'accord
045 spk1>	 hein donc vous allez prendre le bus trois cent soixante-dix huit
046 spk2>	 attendez je note euh euh vous m' excusez hein
047 spk1>	 oui oui bien+sûr
048 spk2>	 parce+que j' ai vu sur internet ils me font passer par euh par trois bus quoi
049 spk1>	 non moi j' ai deux bus moi
050 spk2>	 d'accord
051 spk2>	 OK
052 spk1>	 j' ai
053 spk1>	 deux bus
054 spk2>	 alors
055 spk2>	 trois cent soixante-dix huit
056 spk1>	 trois cent soixante-dix huit
057 spk1>	 donc en direction de La+Défense jusqu'à l' arrêt qui s' appelle Quatre+Routes
058 spk2>	 aux Quatre+Routes ouais
059 spk1>	 voilà
060 spk1>	 et Quatre+Routes là vous récupérez le bus cent+soixante-cinq
061 spk2>	 d'accord
062 spk1>	 direction Porte+de+Champerret
063 spk1>	 cent+soixante-cinq direction Porte+de+Champerret
064 spk1>	 jusqu'à l' arrêt qui s' appelle
065 spk2>	 cent+soixante-cinq
066 spk1>	 alors direction Porte+de+Champerret
067 spk2>	 oui
068 spk1>	 jusqu'à l' arrêt qui s' appelle Collange C O
069 spk1>	 deux L
070 spk2>	 Collange
071 spk1>	 A N G E et il vous laisse euh vraiment à l' angle euh de la rue Jules+Guesde vous faites cinquante mètres après l' arrêt
072 spk1>	 vous êtes dans la
073 spk2>	 d'accord
074 spk1>	 rue Jules+Guesde
075 spk2>	 euh le temps de parcours
076 spk1>	 vous comptez en+gros en maximum quarante-cinq minutes à+peu+près quarante-cinq à cinquante minutes
077 spk2>	 d'accord
078 spk1>	 d'accord
079 spk2>	 d'accord euh bon euh je voudrais savoir quel euh parce+que moi ça fait euh des années que j' ai pas pris euh la la RATP
080 spk1>	 hm+hm
081 spk2>	 donc je voulais je voulais savoir que euh comment comment ça fonctionne euh pour le
082 spk1>	 pour le euh est-ce+qu' il y a une carte Orange des choses comme+ça
083 spk1>	 ben si vous euh avez une carte Orange oui il faut il faut une carte deux à trois zones pour ce trajet là
084 spk1>	 ça dépend si vous faites
085 spk2>	 d'accord d'accord c' est deux trois zones
086 spk1>	 c' est deux trois zones
087 spk2>	 hm+hm
088 spk1>	 sinon si vous faites les trajet une fois
089 spk1>	 occasionnellement avec
090 spk1>	 un ticket
091 spk2>	 ouais
092 spk1>	 vous faites le trajet avec les deux bus maintenant
093 spk2>	 non mais j' en n' ai non je vais en avoir besoin
094 spk2>	 pour quelques
095 spk1>	 ah
096 spk2>	 mois
097 spk1>	 d'accord alors+qu' à la limite ce serait mieux de prendre un un Navigo Orange alors
098 spk2>	 d'accord euh je euh je prends ça où alors
099 spk1>	 alors il faut aller le prendre euh alors ça se fait en agence avec une pièce d' identité
100 spk1>	 un justificatif
101 spk2>	 ouais
102 spk1>	 de domicile
103 spk2>	 ouais
104 spk1>	 euh vous allez donc euh alors vous vous êtes sur euh Villeneuve-la-Garenne
105 spk2>	 hm+hm
106 spk1>	 le+plus proche pour vous euh parce+que l' agence commerciale c' est uniquement dans les agences qu' ils le font
107 spk2>	 d'accord
108 spk1>	 euh pff ce serait La+Défense hein parce+que je vois pas où aller autrement par+rapport+à ouais La+Défense après euh
109 spk1>	 vous avez quoi comme gare vous avez pas une gare à+proximité hein
110 spk1>	 le proche
111 spk2>	 euh j' ai euh j' ai
112 spk1>	 c' est quoi c' est euh
113 spk2>	 eh+ben la gare j' ai euh j' ai Les+Grésillons
114 spk1>	 Les+Grésillons
115 spk2>	 oui
116 spk1>	 hum
117 spk2>	 mais c' est plutôt les RER
118 spk1>	 ben oui oui non mais il y a des gares il y a des agences aussi commerciales
119 spk1>	 sur le RER+C
120 spk2>	 d'accord
121 spk1>	 mais euh ça sera trop loin pour vous parce+que sur le RER+C euh à la limite ouais Pontoise ça fait loin quand+même hein Pereire-Levallois il y en a une dans Paris mais sinon c' est Pontoise hein ça fait
122 spk1>	 loin hein
123 spk2>	 hm+hm
124 spk1>	 ça fait loin hein
125 spk2>	 bien+sûr que oui
126 spk2>	 hm+hm
127 spk1>	 après si c' est non c' est La+Défense euh vous en avez une à Porte+de+Clignancourt à la Gare+du+Nord Châtelet
128 spk2>	 d'accord
129 spk2>	 mais il y a pas des euh j' ai entendu dire dans les euh ceux qui vendent euh les les tickets
130 spk2>	 style les
131 spk1>	 non
132 spk2>	 les marchands de journaux les choses comme+ça
133 spk1>	 non les marchands de journaux c' est que les tickets au détail hein ou les carnets hein qui sont vendus dedans
134 spk1>	 dans les guichets
135 spk2>	 d'accord
136 spk1>	 par+contre si vous voulez avoir un un passe vous pouvez l' avoir en station
137 spk1>	 mais ce sera un euh ce sera un passe qui va être payant vous allez le payer cinq euros
138 spk2>	 ouais
139 spk1>	 vous allez pouvoir l' avoir tout+de+suite il faudra mettre une petite photo dessus
140 spk2>	 d'accord
141 spk1>	 ça va vous coûter cinq euros
142 spk1>	 une seule fois hein
143 spk2>	 ouais
144 spk1>	 et+puis après vous allez recharger dessus donc euh ce+que vous souhaitez donc le transport pour euh deux à trois zones pour un mois par+exemple
145 spk1>	 ça vous pouvez le prendre
146 spk2>	 si euh
147 spk1>	 dans n'+importe+quelle station
148 spk2>	 d'accord
149 spk1>	 mais il va vous coûter cinq euros alors+que
150 spk1>	 si vous allez en
151 spk2>	 mais
152 spk1>	 agence
153 spk1>	 le passe est gratuit
154 spk1>	 vous ne payez
155 spk2>	 d'accord
156 spk1>	 que la recharge
157 spk2>	 d'accord
158 spk2>	 et euh par+exemple moi j' aurais besoin euh pour lundi euh je fais euh mais ça va me compter tout+le mois ou pas
159 spk1>	 pour lundi qui vient là
160 spk2>	 oui
161 spk2>	 euh
162 spk1>	 ben ça vous
163 spk1>	 comptera euh c' est du premier au trente ou trente+et+un hein
164 spk2>	 hm+hm
165 spk1>	 donc ça vous fera jusqu'à la fin du mois oui
166 spk1>	 mais vous allez perdre un+petit+peu de bénéfice par+rapport+au début du jour de
167 spk1>	 par début de du
168 spk2>	 voilà c' est ça
169 spk1>	 mois quoi hein donc
170 spk1>	 euh
171 spk2>	 oui
172 spk1>	 là si vous le prenez lundi euh pff à la limite euh
173 spk1>	 euh je regarde
174 spk1>	 est-ce+que ça vaut le coup d' en prendre euh un pour la semaine qui vient euh et un
175 spk1>	 autre pour la
176 spk2>	 on
177 spk1>	 semaine d'+après
178 spk2>	 on peut le faire à la semaine
179 spk2>	 alors
180 spk1>	 on peut le faire
181 spk1>	 à la semaine parce+que
182 spk1>	 vous deux trois
183 spk2>	 d'accord
184 spk1>	 deux trois zones
185 spk1>	 ça vous reviendra
186 spk2>	 oui
187 spk1>	 à à prendre euh donc
188 spk1>	 un chargement pour la semaine donc seize euros trente pour lundi prochain jusqu'à dimanche
189 spk2>	 d'accord
190 spk1>	 et à+nouveau seize euros trente pour lundi vingt-trois jusqu' au dimanche vingt-neuf
191 spk2>	 d'accord
192 spk2>	 mais je peux prendre euh
193 spk1>	 euh
194 spk2>	 pour euh pour les quinze jours
195 spk2>	 ça en
196 spk1>	 non c' est une
197 spk1>	 semaine à chaque fois et après
198 spk1>	 vous rechargez
199 spk2>	 d'accord
200 spk1>	 pour la semaine suivante
201 spk2>	 euh pour le chargement ça se passe comment
202 spk1>	 alors soit vous pouvez le faire euh au guichet
203 spk1>	 donc l' agent euh
204 spk2>	 hm+hm
205 spk1>	 soit vous réglez par
206 spk1>	 en espèces ou en carte bleue ou
207 spk1>	 chèque vous demandez
208 spk2>	 hm+hm d'accord
209 spk1>	 à ce+qu' ils vous mettent dessus la recharge pour la semaine
210 spk2>	 d'accord
211 spk1>	 ça sera valable du lundi au dimanche
212 spk2>	 hm+hm
213 spk1>	 et+puis sinon ben euh à+partir+de dimanche euh
214 spk1>	 euh ou lundi matin vous rechargez pour la semaine d'+après
215 spk1>	 du vingt-trois au vingt-neuf
216 spk2>	 d'accord
217 spk2>	 d'accord
218 spk2>	 mais
219 spk1>	 d'accord
220 spk2>	 mais mais les zones c' est deux et trois
221 spk1>	 vous c' est deux et trois
222 spk1>	 pour ce trajet là
223 spk2>	 d'accord
224 spk1>	 deux et trois
225 spk2>	 d'accord
226 spk1>	 par+contre attention si vous chargez du seize au vingt-deux
227 spk1>	 dimanche et du
228 spk2>	 oui
229 spk1>	 vingt-trois au vingt-neuf
230 spk1>	 euh ce sera un dimanche
231 spk2>	 ouais
232 spk1>	 la carte sera là jusqu' au dimanche et il va vous manquer lundi hein
233 spk2>	 oui
234 spk2>	 bien+sûr
235 spk1>	 lundi
236 spk1>	 c' est le trente hein donc euh
237 spk2>	 hm+hm
238 spk1>	 et après euh vous chargerez pour le mois
239 spk2>	 d'accord
240 spk1>	 ça ça vous reviendra je pense que ça vous reviendra moins cher
241 spk1>	 que de prendre directement le coupon mensuel parce+qu' il coûte cinquante-trois euros cinquante et là vous avez perdu déjà quinze jours
242 spk2>	 ben oui non mais c' est pour
243 spk2>	 ça c' est juste
244 spk1>	 c' est vrai que
245 spk2>	 pour ces quinze jours là
246 spk1>	 voilà
247 spk2>	 après euh bon euh mais euh il faut une photo c' est ça vous m' avez dit
248 spk1>	 alors pour le Navigo+Découverte qui est payant
249 spk2>	 oui
250 spk1>	 il faut une photo
251 spk2>	 d'accord
252 spk1>	 mais le problème avec celui-là quand vous achetez au guichet vous l' avez
253 spk1>	 tout+de+suite
254 spk2>	 hm
255 spk1>	 si vous le perdez vous perdez ce+qu' il y a dessus hein
256 spk2>	 bien+sûr OK
257 spk1>	 alors+que si vous allez en agence par+exemple à La+Défense euh
258 spk2>	 oui bien+sûr
259 spk2>	 OK
260 spk1>	 voilà euh
261 spk1>	 là c' est gratuit le passe vous rechargez à vous partez avec vide
262 spk1>	 vous le rechargez
263 spk2>	 d'accord
264 spk1>	 à votre convenance
265 spk2>	 d'accord
266 spk1>	 si vous avez chargé dessus une semaine vous le perdez euh
267 spk1>	 euh vous récupérez ce+qu' il y avait sur la semaine ou sur le mois et vous ne payez que huit euros oui
268 spk2>	 d'accord et ça c' est une euh c' est une carte Navigo euh euh mais
269 spk2>	 Navigo hein
270 spk1>	 c' est un Navigo
271 spk1>	 c' est un Navigo mais
272 spk1>	 ça se fait en agence
273 spk2>	 d'accord
274 spk1>	 par+contre ils vont vous prendre la photo
275 spk1>	 ils font tout ils
276 spk2>	 d'accord
277 spk1>	 fabriquent le passe et vous repartez avec
278 spk2>	 d'accord
279 spk1>	 d'accord
280 spk2>	 euh mais ça je peux le faire en gare ou pas ça
281 spk1>	 non ça c' est uniquement dans les agences commerciales dans les
282 spk1>	 gares
283 spk2>	 ah
284 spk1>	 c' est le Navigo+Découverte vous l' avez tout+de+suite
285 spk1>	 c' est ce+que je vous expliquais
286 spk2>	 ouais
287 spk1>	 et euh
288 spk2>	 ouais
289 spk1>	 vous n' avez plus qu' à mettre une photo dessus
290 spk1>	 mais il y a pas le même
291 spk2>	 d'accord
292 spk1>	 service+après-vente que l'autre c' est pour ça
293 spk2>	 d'accord
294 spk1>	 d'accord l' autre il faut aller impérativement en agence
295 spk2>	 ouais
296 spk1>	 ouais et vous avez une agence à La+Défense c' est vrai que là où vous êtes là Grésillons il y a pas d' agence hein
297 spk2>	 non
298 spk1>	 après Pontoise c' est beaucoup trop loin
299 spk1>	 ou euh
300 spk2>	 hm ça
301 spk2>	 fait loin
302 spk1>	 ben oui ça fait loin hein
303 spk2>	 hm+hm
304 spk1>	 hm
305 spk2>	 bon mais sinon je peux euh au au pire des cas je peux le prendre euh dans une gare
306 spk1>	 oui à+ce+moment-là il faut demander un Navigo+Découverte
307 spk1>	 et euh
308 spk2>	 d'accord
309 spk1>	 ça va vous coûter cinq euros en plus du montant que vous allez mettre dessus hein
310 spk2>	 oui oui d'accord
311 spk2>	 d'accord
312 spk1>	 d'accord
313 spk2>	 ouais
314 spk2>	 alors c' est euh c' est euh et quand je rentre dans le bus comment ça se passe
315 spk1>	 en+fait vous badgez le passe une+fois+qu' il est
316 spk1>	 chargé
317 spk2>	 d'accord
318 spk1>	 il y a un petit appareil
319 spk1>	 et c' est
320 spk2>	 d'accord
321 spk1>	 devant et+puis euh normalement il y a un voyant vert euh
322 spk1>	 qui
323 spk2>	 d'accord
324 spk2>	 OK
325 spk1>	 autorise le passage et qui dit que le passe est valide hein
326 spk2>	 d'accord
327 spk1>	 d'accord
328 spk2>	 merci beaucoup
329 spk1>	 je vous en prie
330 spk2>	 merci
331 spk2>	 au+revoir
332 spk1>	 bonne journée à vous
333 spk1>	 au monsieur

