
DIALOG=20091112_RATP_SCD_0344

001 spk1>	 NNAAMMEE va vous répondre
002 spk1>	 NNAAMMEE bonjour
003 spk2>	 bonjour je voudrais savoir comment faire pour aller de Porte+de+Versailles à Porte+d'Auteuil s'il+vous+plaît
004 spk1>	 oui donc de Porte+de+Versailles à Porte+d'Auteuil en bus en métro ou en
005 spk2>	 bah ce+qui est le+mieux en bus je pense
006 spk1>	 d'accord vous pas
007 spk2>	 si y a pas de tramway
008 spk1>	 je vais regarder un instant
009 spk1>	 s'il+vous+plaît
010 spk2>	 oui
011 spk1>	 donc vous allez prendre le tramway le T trois
012 spk2>	 T trois
013 spk1>	 oui en direction du Pont+du+Garigliano
014 spk2>	 oui
015 spk1>	 vous allez jusqu' au terminus
016 spk2>	 oui
017 spk1>	 et vous récupérez le PC un
018 spk2>	 PC un
019 spk1>	 en direction de la Porte+de+Champerret
020 spk2>	 oui
021 spk1>	 et vous descendez à la Porte+d'Auteuil
022 spk2>	 et PC un c' est un un bus
023 spk1>	 un bus oui
024 spk2>	 d'accord merci beaucoup
025 spk1>	 je vous en prie bonne journée au+revoir
026 spk2>	 également

