
DIALOG=20091112_RATP_SCD_0763

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour madame
003 spk2>	 je vous appelle au+sujet+de la ligne B du RER
004 spk1>	 oui
005 spk2>	 sur votre site+internet vous annoncez trois trains sur quatre
006 spk1>	 d'accord
007 spk2>	 euh ce matin c' était loin de euh d' être le cas
008 spk1>	 d'accord
009 spk2>	 voir pour ce soir ce+qu' il faut faire pour pouvoir prendre un avion à+temps à Orly+Sud
010 spk1>	 d'accord
011 spk2>	 à Orly+Ouest
012 spk2>	 pardon
013 spk1>	 Orly+Ouest donc dans+tous+les+cas vous souhaitez savoir si ce soir vous pourrez emprunter le RER+B est-ce+que la situation va s' améliorer c' est ça monsieur
014 spk2>	 voilà oui parce+que trois trains sur quatre normalement c' est bon mais ce matin c' était impossible
015 spk1>	 d'accord vous patientez un instant je m' informe monsieur
016 spk2>	 d'accord merci
017 spk1>	 monsieur
018 spk2>	 oui
019 spk1>	 pour ce soir nous n' avons pas plus de précisions hein mis à+part les trois trains sur quatre hein euh si la situation se débloque hein dans+ces+cas-là vous pourrez bien+sûr renouveler votre appel pour+qu' on puisse vous confirmer mais euh ce n' est pas le cas là actuellement je pense que ce soir ça vous risquez encore d' avoir trois trains sur quatre hein en+moyenne hein et trois trains sur quatre c' est vrai que c' est vraiment en heures de pointe hein
020 spk2>	 bah ce matin à euh disons à huit heures et demie euh entre Antony et Bourg-la-Reine
021 spk1>	 oui
022 spk2>	 euh il y a eu euh deux trains en une heure
023 spk1>	 ah oui effectivement donc là effectivement
024 spk2>	 bah voilà
025 spk1>	 il y avait pas
026 spk1>	 trois trains sur quatre
027 spk2>	 non justement
028 spk1>	 hein oui
029 spk2>	 c' est pour ça que je vous dis ce+qu' il y a sur le site bon
030 spk1>	 oui c' est pas tout+à+fait
031 spk2>	 euh bon
032 spk1>	 ce+qui est euh réel
033 spk2>	 non
034 spk2>	 non non
035 spk1>	 d'accord
036 spk1>	 euh ceci dit c' est vrai que pour ce soir nous n' avons pas+encore les précisions les prévisions hein à savoir s' il y aura plus de trains ou pas
037 spk2>	 bon
038 spk1>	 la seule information ce serait trois trains sur quatre hein et+encore il faudrait qu' ils soient respectés d'+après ce+que vous me dites c' est pas tout+à+fait ça
039 spk2>	 d'accord bon+bah on va se retourner vers le la solution taxi alors
040 spk1>	 hein parce+que vous dans+tous+les+cas c' est pour rejoindre l' aéroport
041 spk1>	 c' est ça
042 spk2>	 oui absolument oui
043 spk1>	 et vous partirez d'où exactement monsieur
044 spk2>	 de de Paris de
045 spk1>	 de quel côté de Paris parce+que sans ça l' aéroport vous pouvez aussi+bien le faire en bus hein
046 spk2>	 c' est Franklin+Roosevelt
047 spk1>	 alors du+côté+de Franklin+Roosevelt je recherche hein
048 spk2>	 rue de Invalides
049 spk1>	 votre itinéraire
050 spk2>	 éventuellement
051 spk1>	 les Invalides alors du côté des Invalides un instant je recherche un autre itinéraire monsieur
052 spk2>	 d'accord
053 spk1>	 monsieur
054 spk2>	 hm
055 spk1>	 c' est
056 spk1>	 pour rejoindre Orly+Ouest hein
057 spk1>	 c' est ça
058 spk2>	 Orly+Ouest
059 spk2>	 oui
060 spk1>	 d'accord parce+que des Invalides hein c' est vrai que vous avez aussi+bien les cars Air+France qui circulent depuis les Invalides
061 spk2>	 c' est ça oui
062 spk1>	 et ils vous emmènent du+côté+d' Orly+Sud hein ou Orly+Ouest même et ils partent des Invalides vous avez aussi+bien cette solution c' est les cars Air+France
063 spk2>	 d'accord
064 spk2>	 très+bien
065 spk2>	 bon+bah euh on va voir c' est soit ça soit les taxis
066 spk1>	 parce+que au+niveau+des horaires pour pour y être vers quelle heure vous me dites
067 spk2>	 euh c' est euh vers j' ai plus l' heure sous les yeux c' est euh dix-neuf heures trente
068 spk1>	 dix-neuf heures trente c' est l' heure euh de d' enregistrement des bagages
069 spk2>	 oui c' est ça
070 spk1>	 ou euh
071 spk1>	 alors donc il faudrait que vous y soyez un+peu plus tôt
072 spk1>	 vers dix-huit heures
073 spk2>	 oui bien+sûr
074 spk1>	 je dirais c' est ça
075 spk2>	 oui enfin dix-neuf heures c' est bon
076 spk1>	 d'accord alors si dix-neuf heures c' est bon dans+ces+cas-là vous avez un départ prévu au+niveau+de des cars Air+France à dix-huit heures vingt et là ça sera arrivée à dix-huit heures cinquante-cinq sans ça celui qui est juste avant il est prévu à dix-sept heures cinquante arrivée prévue à dix-huit heures vingt-cinq
077 spk2>	 d'accord bah c' est très bien ça
078 spk2>	 entendu
079 spk2>	 eh+bien je vous remercie
080 spk1>	 je vous en prie monsieur bonne fin de journée au+revoir
081 spk2>	 au+revoir madame

