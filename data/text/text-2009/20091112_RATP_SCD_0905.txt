
DIALOG=20091112_RATP_SCD_0905

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 bonjour monsieur
004 spk3>	 pourriez vous me dire s'il+vous+plaît pour aller rue de Reuilly je voudrais aller au trente rue de Reuilly quel est le métro qu' il faudrait que je prenne
005 spk2>	 alors c' est dans quel arrondissement madame
006 spk3>	 je suis écoutez je suis à la Porte+de+Vincennes
007 spk2>	 alors Porte+de+Vincennes pour vous rendre au trente rue de Reuilly mais c' est sur quel arrondissement la r() rue de Reuilly
008 spk3>	 ben écoutez je je pense que c' est dans le douzième
009 spk2>	 d'accord
010 spk2>	 donc vous voulez connaître la station de métro
011 spk2>	 la plus proche hein il y a pas de problème
012 spk3>	 c' est ça oui la plus proche
013 spk2>	 patientez je regarde merci
014 spk3>	 merci
015 spk2>	 s'il+vous+plaît madame
016 spk3>	 oui
017 spk2>	 alors il faudra descendre à la station Reuilly-Diderot sur la ligne huit ou une
018 spk3>	 oui et il y a une sortie vous n() enfin vous ne savez si il y a une sortie rue rue de Reuilly
019 spk2>	 alors que je regarde tout+de+suite
020 spk3>	 parce+que je effectivement la sortie euh Reuilly-Diderot il y a beaucoup+de sorties je sais et quelquefois
021 spk2>	 normalement vous avez euh parce+que vous avez une sortie euh de chaque côté du boulevard Diderot
022 spk3>	 oui
023 spk2>	 hein
024 spk2>	 donc euh de+toutes+façons la rue du Diderot la rue la la rue de Reuilly elle est à l' intersection et tombe au+niveau+des sorties de bouches de métro
025 spk3>	 bon de de des bouches de
026 spk3>	 métro
027 spk2>	 ouais
028 spk2>	 donc si vous sortez de+toutes+façons euh sur la mauvaise
029 spk2>	 euh le mauvais côté vous avez juste le boulevard à traverser
030 spk2>	 parce+que je vous dis les deux les s() les bouches de métro elles sont à l' intersection du boulevard Diderot et de la rue de Reuilly
031 spk3>	 oui
032 spk3>	 bon très+bien
033 spk2>	 je vous en prie madame
034 spk3>	 je vous remercie
035 spk2>	 bonne journée au+revoir
036 spk3>	 au+revoir monsieur

