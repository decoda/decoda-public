
DIALOG=20091112_RATP_SCD_0374

001 spk1>	 NNAAMMEE va vous répondre
002 spk1>	 NNAAMMEE bonjour
003 spk2>	 bonjour madame madame s'il+vous+plaît c' est pour un renseignement je viens de recevoir une euh ma carte personnel RATP retraité
004 spk1>	 oui
005 spk2>	 oui et+alors on me met je ne sais pas ce+que c' est ça avec mon ancien matricule
006 spk1>	 oui
007 spk2>	 et là on me met que si je veux ma carte de retraité il faut que je fasse un courrier
008 spk1>	 oui
009 spk2>	 alors je comprends plus rien parce moi je l' ai ma carte de retraité
010 spk1>	 ah ah se seraient ils trompés donc là vous venez de recevoir votre carte euh RATP retraité
011 spk2>	 non là j' ai reçu une alors c' est
012 spk1>	 elle n' est plus valable non
013 spk2>	 non elle est valable mais là maintenant je reçois une carte avec mon matricule quand j' étais en activité
014 spk1>	 d'accord d'accord
015 spk2>	 voilà et+alors on me met exactement le renouvellement de votre carte de retraité se fera par demande écrite de votre part mais qu'est-ce+que ça veut dire je
016 spk1>	 ça vous
017 spk2>	 moi je l' ai ma carte de retraité
018 spk1>	 alors votre carte euh de retraité RATP est valable dix ans donc au+bout+de ces dix années
019 spk2>	 oui mais il n' y a pas de de
020 spk1>	 ce sera à vous de euh demander oui oui allez -y il n' y a pas de date
021 spk2>	 oui mais excusez -moi madame sur ma carte de retraité il n' y a pas de date non
022 spk2>	 j' ai regardé j' ai regardé j' ai regardé il n' y a rien du+tout alors je ne sais pas ce+que ça veut dire je ne sais pas ce+qu' il faut que je fasse
023 spk1>	 alors un un instant
024 spk2>	 moi je veux bien vous faire vous faire un courrier
025 spk1>	 oui oui non mais de+toute+façon ça ne sera pas à nous alors un instant s'il+vous+plaît je vais rechercher le service qui va pouvoir vous répondre
026 spk2>	 oh merci parce+que j' ai téléphoné au euh rue Carmot
027 spk1>	 oui
028 spk2>	 à Fontenay-sous-Bois
029 spk1>	 oui
030 spk2>	 et+puis c' était un monsieur qui me dit mais madame il me dit il faut téléphoner à Championnet nous on ne sait pas+du+tout euh
031 spk1>	 oui oui tout+à+fait
032 spk2>	 voilà alors m' envoyez pas chez lui hein
033 spk1>	 non je ne vous enverrai pas chez lui
034 spk1>	 alors un instant s'il+vous+plaît
035 spk2>	 merci madame
036 spk1>	 madame
037 spk2>	 ah oui
038 spk1>	 oui alors je me suis je me suis renseigné
039 spk2>	 alors oui madame
040 spk2>	 oui madame
041 spk1>	 donc en+fait euh le courrier que vous avez reçu
042 spk2>	 oui
043 spk1>	 pour vous indiquer que le renouvellement ne se fera pas automatiquement
044 spk2>	 oui
045 spk1>	 donc là vous l' avez reçu cette semaine
046 spk2>	 oui mais ça c' est la carte de transport
047 spk1>	 oui oui tout+à+fait c' est la carte de transport où il y a marqué maintenant ils notent votre votre ancien matricule
048 spk2>	 effectivement madame
049 spk1>	 voilà
050 spk2>	 c' est exact
051 spk1>	 donc euh vous comptez le jour où vous l' avez reçu plus il y a dix ans
052 spk2>	 cette carte là
053 spk1>	 oui tout+à+fait
054 spk2>	 oui mais ça c' est la carte de transport qu'est-ce+qu' ils bénéficient dire pour la carte de retraité
055 spk1>	 c' est celle-ci c' est la carte de retraité transport
056 spk2>	 ah c' est elle elle elle fait les deux
057 spk1>	 c' est votre carte de service en+fait
058 spk1>	 oui oui tout+à+fait
059 spk2>	 alors je n' ai plus mon alors et mon matricule de retraité alors il sert à quoi
060 spk1>	 alors le numéro de pension n' est plus sur la carte c' est vraiment votre euh votre matricule euh d' agent actif quand vous étiez active
061 spk2>	 bon alors cette carte là reprise euh quatre onze et+alors elle est valable jusqu'+en deux+mille+dix-neuf
062 spk1>	 euh oui
063 spk2>	 celle que vous m' avez envoyé
064 spk1>	 oui tout+à+fait
065 spk2>	 voilà
066 spk1>	 tout+à+fait madame
067 spk2>	 alors maintenant alors j' ai alors je n' ai pas de courrier à faire j' ai rien à faire
068 spk1>	 ah non là vous n' avez rien à faire pendant dix ans vous n' avez rien à faire par+contre c' est vrai que faudra bah quand vous verrez qu' elle fonctionne plus c' est que c' est à+ce+moment-là que faudra faire un courrier parce+qu' elle n' est pas reconductible
069 spk2>	 et un courrier chez vous à Championnet
070 spk1>	 oui tout+à+fait madame
071 spk2>	 alors bon+bah maintenant je ne tiens pas compte que vous me dites qu' il faut que je fasse un courrier en demandant ma carte de retraité
072 spk1>	 non non non c' est vrai que ce n' est pas être explicite hein
073 spk2>	 ah+bon
074 spk1>	 j' avoue hein
075 spk2>	 alors je ne fais plus rien alors oui mais c' est ma carte de dix ans alors alors l' ancienne
076 spk1>	 hm+hm
077 spk2>	 euh je passe plus je passe je peux toujours voyager avec et le jour qu' elle passera pas j' en prendrai une nouvelle
078 spk1>	 ah non mais maintenant vous vous prenez la nouvelle
079 spk2>	 ah+bon
080 spk1>	 comme+ça vous aurez aucun souci
081 spk2>	 ah+bon d'accord
082 spk1>	 comme+ça au+cas+où elle passait pas vous le saurez tout+de+suite
083 spk2>	 ah+bon d'accord
084 spk1>	 voilà ah non non passez si vous voulez passez les deux vous verrez laquelle ne fonct() laquelle fonctionne et euh il y en aura une des deux normalement qui qui sera désactivée
085 spk2>	 ah+bon
086 spk1>	 oui oui oui
087 spk2>	 enfin maintenant je n' ai pas de courrier je n' ai rien à faire on on va
088 spk1>	 non juste à profiter de la vie
089 spk1>	 voilà
090 spk2>	 parce+que
091 spk2>	 parce+que je savais pas+du+tout ce+qu' il fallait faire
092 spk1>	 oui oui bah vous avez bien fait d' appeler hein
093 spk2>	 voilà bon et bien je vous remercie infiniment madame
094 spk1>	 mais je vous en prie madame
095 spk2>	 allez je vous souhaite une très bonne journée
096 spk1>	 merci
097 spk2>	 au+revoir madame au+revoir
098 spk1>	 au+revoir

