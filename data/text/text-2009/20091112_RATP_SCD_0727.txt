
DIALOG=20091112_RATP_SCD_0727

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 oui bonjour monsieur euh
004 spk3>	 j' aurais voulu avoir un renseignement sur les conditions de trafic sur la ligne B du RER
005 spk2>	 hm d'accord la partie RATP hein
006 spk3>	 la partie RATP oui en+fait c' est pour aller de de Saint-Denis euh jusqu'à Bourg-la-Reine
007 spk2>	 ah d'accord donc euh une partie RATP et SNCF Saint-Denis on est dans la partie SNCF
008 spk3>	 d'accord jusqu'à la Gare+du+Nord
009 spk2>	 jusqu'à la Gare+du+Nord et euh ensuite on passe euh sur le réseau RATP
010 spk3>	 d'accord et+alors donc euh
011 spk2>	 hein euh je vais vous dire ça vous pouvez patienter
012 spk3>	 oui oui
013 spk2>	 merci
014 spk2>	 oui madame donc sur la RATP ça sera trois sur quatre
015 spk3>	 oui
016 spk2>	 euh et sur la SNCF euh c' était deux sur trois
017 spk2>	 deux sur trois sur la SNCF et+alors euh
018 spk3>	 euh pour aller de hum de la Gare+du+Nord à Danfer
019 spk3>	 est-ce+qu' il y a un
020 spk2>	 oui
021 spk3>	 des trains ou est-ce+que c' est interrompu
022 spk2>	 alors vous avez euh le métro sinon hein
023 spk2>	 alors
024 spk3>	 oui mais c' est à dire euh
025 spk3>	 le le RER est-ce+qu' il euh circule ou pas
026 spk2>	 oui il va circuler p() perturbé mais il va circuler
027 spk3>	 ah d'accord il y en a quand+même
028 spk3>	 d'accord ça c' est pas totalement interrompu
029 spk2>	 ah oui il y en a nan nan pas+du+tout
030 spk3>	 d'accord OK très+bien
031 spk3>	 bon je vous remercie
032 spk2>	 je vous en prie madame
033 spk3>	 au+revoir monsieur
034 spk2>	 au+revoir

