
DIALOG=20091112_RATP_SCD_0502

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour euh je vous appelle parce+que je suis enseignante et je vais venir euh prendre un bus alors je sais pas s' il faut vous appeler mais bon je le fais euh on va prendre le cent+quatre-vingt-un euh à l' avenue des petites haies à Créteil
003 spk1>	 oui
004 spk2>	 voilà
005 spk1>	 vous êtes combien
006 spk2>	 euh vingt-trois élèves
007 spk1>	 alors avenue des petites haies vous patientez
008 spk2>	 oui
009 spk1>	 merci
010 spk1>	 c' est pour maintenant
011 spk2>	 c' est pour euh treize heures euh on va prendre le cent+quatre-vingt-un qui est à quatorze heures trois à l' avenue des petites haies
012 spk1>	 d'accord un instant
013 spk2>	 et on va faire le retour aussi
014 spk1>	 oui
015 spk1>	 hm
016 spk1>	 ne quittez pas
017 spk2>	 merci
018 spk1>	 bon alors
019 spk3>	 oui euh
020 spk3>	 hum euh École+Vétérinaire bonjour
021 spk1>	 maison voilà
022 spk1>	 bonjour
023 spk1>	 c' est
024 spk1>	 le Service+Clientèle+à+Distance
025 spk3>	 oui
026 spk1>	 j' ai une euh
027 spk3>	 bonjour
028 spk1>	 m() une maîtresse en ligne qui me dit qu' elle va prendre le cent+quatre-vingt-un avenue des petites haies avec vingt-trois élèves vers treize heures
029 spk3>	 vers treize heures
030 spk1>	 oui
031 spk3>	 oui
032 spk1>	 c' était pour vous prévenir
033 spk3>	 vers treize heures d'accord OK ben je vais voir avec le collègue euh qui qui passe à cette heure là alors je vais le prévenir alors
034 spk1>	 d'accord
035 spk1>	 parce+que vers treize
036 spk3>	 direction
037 spk3>	 direction Gaîté hein ou École+Vétérinaire c' est ça hein tu m' as dit je
038 spk1>	 avenue
039 spk3>	 ou pas
040 spk1>	 des petites haies
041 spk1>	 mais elle m' a pas dit
042 spk3>	 direction
043 spk1>	 d() attends je lui demande
044 spk3>	 d'accord
045 spk1>	 s'il+vous+plaît
046 spk2>	 oui
047 spk1>	 c' est euh quelle direction vous le prenez
048 spk2>	 on va à Créteil-Préfecture
049 spk1>	 Préf d'accord et c' est v()
050 spk1>	 treize heures pile
051 spk2>	 et le retour
052 spk2>	 c' est le bus euh je suis allée vérifier les horaires là
053 spk1>	 oui
054 spk2>	 quatorze heures
055 spk2>	 trois il passe
056 spk1>	 ah
057 spk2>	 à
058 spk2>	 l' avenue des petites haies
059 spk1>	 quatorze heures trois d'accord et pour le
060 spk1>	 retour
061 spk2>	 et en
062 spk2>	 eh+ben le retour c' est celui de euh seize heures euh sept je crois parce+que quarante-neuf on n' arrivera pas à l' avoir
063 spk1>	 à la Préfecture
064 spk2>	 quinze heures quarante-neuf
065 spk1>	 d'accord
066 spk2>	 ouais
067 spk1>	 je vais leur dire
068 spk1>	 allo
069 spk3>	 oui
070 spk1>	 oui
071 spk1>	 c' est celui de quatorze heures zéro+trois qui passe avenue des petites haies en direction de Préfecture
072 spk3>	 quatorze heures zéro+trois en direction de Préfecture
073 spk3>	 alors d'accord
074 spk1>	 et cel()
075 spk1>	 pour le retour ils vont p() sûrement prendre à Préfecture celui de seize heures zéro+sept
076 spk3>	 après donc retour seize heures zéro+sept direction d'accord OK
077 spk1>	 OK
078 spk3>	 c' est noté
079 spk3>	 je je vais
080 spk1>	 merci
081 spk3>	 prévenir le machiniste hein
082 spk1>	 d'accord merci au+revoir
083 spk3>	 merci au+revoir
084 spk1>	 s'il+vous+plaît
085 spk2>	 oui
086 spk1>	 oui c' est bon c' est prévenu
087 spk2>	 OK très+bien bon+ben je vous remercie
088 spk1>	 merci beaucoup
089 spk2>	 c' est nécessaire
090 spk1>	 au+revoir
091 spk2>	 ou pas ou euh
092 spk1>	 oui parce+qu' ils vont prévenir les collègues
093 spk2>	 ouais voilà ils seront pas surpris de nous voir
094 spk2>	 débarquer
095 spk1>	 voilà
096 spk1>	 ils préviennent les
097 spk2>	 merci
098 spk1>	 machinistes
099 spk2>	 ça marche merci
100 spk1>	 je vous en prie au+revoir
101 spk2>	 au+revoir madame

