
DIALOG=20091112_RATP_SCD_0333

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 allo
003 spk1>	 oui bonjour
004 spk2>	 allo vous m' entendez oui bonjour madame je suis assistante sociale sur l' hôpital de Maison+Blanche
005 spk1>	 oui
006 spk2>	 et j' aurais voulu avoir le le service qui s' occupe des objets trouvés ou perdus enfin
007 spk1>	 c' est moi
008 spk2>	 c' est vous
009 spk1>	 dites -moi tout
010 spk2>	 ah donc il y a un type qui est hospitalisé qui est arrivé en voyage pathologique il a tout perdu
011 spk1>	 oui
012 spk2>	 donc je voulais savoir si vous aviez pièce d' identité et euh carte de retrait donc je vous donne son nom
013 spk1>	 oui
014 spk2>	 monsieur NNAAMMEE
015 spk1>	 donc l' orthographe du nom c' est NNAAMMEE il a perdu quand où comment
016 spk2>	 alors donc il s' est retrouvé à l' hôpital le sept novembre donc il l' a perdu peut-être entre le six et le sept
017 spk1>	 d'accord donc je vais regarder c' était dans une sacoche quelque+chose
018 spk2>	 alors il dit avoir deux valises Deslay un sac à dos euh un sac à dos euh
019 spk1>	 donc c' est
020 spk2>	 euh voilà
021 spk1>	 c' est dans des valises
022 spk2>	 bah il a voilà
023 spk1>	 alors alors ce+que j' ai besoin de savoir alors valise
024 spk2>	 une valise et un sac à dos bleu y a une valise Deslay à roulettes avec
025 spk1>	 alors une valise sac à dos en+dehors des papiers d' identité il y avait autre+chose à l' intérieur
026 spk2>	 bah il devait y avoir des vêtements
027 spk1>	 des vêtements il a perdu où sur quelle ligne de métro
028 spk2>	 bah justement il est incapable de il a une perte de faculté alors euh
029 spk1>	 oh+mince
030 spk2>	 ouais bah oui c' est
031 spk1>	 euh
032 spk2>	 je vais pas vous faciliter la tâche hein quand+même
033 spk1>	 non
034 spk2>	 bah impossible de savoir où+est-ce+qu' il il a dû errer dans le métro à mon avis faire aller à+droite à+gauche
035 spk1>	 oui
036 spk2>	 euh
037 spk1>	 bon là ce+que je vais faire déjà c' est que je vais regarder euh donc euh je vais mettre avant six semaines donc alors à+peu+près aux aux alentours du cinq jusqu'à aujourd'hui voir si en tapant son nom j' ai quelque+chose qui ressort
038 spk2>	 c' est jusqu' au sept en+tout+cas ça commence à l' hôpital le sept au matin
039 spk1>	 il s' est retrouvé le sept
040 spk2>	 donc c' est euh voilà c' est la veille ou le jour
041 spk1>	 bon d'accord donc je vais regarder entre le cinq et sept voir si j' ai quelque+chose qui ressort je vais vérifier sur l' ensemble du réseau voir si il y a quelque+chose à prendre vous patientez un instant
042 spk2>	 oui merci
043 spk1>	 merci
044 spk1>	 madame s'il+vous+plaît
045 spk2>	 allo
046 spk1>	 en tapant son nom j' ai rien qui ressort
047 spk2>	 y a rien
048 spk1>	 euh ouais donc il vous faudrait écoutez s' ils ont pas tapé son identité ils ont tapé le descriptif des valises ou des sacs à dos il faudrait que je sache au+moins si c' est sur le métro le bus RER peut-être pas le nom des lignes de métro ni de bus mais au+moins euh
049 spk2>	 il a parlé d' Odéon à un moment mais
050 spk1>	 Odéon parce+que là quand je tape si je regarde uniquement sur métro j' ai plus de cinq cent objets donc euh
051 spk2>	 ah oui ça j' en doute pas un instant
052 spk1>	 hein hein hein
053 spk1>	 malheureusement
054 spk2>	 hm
055 spk1>	 donc pour pouvoir vous vous renseigner faudrait que ce soit il se souvient pas+du+tout
056 spk2>	 non
057 spk1>	 y a rien qui lui revient
058 spk2>	 alors peut-être que demain il sera plus euh
059 spk1>	 bah au+moins pour+qu' on puisse cibler juste la recherche sur euh le type de transport qu' il a pris en fonction de ça si on sait que si c' est que le métro on regardera à+peu+près par+rapport+au trajet qu' il a effectué même pas forcément on n' a pas besoin de savoir les lignes mais là où il est parti
060 spk2>	 parce+que en+fait il arrivait de il arrivait de Dijon alors c' est par la ligne sur quel métro après quand on arrive de Dijon
061 spk1>	 honnêtement
062 spk2>	 alors là c' est gare de
063 spk1>	 euh Gare+de+l'Est ou Gare+du+Nord je ne sais pas honnêtement
064 spk2>	 savoir par quelle ligne il aurait pu arriver
065 spk1>	 oui oui
066 spk2>	 par le train
067 spk1>	 bah il faudrait au+moins juste ça savoir quel partir d'où pour arriver où comme+ça on refera l' itinéraire hein et on regardera sur son trajet ce+qu' il a emprunté comme transport à+peu+près
068 spk2>	 d'accord bon+bah
069 spk1>	 ce serait le+mieux comme+ça on regardera à+nouveau quoi
070 spk2>	 hm+hm sinon c' est ouais
071 spk1>	 là c' est trop vaste hein je suis vraiment désolée
072 spk2>	 ah+bah non on va essayer de chercher autrement hein donc j' essaye d' avoir plus+de renseignements et+puis je vous je vous rappelle je ne vous demande pas vous peut-être un autre ou
073 spk1>	 vous refaites le trente-deux+quarante-six et on revérifiera
074 spk2>	 d'accord merci beaucoup
075 spk1>	 je vous remercie bonne journée
076 spk1>	 au+revoir
077 spk2>	 au+revoir

