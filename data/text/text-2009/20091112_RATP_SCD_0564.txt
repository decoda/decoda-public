
DIALOG=20091112_RATP_SCD_0564

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour
003 spk2>	 voilà je dois me rendre au deux rue Édouard+Colonne dans le premier arrondissement et je suis sur la Porte+de+Vanves dans le quatorzième
004 spk1>	 Édouard
005 spk2>	 colonne
006 spk1>	 ah oui d'accord
007 spk1>	 et vous êtes à porte de
008 spk2>	 Porte+de+Vanves
009 spk1>	 Porte+de+Vanves
010 spk2>	 dans le quatorzième
011 spk2>	 hm
012 spk1>	 quel que soit le mode de transport
013 spk2>	 oui
014 spk1>	 d'accord je vous fais patienter
015 spk2>	 oui merci
016 spk1>	 s'il+vous+plaît
017 spk2>	 oui
018 spk1>	 donc vous allez prendre le métro ligne treize direction Saint-Denis-Université
019 spk2>	 oui
020 spk1>	 jusqu'à Champs-Élysées-Clemenceau
021 spk2>	 oui
022 spk1>	 et là Champs-Élysées-Clemenceau vous allez prendre la ligne une direction Château+de+Vincennes
023 spk2>	 oui
024 spk1>	 et vous descendrez à Châtelet
025 spk1>	 et vous avez euh une sortie
026 spk2>	 d'accord
027 spk1>	 qui donne sur euh à l' angle de la rue de Rivoli et la rue de la Colonne
028 spk2>	 d'accord OK OK donc je reprends la une alors OK d'accord OK je descends à Châtelet d'accord
029 spk1>	 voilà
030 spk2>	 merci
031 spk1>	 bonne journée
032 spk2>	 bonne journée à vous
033 spk2>	 au+revoir
034 spk1>	 merci au+revoir

