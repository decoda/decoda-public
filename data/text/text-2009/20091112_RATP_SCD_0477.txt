
DIALOG=20091112_RATP_SCD_0477

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 attends
003 spk3>	 NNAAMMEE bonjour
004 spk2>	 oui bonjour madame euh je voudrais me renseigner sur le trafic de de l' autobus quatre-vingts pendant le week-end parce+que il y a une manifestation culturelle à Montmartre et il me semble me rappeler que souvent le le dans+ce+cas-là il le quatre-vingts ne va pas jusqu'à Montmartre est-ce+que vous avez des informations
005 spk3>	 c' est pour ce week-end vous patientez je regarde
006 spk2>	 merci
007 spk3>	 s'il+vous+plaît
008 spk2>	 oui
009 spk3>	 oui alors effectivement pour le dimanche pour l' instant j' ai que pour le dimanche
010 spk2>	 oui
011 spk3>	 euh il y aura sûrement une déviation entre neuf heures et demie et onze heures et demie pour le quatre-vingts c'est-à-dire il va s' arrêter sans+doute à Clichy j' imagine
012 spk3>	 je
013 spk2>	 ou quelque+chose comme+ça
014 spk2>	 oui parce+que
015 spk3>	 ben même peut-être pas Clichy parce+que justement là la
016 spk2>	 à Saint-Lazare alors
017 spk3>	 ça moi j' ai euh c' est une course pédestre que j' ai pour le dimanche dans le neuvième et elle passe par la Place+de+Clichy rue rue Saint-Lazare
018 spk2>	 et+puis en+plus
019 spk3>	 donc ça risque vraiment
020 spk2>	 il y a le une manifestation culturelle qui s' appelle d'+Anvers+aux+Abbesses
021 spk3>	 ouais ça risque d' être coupé
022 spk2>	 je d'accord
023 spk2>	 je vous remercie
024 spk3>	 je vous en prie
025 spk3>	 au+revoir
026 spk2>	 au+revoir

