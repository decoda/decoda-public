
DIALOG=20091112_RATP_SCD_1341

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour
003 spk2>	 je vous appelle parce+que j' ai ligne B du RER
004 spk1>	 euh vous voudriez savoir je j' ai pas bien entendu c' était coupé madame
005 spk2>	 je voulais savoir s' il y aurait des grèves demain sur la ligne B du RER
006 spk1>	 s' il y a un trafic pour demain sur le RER+B
007 spk2>	 oui
008 spk1>	 ah
009 spk2>	 c' est ça
010 spk1>	 alors à l' heure actuelle nous n' avons pas d' information madame pour savoir si la grève elle a été reconduit donc là
011 spk2>	 vous savez pas
012 spk1>	 je ne peux pas vous dire
013 spk1>	 non
014 spk2>	 est-ce+que vous pouvez me dire si il y a des trains qui viennent de Robinson vers Bourg-la-Reine
015 spk1>	 si il y a un train qui vient de
016 spk2>	 Robinson
017 spk1>	 non là je n' ai pas d' horaires tant+que le service est perturbé est perturbé nous n' avons pas+du+tout d' horaires
018 spk2>	 euh donc vous n' avez pas d' information sur la grève en+fait
019 spk1>	 comment
020 spk2>	 vous n' avez pas d' information sur la grève
021 spk1>	 non je n' ai pas d' information madame
022 spk2>	 bon merci
023 spk1>	 mais je vous en prie bonne soirée madame au+revoir

