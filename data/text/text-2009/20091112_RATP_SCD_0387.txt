
DIALOG=20091112_RATP_SCD_0387

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour euh je voudrais aller à Michel-Ange-Auteuil
003 spk1>	 oui
004 spk2>	 et je passe j' ai la possibilité de partir soit de la mairie de Saint-Ouen soit de la Porte+de+Clignancourt celui qui est le+plus rapide
005 spk1>	 alors soit vous partez de la mairie de Saint-Ouen ou Porte+de+Clignancourt ou et c' est pour aller à Michel-Ange
006 spk2>	 Auteuil
007 spk1>	 Michel-Ange-Auteuil très+bien
008 spk1>	 vous patientez un instant je me renseigne merci
009 spk2>	 je vous en prie
010 spk1>	 madame s'il+vous+plaît
011 spk2>	 oui
012 spk1>	 le trajet sera plus court si vous partez de la Porte+de+Saint-Ouen
013 spk2>	 Porte+de+Saint-Ouen il faut que j' aille jusqu'à la Porte+de+Saint-Ouen
014 spk1>	 ça met trente minutes oui trente minutes oui ah oui vous n' êtes pas à côté donc oui
015 spk2>	 mais si j' arrive à la mairie j' ai qu' une station pour la porte
016 spk1>	 exactement
017 spk2>	 donc je peux prendre
018 spk1>	 alors+que de la Porte+de+Clignancourt ça met quarante minutes
019 spk2>	 non non c' est bon alors je pars de la mairie de Saint-Ouen
020 spk1>	 alors à+ce+moment-là vous prenez de la mairie de Saint-Ouen
021 spk2>	 alors comment je fais
022 spk1>	 à+partir+de la mairie de Saint-Ouen
023 spk2>	 c' est en ligne directe
024 spk1>	 non vous emprunterez donc à la mairie de Saint-Ouen la ligne treize vous allez faire un un changement alors attendez je vais vous donner le temps exact voir ce+qui serait le+plus court
025 spk2>	 je n' ai pas les changements
026 spk1>	 au niveau là
027 spk1>	 vous mettrez trente-cinq minutes en+tout
028 spk2>	 oui c' est bon
029 spk1>	 vous changez à Miromesnil
030 spk2>	 je change où
031 spk1>	 Miromesnil
032 spk2>	 oui et je prends
033 spk1>	 et ensuite la ligne neuf
034 spk2>	 jusqu'à Michel-Ange-Auteuil
035 spk1>	 direction Pont+de+Sèvres jusqu'à Michel-Ange-Auteuil
036 spk2>	 merci beaucoup
037 spk1>	 je vous en prie bonne journée à vous

