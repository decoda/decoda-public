
DIALOG=20091112_RATP_SCD_0212

001 spk1>	 bonjour
002 spk2>	 oui bonjour je vous appelle parce+que j' habite à côté le cent+quatre-vingt-deux euh la la station Baignade et il y a pas de bus donc je voudrais savoir est-ce+qu' il y a une perturbation puisqu' il y a rien de sur la borne il y a rien de noté
003 spk1>	 alors cent+quatre-vingt-deux
004 spk2>	 ah euh pardon cent+quatre-vingts excusez -moi
005 spk1>	 oui cent+quatre-vingts oui tous+les bus du dépôt de Vitry dont fait partie le cent+quatre-vingts
006 spk2>	 oui
007 spk1>	 ils sont en grève madame aujourd'hui donc toutes+les lignes sont perturbées sur la ligne sur le dépôt de Vitry
008 spk1>	 c' est pour ça
009 spk2>	 ah
010 spk2>	 d'accord et ça va durer toute+la journée
011 spk1>	 oh c' est prévu pour toute+la journée oui
012 spk2>	 ah d'accord parce+que comme ma fille elle prend le bus pour aller au collège don c' était pour savoir parce+qu' elle avait raté les cours il y en a eu un mais sur les bornes il y a marqué des+fois dix-huit et tout donc il faut pas tenir compte de ça
013 spk1>	 parce parce+que vous avez certaines lignes de bus par+exemple au+lieu+d' avoir un bus toutes+les cinq minutes ou toutes+les dix minutes vous en aurez un toutes+les demi-heures
014 spk1>	 donc alors
015 spk2>	 d'accord
016 spk1>	 normalement si la borne vous indique prochain bus dans une demi-heure c' est que il y a quand+même quelques bus qui roulent mais c' est très sérieusement perturbé quand+même d'accord
017 spk2>	 d'accord d'accord bon+ben parce+que dans les informations on n' a pas entendu quoi le bus c' est pour
018 spk2>	 ça
019 spk1>	 non ben
020 spk1>	 ça s' est décidé euh hier hier s() hier soir en+fait
021 spk2>	 ah d'accord
022 spk1>	 c' est dommage
023 spk2>	 d'accord
024 spk2>	 et là je voyais qu' il y a beaucoup+de gens qui attendent depuis le matin donc ils ont de l' espoir parce+que la borne elle marque dix-huit mais y a pas de bus le problème après
025 spk1>	 ben oui oui
026 spk2>	 après ça change
027 spk1>	 hum ben oui
028 spk2>	 donc c' est ça qui perturbe bon+ben je vous remercie
029 spk2>	 beaucoup hein
030 spk1>	 ben je vous en prie madame
031 spk2>	 bon bonne journée au+revoir
032 spk1>	 au+revoir
033 spk2>	 au+revoir

