
DIALOG=20091112_RATP_SCD_0743

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour euh
003 spk2>	 j' aurais besoin en+fait+d' avoir les justificatifs de mes chargements de passe+Navigo
004 spk2>	 de passe+Navigo hebdomadaire
005 spk1>	 oui
006 spk2>	 j' aurais voulu savoir il me semble que je suis obligée d' aller dans une agence
007 spk1>	 oui tout+à+fait
008 spk2>	 euh
009 spk1>	 vous êtes obligée d' aller dans une agence commerciale pour une
010 spk1>	 attestation
011 spk2>	 commerciale
012 spk1>	 de paiement
013 spk2>	 d'accord et vous savez dans quelle est-ce+que vous pouvez me dire où elle je p() j' en connais une uniquement à Montparnasse
014 spk1>	 oui vous êtes de quel côté
015 spk2>	 mais
016 spk2>	 ben moi je suis à Boulogne enfin je travaille à Opéra
017 spk1>	 oui
018 spk2>	 métro Opéra
019 spk1>	 vous en avez une à
020 spk2>	 ou Tuileries
021 spk1>	 Havre-Caumartin
022 spk2>	 Havre-Caumartin il y en a une
023 spk2>	 ah+bon
024 spk1>	 oui oui
025 spk2>	 d'accord ah+ben
026 spk2>	 c' était d'accord
027 spk2>	 très+bien
028 spk1>	 voilà c' est
029 spk1>	 à côté hein alors elle est ouverte de sept heures à dix-neuf heures trente
030 spk2>	 sept heures du matin
031 spk2>	 à dix-neuf heures trente
032 spk1>	 à dix-neuf heures trente
033 spk1>	 et le samedi de douze heures quinze à dix-huit heures quarante-cinq
034 spk2>	 d'accord de sept heures d() ah+ben je je j' ai jamais vu à Havre-Caumartin
035 spk1>	 elle est au niveau
036 spk2>	 très+bien
037 spk1>	 de la ligne trois dans le métro
038 spk2>	 OK
039 spk1>	 hein
040 spk2>	 d'accord et eux pourront également me donner
041 spk1>	 une attestation de paiement il y a pas de soucis
042 spk2>	 attestation de paiement et également parce+que du+coup je vais passer au Navigo euh hum annuel
043 spk1>	 ah+ben alors si vous voulez un a() alors vous amenez une pièce d' identité et un justificatif de domicile et là en+même+temps ils vous fabriquent le passe
044 spk2>	 d'accord
045 spk1>	 hein
046 spk2>	 OK
047 spk1>	 et un RIB
048 spk1>	 et de quoi payer la première mensualité
049 spk2>	 d'accord merci
050 spk1>	 je vous en prie bonne journée
051 spk1>	 au+revoir
052 spk2>	 merci au+revoir

