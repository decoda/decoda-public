
DIALOG=20091112_RATP_SCD_0524

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 bonjour euh
003 spk2>	 c' est je voudrais savoir euh je je dois être à onze heures moins le quart à Garges-lès-Gonesse à la gare enfin un+petit+peu après à au magasin Cora
004 spk1>	 oui
005 spk2>	 je sais pas où c' est
006 spk2>	 et je suis à la Porte+de+Saint-Cloud à quelle heure est-ce+que je dois partir à votre avis
007 spk1>	 d'accord donc vous a() partez de Porte+de+Saint-Cloud pour aller à Garges-lès-Gonesse au niveau juste à+côté+de la gare
008 spk2>	 RER+D pas très loin+de la gare oui
009 spk1>	 d'accord vous patientez je regarde vous devez y être
010 spk2>	 merci
011 spk1>	 à quelle heure
012 spk1>	 vous m' avez dit
013 spk2>	 oh un+peu avant onze heures onze heures moins le quart
014 spk1>	 hum d'accord euh c' est un jour en semaine
015 spk2>	 c' est demain
016 spk2>	 demain
017 spk1>	 demain
018 spk1>	 d'accord vous patientez je regarde
019 spk2>	 oui
020 spk1>	 s'il+vous+plaît
021 spk2>	 oui
022 spk1>	 oui donc
023 spk1>	 vous prenez la neuf le RER+A et le RER+D c' est ça pour aller à gare de
024 spk1>	 Garges
025 spk2>	 je j' en
026 spk2>	 sais rien c' est vous qui me dites
027 spk1>	 d'accord
028 spk1>	 il faut je pensais que vous juste les horaires non
029 spk1>	 c' est le trajet avec
030 spk2>	 ah non non
031 spk2>	 je sais même pas où est le RER+D je sais même pas ce+que c' est que le RER
032 spk1>	 d'accord
033 spk2>	 D euh
034 spk1>	 donc vous allez prendre la ligne neuf direction mairie de Montreuil
035 spk2>	 oui
036 spk1>	 vous faites un changement à Havre-Caumartin
037 spk2>	 oui
038 spk1>	 là vous récupérez le RER+A
039 spk2>	 oui
040 spk1>	 direction Marne-la-Vallée Boissy-Saint-Léger vous changez à Châtelet-Les+Halles
041 spk2>	 Châtelet
042 spk1>	 oui
043 spk2>	 d'accord
044 spk1>	 et à Châtelet vous prenez le RER+D
045 spk2>	 oui
046 spk1>	 direction Orry-la-Ville
047 spk2>	 Orry-la-Ville
048 spk1>	 voilà et vous
049 spk2>	 oui
050 spk1>	 descendez à la Gare+de+Garges-Sarcelles ça correspond à Garges-lès-Gonesse
051 spk2>	 gare
052 spk1>	 Garges Sarcelles
053 spk2>	 Garges Sarcelles
054 spk1>	 voilà c' est la gare qui est sur euh Garges-lès-Gonesse
055 spk2>	 d'accord
056 spk1>	 donc
057 spk1>	 pour être à onze heures euh
058 spk1>	 j' ai une arrivée à
059 spk2>	 moins le quart
060 spk2>	 au magasin Cora C O R A je sais même pas où c' est
061 spk2>	 j' ai pas+encore eu le temps de regarder sur internet
062 spk1>	 est-ce+que je vais l' avoir alors je suis pas sûre
063 spk1>	 je je n' ai pas le magasin Cora dans mon donc je sais pas par+rapport+à la gare si c' est loin ou pas euh j' ai une arrivée à dix heures dix-neuf à la gare de Garges ça irait
064 spk2>	 ah oui c' est
065 spk1>	 hm
066 spk2>	 très oui oui il y a il y a pas plus a arrivée dix heures dix-neuf
067 spk1>	 oui
068 spk2>	 il y a pas
069 spk2>	 il y a pas il y a pas plus+tard
070 spk1>	 si dix heures trente-quatre
071 spk2>	 ou dix heures
072 spk2>	 trente+quatre d'accord
073 spk1>	 alors pour
074 spk1>	 dix heures dix+neuf il faut prendre le RER+D à Châtelet à dix heures
075 spk2>	 oui non mais il faut que je parte de+chez moi à quelle heure du+coup
076 spk1>	 il faut partir aux alentours de neuf heures et quart neuf heures et quart d'accord départ
077 spk1>	 oui
078 spk2>	 d'accord et l'autre
079 spk2>	 euh si je pars à neuf heures et demie
080 spk1>	 han euh
081 spk1>	 ben ce sera pour dix heures trente-quatre d' arrivée
082 spk2>	 hm+hm départ euh neuf heures trente hum d'accord OK merci et+alors le samedi ça c' est pour le vendredi le samedi il faut que je sois à dix heures là-bas donc il faut partir à quelle heure
083 spk2>	 il faut que j' y sois
084 spk1>	 euh
085 spk2>	 à dix heures moins et des poussières quoi
086 spk1>	 d'accord ne quittez pas je vais regarder
087 spk1>	 s'il+vous+plaît
088 spk1>	 j' ai une
089 spk2>	 oui
090 spk1>	 arrivée à dix heures zéro+quatre
091 spk1>	 c' est trop tard
092 spk2>	 et c' est tout
093 spk1>	 euh
094 spk2>	 euh ah oui oui c' est un+peu tard oui
095 spk1>	 celle d'avant c' est neuf heures quarante+neuf d' arrivée donc il faut prendre euh il faut partir à neuf heures moins vingt
096 spk2>	 à huit heures trente
097 spk1>	 moui
098 spk1>	 d'accord
099 spk2>	 merci
100 spk2>	 beaucoup merci beaucoup bonne journée
101 spk1>	 je vous en prie bonne journée au+revoir
102 spk2>	 au+revoir madame

