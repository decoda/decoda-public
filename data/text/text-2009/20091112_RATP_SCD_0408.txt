
DIALOG=20091112_RATP_SCD_0408

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui
003 spk1>	 NNAAMMEE bonjour
004 spk2>	 oui bonjour monsieur excusez -moi de vous déranger j' attends le quatre+cent+quatre-vingt-sept et de euh pour aller jusqu'à Juvisy et il vient pas euh de il y en avait un prévu à
005 spk1>	 ouais
006 spk2>	 onze heures douze et il est pas passé et je comprends pas
007 spk2>	 très+bien
008 spk1>	 on a eu des
009 spk1>	 appels ce matin à ce sujet alors malheureusement même+s' il y a notre numéro sur le l' arrêt du bus
010 spk2>	 ouais
011 spk1>	 on renseigne
012 spk1>	 sur les horaires de ces bus par+contre en+cas+de perturbations on n' est pas gestionnaires de la ligne c' est la compagnie Athis+Cars qui gère les bus quatre cent quatre-vingt-sept
013 spk2>	 oui mais+alors il y en a
014 spk1>	 donc
015 spk2>	 a() euh on peut pas savoir
016 spk2>	 s' ils sont en grève
017 spk1>	 ben je vais vous donner
018 spk1>	 je vais vous donner leur numéro c' est un zéro+un en+plus hein donc ça vous coûtera moins cher
019 spk2>	 ouais
020 spk1>	 afin+de les appeler
021 spk1>	 hein parce+que
022 spk1>	 je n' ai pas
023 spk2>	 attendez
024 spk1>	 l' information
025 spk2>	 d'accord alors
026 spk1>	 je vous laisse prendre de+quoi noter
027 spk1>	 allez -y
028 spk2>	 ah+ben non je suis à l' arrêt alors
029 spk2>	 j' ai rien pour noter pour noter
030 spk1>	 vous êtes à l' arrêt tapez sur votre portable sinon
031 spk2>	 oui oui oui oui
032 spk1>	 ça enregistrera donc c' est zéro+un
033 spk2>	 allez -y attendez
034 spk2>	 allez -y
035 spk1>	 zéro+un
036 spk1>	 donc
037 spk2>	 zéro+un
038 spk1>	 soixante neuf
039 spk2>	 soixante neuf
040 spk1>	 zéro+cinq
041 spk2>	 zéro+cinq
042 spk1>	 treize
043 spk2>	 treize
044 spk1>	 cinquante-six
045 spk2>	 cinquante-six
046 spk1>	 tout+à+fait
047 spk2>	 merci monsieur
048 spk1>	 je vous en prie madame
049 spk1>	 au+revoir
050 spk2>	 au+revoir
051 spk1>	 au+revoir

