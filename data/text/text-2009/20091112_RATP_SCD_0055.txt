
DIALOG=20091112_RATP_SCD_0055

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour je suis un usager du bus quatre cent quatre-vingt-douze euh à la gare de Savigny-sur-Orge dans l' Essonne
003 spk1>	 oui
004 spk2>	 et euh depuis quelques temps nous avons des problèmes récurrents à+savoir+que le bus est en retard ou même carrément ne passe pas et c' est encore le cas ce matin il y a eu un bus de supprimé celui de de sept heures quarante-huit et là on attend celui de huit heures cinq et euh voilà
005 spk2>	 euh
006 spk1>	 ouais sauf
007 spk1>	 sauf+que la le bus quatre cent quatre-vingt-douze est il commandé par la société Athis+Cars
008 spk2>	 oui d'accord je sais oui
009 spk2>	 mais bon voilà euh
010 spk1>	 bah oui mais c' est c' est leur euh
011 spk1>	 c' est eux qui
012 spk1>	 qui font justement le je vous donne leur numéro de téléphone
013 spk2>	 alors vous pou() vous
014 spk2>	 ouais
015 spk1>	 c' est le zéro+un+soixante-neuf
016 spk2>	 hm
017 spk1>	 zéro+cinq
018 spk2>	 soixante-neuf+zéro+cinq
019 spk1>	 treize cinquante-six
020 spk2>	 OK je vous remercie
021 spk1>	 bonne journée
022 spk1>	 au+revoir
023 spk2>	 au+revoir

