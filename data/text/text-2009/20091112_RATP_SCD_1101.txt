
DIALOG=20091112_RATP_SCD_1101

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour
003 spk2>	 excusez -moi c' était Europe+Assistance euh je souhaiterais en+fait je sais pas si vous êtes à+même+de me répondre euh en+fait à envoyer un courrier à la RATP euh mais au service euh de la des personnes qui s' occupent du tramway euh T1 à sur Gennevilliers
004 spk1>	 oui c' est un courrier à quel sujet
005 spk2>	 et en+fait c' est un c' est un courrier d' information d' information parce+qu' on a fait des réclamations auprès+de la mairie concernant des désagréments qu' on a subis
006 spk1>	 d'accord
007 spk2>	 et c' est à titre en+fait+d' information on m' avait donné monsieur NNAAMMEE comme euh comme interlocuteur mais je sais pas si ils ont pas confondu avec une autre société on est au+niveau+du service technique de la voirie
008 spk1>	 ah non service technique
009 spk2>	 et
010 spk1>	 de la voirie c' est euh c' est c' est les communes qui gèrent
011 spk2>	 c' est les
012 spk1>	 ça
013 spk2>	 communes d'accord
014 spk1>	 les communes nous on on
015 spk2>	 et au+niveau+de la RATP
016 spk1>	 a dans certains centres bus euh des des responsables euh voirie
017 spk2>	 d'accord
018 spk1>	 hein mais ce sont des gens qui voient avec les services techniques ou avec les
019 spk2>	 d'accord
020 spk1>	 les élus communaux
021 spk1>	 euh les les les euh comment dirais -je les itinéraires de bus les déplacements d' arrêts les emplacements les
022 spk2>	 d'accord
023 spk1>	 euh voilà
024 spk1>	 c' est ils gèrent ça ensemble
025 spk2>	 et pour euh juste informer euh au+niveau+de la RATP le courrier qu' on a fait à notre mairie sur Gennevilliers il faut que je l' adresse à quelle adresse
026 spk1>	 alors
027 spk1>	 je vais vous
028 spk1>	 donner ça
029 spk2>	 ouais s'il+vous+plaît
030 spk1>	 alors vous allez écrire Service+Clientèle
031 spk2>	 alors Service+Clientèle
032 spk1>	 Centre+Bus Pavillons-sous-Bois
033 spk2>	 Centre+Bus euh Pavillons-sous-Bois
034 spk1>	 cent+trente-deux avenue de Rome
035 spk2>	 oui
036 spk1>	 quatre-vingt-treize+mille+trois+cent+vingt
037 spk2>	 oui
038 spk1>	 Pavillons-sous-Bois
039 spk2>	 d'accord et eux ils gèrent euh en+fait euh
040 spk1>	 pardon
041 spk2>	 ce secteur là
042 spk1>	 c' est eux qui gèrent le tramway
043 spk2>	 d'accord
044 spk2>	 bah je vous remercie beaucoup
045 spk1>	 je vous en prie
046 spk2>	 au+revoir
047 spk1>	 bonne soirée au+revoir
048 spk2>	 merci

