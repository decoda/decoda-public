
DIALOG=20091112_RATP_SCD_1068

001 spk1>	 bonjour NNAAMMEE bonjour
002 spk2>	 oui bonjour excusez -moi de vous déranger
003 spk2>	 est-ce+que je pourrais être mise en contact avec le centre Place+de+la+Boule s'il+vous+plaît
004 spk1>	 et c' est pour quelle raison madame
005 spk2>	 j' ai perdu perdu mes clés dans le trois+cent+quatre et c' est pour savoir si ils les avaient retrouvé si on leur avait donné
006 spk1>	 ah ah là-bas je vais pouvoir faire cette démarche c' est des clés que vous avez perdues aujourd'hui
007 spk2>	 oui entre quinze et dix-sept heures à+peu+près
008 spk1>	 d'accord quinze et dix-sept heures et vous me dites que c' est sur le trois+cent+quatre elles sont comment elles sont identifiables vos clés
009 spk2>	 euh en+fait c' est une clé c' est une grande clé parce+que c' est une porte sans souci
010 spk1>	 hm
011 spk2>	 il y a une
012 spk2>	 dessus il y a une carte Leclerc
013 spk2>	 un porte clé en forme de taureau avec marqué Espaa dessus
014 spk1>	 hm d'accord
015 spk2>	 une clé de boîte à lettres et des petites clés en genre de plastique voilà
016 spk1>	 hm d'accord je vais vous faire patienter un petit instant
017 spk2>	 je vous remercie
018 spk3>	 la Boule bonjour
019 spk1>	 oui bonjour c' est NNAAMMEE au Service+Clients+à+Distance j' ai en ligne une dame qui a pris le bus trois cent quatre dans le milieu de l' après-midi et elle a laissé un trousseau des clés est-ce+que vous auriez eu un signalement de cet OT
020 spk3>	 euh non non parce+que on m' a déjà demandé tout à l' heure j' ai passé une annonce et ça date depuis de cet après-midi on m' a rien rapporté du+tout
021 spk1>	 bon d'accord OK très+bien ben je te remercie
022 spk3>	 de rien au+revoir
023 spk1>	 bon courage au+revoir
024 spk1>	 s'il+vous+plaît madame
025 spk2>	 oui
026 spk1>	 donc concernant donc la ligne de bus trois+cent+quatre ils n' ont toujours pas le signalement de votre trousseau des clés hein
027 spk2>	 d'accord
028 spk1>	 ah non
029 spk2>	 OK je vous remercie
030 spk1>	 mais je vous prie
031 spk2>	 bonne fin de journée au+revoir
032 spk1>	 bonne fin de journée au+revoir

