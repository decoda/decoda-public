
DIALOG=20091112_RATP_SCD_0156

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour euh j' avais une question à vous poser parce+que je je prends la traverse de Charonne
003 spk2>	 tous
004 spk1>	 oui
005 spk2>	 tous+les matins pour emmener euh je le prends à hum aux Vignoles
006 spk1>	 hm
007 spk2>	 et euh je vais jusqu'à euh Schubert et en+fait je prends la correspondance à Albert+Marquet mais euh je voulais savoir parce+que moi j' utilise un ticket plus
008 spk1>	 hm
009 spk2>	 et euh est-ce+que je peux prendre la correspondance à Albert+Marquet sans changer de ticket
010 spk2>	 et euh
011 spk1>	 ben un ticket de bus est valable une heure et demie
012 spk2>	 oui mais eh c' est c' est pas ça c' est que entre les euh enfin entre certains chauffeurs ils tiennent pas le même discours donc euh il y en a où euh on va me dire euh ben que en+effet que j' utilise le même ticket sur une heure et demie
013 spk1>	 hm
014 spk2>	 alors
015 spk2>	 moi ce+que je fais j' utilise euh mon ticket à Vignole je le revalide à Albert+Marquet
016 spk1>	 hm
017 spk2>	 et ensuite euh je m' arrête à hum à Schubert je dépose mon fils à la crèche et là je réutilise le même ticket euh à Schubert pour euh pour reprendre euh pour retourner jusqu'à Vignole et euh alors il y a des jours ça passe et il y a d' autres jours où la machine elle veut pas elle v() veut pas valider
018 spk1>	 oui parce+que vous vous reprenez
019 spk1>	 vous
020 spk1>	 reprenez en face le même bus c' est ça
021 spk1>	 la même ligne
022 spk2>	 non même pas en face parce+qu' elle a ça fait une boucle en+fait euh
023 spk1>	 oui mais vous reprenez la même ligne
024 spk2>	 euh
025 spk2>	 oui
026 spk1>	 ouais vous pouvez pas
027 spk1>	 il faut prendre des bus différents madame c' est normal
028 spk2>	 ben oui mais bon ça me fait prendre trois tickets alors euh en+plus le soir je retourne le chercher ça me fait prendre trois autres tickets tickets ça me fait prendre six
029 spk2>	 tickets plus pour la même journée pour faire un un un trajet
030 spk1>	 ah+ben oui mais ça c' est ça c' est la réglementation
031 spk2>	 de dix minutes dix minutes
032 spk2>	 vous comprenez
033 spk1>	 avec vous
034 spk1>	 vous pouvez euh
035 spk1>	 avec un seul ticket
036 spk2>	 alors+que bon la traverse
037 spk2>	 de Charonne c' est un bus qui fait une boucle en+fait
038 spk1>	 oui madame je je sais mais je
039 spk1>	 vous dis avec un seul ticket vous pouvez
040 spk2>	 donc euh
041 spk1>	 faire plusieurs bus à+condition+que ce soit pas la même ligne de bus
042 spk2>	 ah ouais mais ça revient
043 spk1>	 c' est la réglementation madame
044 spk2>	 ça revient cher euh quand+même euh je veux dire euh voilà c' est c' est pas très logique quoi que il y ait pas une réglementation différente
045 spk2>	 sur euh
046 spk1>	 ben
047 spk1>	 avant madame c' était un ticket par bus
048 spk2>	 pour un trajet comme+ça ben
049 spk1>	 maintenant c' est un ticket pour une heure et demie
050 spk2>	 ouais d'accord mais bon
051 spk1>	 hein
052 spk2>	 un ticket par bus à la limite allez euh quand on quand on sort euh quand on r() quand on redescend du bus et qu' on reprend le même bus euh on doit revalider à chaque fois aussi
053 spk1>	 ben oui
054 spk2>	 hein c' est ça le truc hein
055 spk1>	 hm
056 spk2>	 donc euh c' est vrai que ça
057 spk2>	 ça revient cher quoi pour un trajet qui dure euh même pas dix minutes euh c' est vrai que
058 spk2>	 voilà
059 spk2>	 bon+ben c' était ma question ben tant+pis hein
060 spk2>	 bon+ben je vous remercie
061 spk1>	 ben je vous en prie madame bonne journée
062 spk2>	 bah bonne journée
063 spk1>	 au+revoir
064 spk2>	 au+revoir

