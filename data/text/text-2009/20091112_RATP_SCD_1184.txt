
DIALOG=20091112_RATP_SCD_1184

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui allo bonjour
003 spk2>	 je vous appelle pour un renseignement je voudrais savoir s' il y a eu une perturbation ou un problème sur la ligne treize ou sept
004 spk1>	 je vais regarder un instant
005 spk2>	 merci
006 spk1>	 à quelle heure
007 spk2>	 euh vers cinq heures et demie ben là vers là
008 spk1>	 sept ou treize
009 spk2>	 oui
010 spk1>	 non
011 spk2>	 il y a eu aucune perturbation parce+que j' ai un problème j' ai mon fils qui est qui est sorti du collège à quatre heures et demie et toujours pas rentré chez moi donc
012 spk1>	 ben en+tout+cas moi j' ai rien de signalé hein
013 spk2>	 il y a rien
014 spk1>	 quatre heures et demie dix-neuf heures on aurait quelque+chose
015 spk2>	 d'accord
016 spk1>	 non non a+priori j' ai rien de signalé là
017 spk2>	 d'accord
018 spk2>	 ben je vous remercie
019 spk1>	 je vous en prie madame bonne journée madame
020 spk2>	 beaucoup monsieur au+revoir
021 spk1>	 au+revoir

