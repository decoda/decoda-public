
DIALOG=20091112_RATP_SCD_1158

001 spk2>	 NNAAMMEE bonjour
002 spk3>	 oui bonjour
003 spk3>	 euh en+fait ça serait pour avoir un renseignement mon fils il a perdu sa carte euh m() Imagine+R
004 spk2>	 oui
005 spk3>	 euh en+fait mardi euh dans le bus trois cent soixante-dix huit ou parce+qu' il prend deux bus en+fait
006 spk3>	 et le deux+cent+soixante-cinq
007 spk2>	 alors
008 spk3>	 oui
009 spk2>	 donc là votre fils il a perdu son passe Imagine+R dans un bus mais mardi
010 spk3>	 oui
011 spk2>	 alors sachez que si c' est pas
012 spk2>	 récupéré alors
013 spk3>	 c' est oui pa()
014 spk3>	 oui oui parce+qu' il prend le
015 spk3>	 il prend le trois+cent+dix-huit
016 spk2>	 oui alors
017 spk3>	 trois cent soixante-dix huit et après
018 spk2>	 attendez madame
019 spk3>	 il prend le
020 spk2>	 alors sachez que si c' est pas récupéré les le jour même vous ne pouvez plus récupérer votre carte là il faut faire
021 spk2>	 la demande
022 spk3>	 ah
023 spk2>	 d' une nouvelle carte
024 spk3>	 ah d'accord
025 spk2>	 hm donc là la procédure c' est de passer en agence c' est la première fois qu' il perd son
026 spk2>	 passe
027 spk3>	 euh
028 spk3>	 oui
029 spk3>	 oui
030 spk2>	 d'accord
031 spk2>	 donc là en+fait en agence moyennant euh vingt-trois euros de frais de dossier ils vont lui faire un nouveau passe
032 spk3>	 il faut combien de combien+de semaines
033 spk2>	 non ça sera fait tout+de+suite mais il faut passer en agence commerciale mais vous allez avoir vingt-trois euros de frais de dossier
034 spk3>	 ah d'accord parce+que nous en+fait on habite Villeneuve-la-Garenne donc euh on peut aller où à Saint-Denis ou+bien
035 spk2>	 voilà tout+à+fait
036 spk3>	 à la gare de
037 spk3>	 Gennevilliers
038 spk3>	 non
039 spk2>	 voilà
040 spk2>	 vous avez une agence à Saint-Denis
041 spk3>	 oui
042 spk2>	 donc par+contre je vais vous donner l' horaire
043 spk3>	 oui
044 spk2>	 alors je vais vous donner l' horaire vous patientez merci
045 spk2>	 s'il+vous+plaît madame
046 spk2>	 donc l' agence
047 spk3>	 oui
048 spk2>	 de Saint-Denis est ouverte de douze heures trente à dix-neuf heures trente du lundi au vendredi
049 spk3>	 douze heures trente à di() euh vin() dix-neuf heures trente
050 spk2>	 oui
051 spk3>	 euh
052 spk2>	 hein vous passez
053 spk2>	 avec euh
054 spk2>	 il faudrait passer donc euh avec une pièce d' identité donc votre fils il pe() il sera avec vous
055 spk3>	 euh hein
056 spk2>	 votre fils
057 spk2>	 il sera
058 spk3>	 ben
059 spk2>	 avec vous
060 spk3>	 euh il est étudiant de+toute+façon
061 spk2>	 il a quel âge
062 spk3>	 il a treize ans
063 spk2>	 d'accord donc il faudrait passer donc avec euh donc moyennant vingt-trois euros de frais de dossier et ils vous feront un
064 spk2>	 nouveau passe
065 spk3>	 oui
066 spk2>	 d'accord
067 spk3>	 d'accord
068 spk3>	 donc ils me feront ça sur place
069 spk3>	 alors
070 spk2>	 oui
071 spk2>	 tout+à+fait
072 spk3>	 hum d'accord merci
073 spk2>	 bonne journée au+revoir
074 spk3>	 au+revoir

