
DIALOG=20091112_RATP_SCD_1203

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 allo bonsoir
003 spk1>	 oui bonsoir
004 spk2>	 euh je voudrais m' informer s' il n' y a pas euh par+hasard une euh grève demain sur la ligne B du RER
005 spk1>	 alors donc vous voulez savoir s' il y aura une grève demain sur la ligne B
006 spk1>	 malheureusement nous n' avons aucune aucune indication pour demain
007 spk2>	 aucune indication
008 spk1>	 non non malheureusement là pour l' instant je n' ai aucune en indu() indication pour demain sur le RER+B
009 spk2>	 parce+que je voudrais aller vers euh le aéroport Charles+de+Gaulle
010 spk1>	 oui
011 spk2>	 vers six heures du matin mais je sais pas s' il y a une grève ou il faut
012 spk1>	 là je n' ai pas d' information monsieur je suis désolée
013 spk2>	 et vous l' aurez l' information ou vous savez pas ou
014 spk1>	 bah écoutez si vous avez
015 spk2>	 faut s' il faut aller
016 spk1>	 est-ce+que vous avez
017 spk2>	 oui
018 spk1>	 internet monsieur
019 spk2>	 euh oui
020 spk1>	 donc vous pouvez consulter internet régulièrement si il y a quoi que ce soit comme information ça va être affiché mais là là à l' heure actuelle nous n' avons absolument aucune information
021 spk2>	 d'accord
022 spk1>	 d'accord
023 spk2>	 oui oui
024 spk1>	 désolée au+revoir bonne soirée
025 spk2>	 merci beaucoup au+revoir

