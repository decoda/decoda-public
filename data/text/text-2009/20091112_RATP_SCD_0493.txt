
DIALOG=20091112_RATP_SCD_0493

001 spk1>	 NNAAMMEE bonjour NNAAMMEE bonjour
002 spk2>	 oui bonjour monsieur écoutez moi je vous appelle p() juste pour un petit renseignement
003 spk2>	 je voulais savoir si je pourrais avoir éventuellement le numéro de téléphone de l' accueil du métro huit à l' arrêt euh Roch euh Richelieu Richelieu Richelieu-Drouot
004 spk1>	 il y a pas de téléphone monsieur c' est des numéros internes à cinq chiffres des numéros internes à la RATP
005 spk2>	 ah d'accord parce+qu' en+fait bon quand+même je vous explique mon problème il se trouve que j' ai je suis rentré de Paris en direction de Versailles ce matin et en+fait j' ai eu un petit problème avec le billet de train enfin avec le le b() le billet de métro que j' avais que j' avais déjà acheté donc du+coup euh enfin à cau() à+cause+de ce problème là j' ai dû racheter un billet auprès+de l' hôtesse d' accueil et il se trouve que pour vérifier mes zones elle a pris ma carte Imagine+R
006 spk1>	 oui
007 spk2>	 et
008 spk2>	 c' est la se c' est la dernière fois que j' ai vu ma carte donc je pense qu' elle a oublié de me la rendre
009 spk1>	 ah au guichet alors
010 spk2>	 euh quand
011 spk2>	 euh voilà enfin dans l' espèce de petite borne pour carte donc euh le problème c' est que moi maintenant j' ai plus ma carte donc je ne peux plus circuler au+sein+de Versailles comme les alentours et je voulais savoir comment je pourrais faire pour euh enfin pour reparler à la à l' hôtesse d' accueil qui m' a
012 spk1>	 sur la ligne huit hein c' était vous dites hein
013 spk2>	 euh oui à l' arrêt Richelieu-Drouot
014 spk1>	 alors Richelieu-Drouot donc c' est une carte Imagine+R à quel nom
015 spk2>	 euh NNAAMMEE
016 spk1>	 vous patientez je vais l' appeler hein
017 spk2>	 merci beaucoup
018 spk3>	 oui euh Miró
019 spk1>	 oui bonjour euh Richelieu le Service+Clients
020 spk3>	 oui
021 spk1>	 je t' appelle j' ai une personne qu() en ligne qui est venue chez toi et qui peut-être laissé son passe Ima() euh Navigo
022 spk3>	 ah+ben moi je suis au Pôle donc je suis pas au courant
023 spk1>	 d'accord
024 spk3>	 euh il faudrait voir avec la caisse hein
025 spk1>	 euh p() s()
026 spk1>	 OK p()
027 spk3>	 tu veux tu veux que je te la que je te
028 spk3>	 transfère
029 spk1>	 ouais c' est au au guichet que tu vas me
030 spk1>	 oui
031 spk3>	 transférer
032 spk1>	 ouais s' il te plaît
033 spk3>	 quitte pas alors hein
034 spk1>	 merci
035 spk3>	 et et et ça passe pas euh Miró là j' arrive pas à passer Miró c' est quoi ce bordel
036 spk1>	 c' est Richelieu-Drouot hein qu' il faut que tu me passes
037 spk3>	 pourquoi j' arrive pas à passer Mi()
038 spk3>	 oui attends ça a coupé hein
039 spk1>	 et c' mais c' est Richelieu-Drouot hein que j' ai besoin d' avoir
040 spk3>	 ouais ne quitte pas
041 spk1>	 d'accord
042 spk3>	 mais ça passe pas pourquoi ça marche pas mon double appel tu es là non
043 spk1>	 allo
044 spk3>	 ouais
045 spk1>	 oui
046 spk3>	 non ça marche pas hein
047 spk1>	 ben donne -moi le numéro je vais les
048 spk1>	 les les appeler
049 spk3>	 ouh alors attends
050 spk3>	 parce+que moi je suis pas d' ici trente neuf cent vingt+et+un
051 spk1>	 trente
052 spk3>	 neuf cent vingt+et+un
053 spk1>	 c' est ce+que je fais là
054 spk3>	 mais non ho il a mis le transfert euh le renvoi d' appel alors c' est pour ça
055 spk1>	 ouais je c' est celui c' est le
056 spk1>	 numéro que j' ai fait
057 spk3>	 ouais
058 spk3>	 ben attends quitte pas je vais essayer de les avoir autrement
059 spk3>	 euh c' est un appel du centre de liaison est-ce+que Richelieu-Drouot est à l' écoute ça marche pas euh tu es revenu dans la caisse
060 spk4>	 euh négatif euh là je suis devant euh je viens de passer juste devant la caisse là je suis au+niveau+de
061 spk3>	 juste pour info est-ce+que tu as un passe euh Orange ou Imagine+R je sais plus trop euh dans ta caisse que quelqu'un aurait oublié
062 spk4>	 firmatif()
063 spk3>	 affirmatif ou négatif
064 spk4>	 affirmatif
065 spk3>	 bon apparemment il y a bien quelque+chose à Drouot hein
066 spk1>	 ah d'accord donc je peux envoyer la personne
067 spk1>	 alors d'accord OK ben écoute je te remercie
068 spk3>	 ouais ouais ouais hein parce+que
069 spk3>	 là il est en intervention donc il peut pas me donner le nom
070 spk1>	 OK d'accord
071 spk3>	 hein mais euh bon la personne se rend mais apparemment il m' a dit que oui il avait eu un
072 spk3>	 un
073 spk1>	 passe
074 spk3>	 passe hein
075 spk1>	 OK ben écoute c' est sympa je te remercie hein bon courage hein salut
076 spk3>	 d'accord voilà de rien au+revoir
077 spk1>	 monsieur
078 spk2>	 oui bonjour
079 spk1>	 oui
080 spk1>	 non c' est toujours moi
081 spk1>	 donc apparemment
082 spk2>	 oui
083 spk1>	 ils ont appelé la station il y a effectivement un passe qui a été la oublié là-bas
084 spk1>	 donc il
085 spk2>	 oui
086 spk1>	 faudrait y passer le+plus rapidement possible
087 spk2>	 d'accord et si je viens en d() en+fin+de journée c' est bon
088 spk1>	 euh moi je vous conseillerais d' y aller avant hein
089 spk1>	 parce+que comme il va y avoir un changement de service
090 spk2>	 ben le pro le problème c' est que je
091 spk2>	 je je travaille donc euh je pourrai pas avant malheureusement
092 spk1>	 ben a() allez -y le+plus tôt possible alors avec une pièce d' identité
093 spk2>	 d'accord
094 spk1>	 d'accord
095 spk2>	 d'accord
096 spk2>	 très+bien ça marche ben je vous remercie beaucoup
097 spk2>	 monsieur
098 spk1>	 je vous en prie
099 spk1>	 monsieur bonne journée au+revoir
100 spk2>	 au+revoir vous aussi

