
DIALOG=20091112_RATP_SCD_1237

001 spk1>	 NNAAMMEE bonsoir
002 spk2>	 oui bonsoir
003 spk1>	 bonsoir monsieur
004 spk2>	 je
005 spk2>	 vous appelle ça serait pour avoir un renseignement par+rapport euh à la journée de demain
006 spk1>	 oui
007 spk2>	 au niveau RATP RER+B
008 spk1>	 oui
009 spk2>	 qu'est-ce+qui est annoncé s'il+vous+plaît
010 spk1>	 alors pour l' instant pour concernant le le RER+B euh pour demain nous n' avons aucune information malheureusement
011 spk2>	 d'accord parce+qu' en+fin+de+compte moi je vous explique moi je euh je dois le prendre demain dans les alentours de euh cinq heures et demie six heures du matin
012 spk2>	 donc euh je voulais savoir
013 spk1>	 d'accord oui
014 spk2>	 comme je rentre de vacances
015 spk1>	 bien+sûr
016 spk2>	 je voulais savoir si je devais euh prévoir de partir beaucoup plus tôt au pas
017 spk1>	 bah ce+qui se passe c' est que moi je je je ne peux pas vous donner d' indication parce+qu' on a pas d' c'est-à-dire que là quand vous allez sur le site RATP il n' y a aucune information concernant demain
018 spk2>	 oui je sais bien
019 spk1>	 vous avez pour aujourd'hui
020 spk1>	 vous avez trois trains sur quatre qui fonctionnent euh mais là on je suis comme vous je n' ai pas+du+tout d' information à vous communiquer
021 spk1>	 donc je ne peux
022 spk2>	 hm
023 spk1>	 pas vous dire comment va être demain le trafic sur la ligne B du RER
024 spk2>	 hm
025 spk1>	 malheureusement
026 spk2>	 d'accord
027 spk1>	 malheureusement je suis désolée
028 spk2>	 bah je vous remercie
029 spk1>	 bonne soirée
030 spk2>	 bonne soirée
031 spk1>	 au+revoir
032 spk2>	 au+revoir

