
DIALOG=20091112_RATP_SCD_0545

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour je vous appelle s'il+vous+plaît parce+que je lundi j' ai je suis rentré d' Orly et j' ai j' ai pris un vol je suis euh je suis atterri à Orly et j' ai pris le bus entre cent quatre-vingt-trois et deux cent quatre-vingt-cinq le problème j' ai oublié une valise dans un des bus j' ai appelé plusieurs fois juste pour savoir si vous avez pas trouvé quelque+chose
003 spk1>	 donc c' est sur l' Orlybus
004 spk2>	 euh Orly ouais j' étais à Orly comme je connaissais pas très bien les bus j' en ai pris euh deux
005 spk1>	 alors vous avez pris quoi
006 spk2>	 et euh
007 spk2>	 ma valise bon elle est restée euh dans un des bus je j' ai appelé ils m' ont donné des numéros ben j' appelle mais c' était des
008 spk1>	 non mais moi je
009 spk1>	 voudrais savoir si c' est l' Orlybus si c' est le bus deux cent quatre-vingt-cinq ou des choses comme+ça
010 spk2>	 ah là j' ai pris le premier c' était cent quatre-vingt-trois et après j' ai pris le deux+cent+quatre-vingt-cinq
011 spk1>	 d'accord c' était quand
012 spk2>	 c' était lundi
013 spk2>	 lundi soir
014 spk1>	 lundi
015 spk1>	 donc une valise de quelle couleur
016 spk2>	 noire
017 spk2>	 avec roulettes
018 spk1>	 noire
019 spk2>	 on va dire euh pas les nouvelles bon avec roulettes les les anciennes les grandes valises là
020 spk1>	 hum d'accord ben je vais appeler hein euh
021 spk1>	 je vais regarder
022 spk2>	 pas comme un caddie pas
023 spk2>	 comme un caddie hein
024 spk1>	 non non non j' ai j' ai bien compris
025 spk1>	 vous
026 spk2>	 d'accord
027 spk1>	 patientez un instant
028 spk2>	 d'accord merci
029 spk1>	 monsieur
030 spk2>	 oui
031 spk1>	 non hein il y a rien
032 spk2>	 il y a rien
033 spk1>	 non
034 spk2>	 il y a
035 spk2>	 pas de valise
036 spk2>	 lundi euh
037 spk1>	 non
038 spk1>	 non non non non
039 spk2>	 ah OK ben euh est-ce+que bon ça sert à rien que je vous rappelle maintenant alors
040 spk1>	 ah là oui maintenant ça sert plus à rien hein
041 spk2>	 ça sert plus à rien parce+que j' ai j' ai appelé hier ils m' ont dit qu' il y a plusieurs dépôts il y a plusieurs
042 spk2>	 valises euh
043 spk1>	 non non non non le le
044 spk1>	 cent+quatre-vingt-trois et le deux+cent+quatre-vingt-cinq c' est au même dépôt
045 spk2>	 ouais
046 spk1>	 donc euh on peut regarder
047 spk1>	 pour ces deux bus
048 spk2>	 ouais de de de d' Orly euh
049 spk2>	 parce+que je sais que je suis descendu vers Ivry
050 spk1>	 vers
051 spk2>	 Ivry
052 spk1>	 Ivry-sur-Seine
053 spk2>	 ouais
054 spk1>	 moui mais c' est le cent+quatre-vingt-trois hein monsieur qui va sur la Porte+de+Choisy
055 spk2>	 euh le non parce+que le premier c' était le cent+quatre-vingt-trois j' ai toujours le ticket mais après j' ai pris normalement parce+que je suis pas sûr à cent pourcent le deux+cent+quatre-vingt-cinq mais je me suis atterri à Ivry après
056 spk1>	 mais Évry ou Ivry
057 spk2>	 Ivry à+côté+d' Orly on va dire là Ivry
058 spk1>	 parce+que là Ivry euh le cent+quatre-vingt-trois
059 spk2>	 non je suis descendu je me rappelle très bien je suis descendu du cent+quatre-vingt-trois le chauffeur du cent+quatre-vingt-trois il m' a dit de prendre un autre bus je pense c' était le deux+cent+quatre-vingt-cinq mais je sais qu' après quand j' ai oublié la valise j' étais à Ivry I V E R Y
060 spk1>	 sur Ivry-sur-Seine
061 spk2>	 voilà
062 spk1>	 hm un instant
063 spk1>	 non monsieur
064 spk2>	 oui
065 spk1>	 non j' ai beau regarder il y a rien hein il y a rien mais même vers Ivry et
066 spk2>	 il y a rien
067 spk1>	 non
068 spk2>	 ben merci+bien madame
069 spk1>	 je vous en prie bonne au+revoir
070 spk2>	 bonne journée au+revoir

