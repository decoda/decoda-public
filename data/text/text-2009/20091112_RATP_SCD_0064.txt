
DIALOG=20091112_RATP_SCD_0064

001 spk3>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk1>	 allo
004 spk2>	 euh bonjour
005 spk2>	 oui bonjour monsieur
006 spk1>	 oui
007 spk1>	 bonjour monsieur euh voilà ce serait pour un renseignement je prends le bus quatre cent quatre-vingt-douze
008 spk2>	 oui
009 spk1>	 et là depuis ce matin il y en a aucun
010 spk2>	 quatre+cent+quatre-vingt-douze
011 spk1>	 oui
012 spk2>	 alors faut savoir que même+si il y a notre numéro sur cette euh sur ces bus euh c' est des bus qui sont gérés par la compagnie Athis+Cars
013 spk1>	 oui
014 spk2>	 euh donc moi
015 spk1>	 ça je le savais
016 spk2>	 ce+que je peux faire on n' a pas beaucoup+d' informations sur ce réseau je vais vous donner le numéro de téléphone de la société Athis+Cars
017 spk1>	 oui
018 spk2>	 qui gère donc euh ces transports alors vous pourrez les joindre au zéro+un+soixante-neuf
019 spk1>	 oui
020 spk2>	 zéro+cinq
021 spk1>	 oui
022 spk2>	 treize
023 spk1>	 oui
024 spk2>	 cinquante-six
025 spk1>	 oui très+bien zéro+un+soixante-neuf oui
026 spk2>	 zéro+un+soixante-neuf
027 spk2>	 zéro+cinq+treize cinquante-six
028 spk1>	 hm
029 spk2>	 oui
030 spk1>	 très+bien
031 spk2>	 je vous en prie
032 spk1>	 merci au+revoir
033 spk2>	 bonne journée monsieur
034 spk2>	 au+revoir

