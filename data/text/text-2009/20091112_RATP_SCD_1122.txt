
DIALOG=20091112_RATP_SCD_1122

001 spk1>	 NNAAMMEE bonsoir
002 spk2>	 oui bonsoir monsieur
003 spk2>	 voilà j' aurais voulu avoir un des renseignements pour les prévisions de trafic pour demain pour le RER+B enfin
004 spk1>	 alors euh pour demain on a il y a rien qui est prévu encore au+niveau+du trafic on sait que ça va être reconduit normalement
005 spk2>	 hm
006 spk1>	 mais après on peut pas vous
007 spk1>	 dire euh
008 spk2>	 oui si c' est les mêmes
009 spk2>	 prévisions de trafic qu' aujourd'hui c' est pas la peine de les donner honnêtement ça sert à rien quoi
010 spk2>	 alors là
011 spk1>	 ah
012 spk2>	 je vais
013 spk2>	 me plaindre par+rapport+à ça aussi parce+que je trouve ça inadmissible qu' on ait dit trois trains sur quatre alors+qu' en+fait sur le terrain il y en a pas trois sur quatre mais il y en a un sur quatre quoi hein euh
014 spk1>	 ben c' est euh trois sur quatre c' est euh c' est au+niveau+des heures de pointe et après c' est sur l' ensemble de la ligne
015 spk1>	 donc comme la ligne est longue
016 spk2>	 ouais ouais
017 spk1>	 et euh donc euh c' est sûr que quand on attend un train c' est euh
018 spk1>	 on on l' attend
019 spk2>	 oui mais quand vous l' attendez
020 spk2>	 quand vous l' attendez trois quart d' heure le
021 spk2>	 train
022 spk1>	 hm
023 spk2>	 voire une heure parfois je peux vous dire que c' est pas trois sur quatre hein pour moi c' est un sur quatre hein
024 spk2>	 euh
025 spk1>	 non
026 spk1>	 c' est pas un sur quatre monsieur parce+que ça ça veut dire un sur quatre ça veut dire qu' il serait tout seul à tourner
027 spk1>	 et comme je vous disais la ligne est longue et il y a des fourches
028 spk1>	 donc il fait il y a il y a des trains qui sont sur partis
029 spk2>	 ouais mais là le
030 spk2>	 là je trouve
031 spk2>	 que c' est un+peu vasouillard hein la communication sur ce truc je sais pas ce+qu' il y a mais je trouve que c' est un+peu euh moi limite quoi l' information bon elle a été donnée pour une fois elle a été donnée mais elle est quand+même euh moyenne moyenne voilà donc je tenais quand+même à vous le signaler
032 spk2>	 quoi
033 spk1>	 très
034 spk1>	 bien ben je l' ai pris en compte monsieur
035 spk2>	 enfin je sais pas si ça sera pris en compte mais bon je le dis quand+même et
036 spk2>	 puis voilà quoi
037 spk1>	 si si si
038 spk1>	 si si si c' est pris en compte euh votre appel est enregistré monsieur donc c' est pris en compte hein vous inquiétez pas
039 spk1>	 et si vous voulez
040 spk2>	 voilà
041 spk1>	 vous faire votre réclamation je peux même vous donner l' adresse
042 spk2>	 ah+ben écoutez euh je vais la prendre
043 spk2>	 hein euh
044 spk1>	 vous la frais
045 spk1>	 oui comme+ça ça sera fait alors euh faudra marquer RATP
046 spk2>	 oui
047 spk1>	 alors RER+B alors RATP
048 spk1>	 service relation clientèle
049 spk1>	 ligne B
050 spk1>	 RER+B
051 spk1>	 et c' est soixante-treize boulevard Saint-Jacques
052 spk2>	 oui
053 spk1>	 soixante-quinze+mille+quatorze
054 spk1>	 Paris monsieur
055 spk2>	 hum d'accord OK
056 spk1>	 je vous en prie
057 spk2>	 ben écoutez
058 spk2>	 vous remercie merci bonne journée
059 spk2>	 au+revoir et bon courage au+revoir
060 spk1>	 bonne soirée monsieur merci
061 spk1>	 au+revoir

