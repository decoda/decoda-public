
DIALOG=20091112_RATP_SCD_0074

001 spk1>	 un instant
002 spk1>	 NNAAMMEE va vous répondre
003 spk2>	 NNAAMMEE bonjour
004 spk3>	 oui bonjour monsieur
005 spk2>	 bonjour madame
006 spk3>	 voilà je vous appelle parce+que donc euh euh j' étais prise dans les perturbation lundi et mardi euh aujourd'hui je de pour le hum RER+B je veux dire hein
007 spk3>	 c' est moi j' ai pas précisé
008 spk2>	 oui
009 spk3>	 euh là je viens d' entendre qu' il y avait encore des problèmes à+raison+de trois trains sur quatre c' est ça
010 spk2>	 oui nan mais quasi normal hein le trafic
011 spk3>	 d'accord
012 spk3>	 alors je vous a() je vous appelais également
013 spk2>	 nan pas de problème
014 spk3>	 pour demain euh est-ce+que demain ce sera encore
015 spk3>	 perturbé demain ce sera complètement rétabli
016 spk2>	 nan nan normalement ça rentre euh
017 spk2>	 dans l' ordre là ce matin déjà
018 spk3>	 d'accord parce+que là aux informations ils ont dit trois trains sur quatre sur le B mais c' est pratiquement rétabli
019 spk2>	 oui oui oui
020 spk3>	 et demain ce sera
021 spk3>	 complètement rétabli
022 spk2>	 oui tout+à+fait
023 spk3>	 d'accord bon+ben c' est ce+que je voulais savoir
024 spk3>	 merci beaucoup monsieur au+revoir vous aussi au+revoir
025 spk2>	 de rien bonne journée au+revoir

