
DIALOG=20091112_RATP_SCD_0577

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui allo bonjour
003 spk2>	 oui en+fait c' est par+rapport+à parce+que j' ai j' ai une mission d' intérim de deux mois et on m' a demandé pour avoir euh le prix d' un passe pour une deux zones jusqu'à Saint-Georges quoi à Paris dans le neuvième il me semble le tarif en+fait
004 spk1>	 zones un et deux
005 spk2>	 oui zone un ben en+fait c' est la station Saint-Georges quoi c' est zone un et deux
006 spk1>	 ben c() c' est Paris ouais mais vous vous habitez où
007 spk2>	 moi j' habite à Boulogne-Billancourt en+fait mais comptez pour deux simple comptez pas moi je mont
008 spk1>	 ouais non mais même Boulogne c' est bon c' est en zone deux euh donc c' est cinquante-six soixante
009 spk2>	 pour combien+de temps
010 spk1>	 pour un mois
011 spk2>	 pour un mois
012 spk1>	 hm
013 spk2>	 bon mais il y a pas un truc euh moins cher que ça là
014 spk1>	 ah non
015 spk1>	 non non après c' est le Navigo Intégrale mais c' est un a() c' est un abonnement à l' année
016 spk1>	 donc si vous avez une mission pour deux mois
017 spk2>	 c' est combien c() c' est combien
018 spk1>	 cinquante-deux vingt-trois
019 spk1>	 par mois
020 spk2>	 cinquante-deux vingt-trois
021 spk1>	 ouais
022 spk1>	 mais il faut vous a() il faut vous a()
023 spk1>	 ben il faut le prendre à l' année donc ça a pas d' intérêt
024 spk2>	 et pour la semaine ça fait combien ça
025 spk1>	 dix-sept vingt
026 spk2>	 dix-sept vingt
027 spk1>	 mais si vous le multipliez par quatre semaines dans le mois ça vous revient plus cher que de prendre le le mois hein
028 spk2>	 mais et l() p() et les tickets ça ça revient moins cher non les tickets
029 spk1>	 ben le tickets c' est onze euros soixante le carnet de dix
030 spk1>	 donc ça fait onze soixante euh si p() vous prenez un aller un retour par jour
031 spk1>	 vous travaillez cinq jours ça fait un carnet par semaine
032 spk1>	 ça fait onze euros soixante euh multiplié par quatre
033 spk2>	 et là je pourrai avoir un reçu enfin le truc pour donner
034 spk2>	 quand j' irai au guichet
035 spk1>	 alors quand pour acheter quoi
036 spk2>	 ben pour acheter le passe pour le mois
037 spk1>	 ah oui d() euh ben vous demandez le reçu de chargement vous donnez le reçu de chargement à votre patron
038 spk2>	 d'accord euh je vais voir ça
039 spk2>	 je vous remercie
040 spk1>	 je vous en prie au+revoir
041 spk2>	 au+revoir

