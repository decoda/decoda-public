
DIALOG=20091112_RATP_SCD_0787

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui
003 spk2>	 bonjour monsieur s'il+vous+plaît
004 spk1>	 bonjour monsieur
005 spk2>	 j' ai j' aimerais avoir l' adresse s'il+vous+plaît pour les artistes qui qui souhaitent jouer à la RATP avec une autorisation
006 spk1>	 d'accord
007 spk2>	 jouer de la musique une guitare sèche
008 spk1>	 d'accord
009 spk2>	 voilà
010 spk1>	 très+bien bah je vais vous donner le
011 spk2>	 merci beaucoup monsieur
012 spk1>	 les références vous patientez un instant s'il+vous+plaît
013 spk1>	 merci
014 spk2>	 je vous en prie
015 spk1>	 monsieur s'il+vous+plaît
016 spk2>	 oui
017 spk1>	 merci d' avoir patienté
018 spk2>	 s'il+vous+plaît
019 spk1>	 je vais vous donner le numéro de téléphone du service qui s' occupe de des autorisations de euh
020 spk2>	 s'il+vous+plaît
021 spk1>	 de jouer de la musique
022 spk1>	 dans le métro
023 spk2>	 merci
024 spk1>	 alors c' est zéro+un
025 spk1>	 cinquante-huit
026 spk2>	 zéro+un+cinquante-huit
027 spk1>	 soixante-dix-sept
028 spk2>	 soixante-dix-sept
029 spk1>	 quarante
030 spk2>	 quarante
031 spk1>	 soixante-quatorze
032 spk2>	 soixante-quatorze
033 spk1>	 zéro+un+cinquante-huit
034 spk2>	 d'accord et il y a
035 spk1>	 soixante-dix-sept+quarante+soixante-quatorze
036 spk2>	 merci et euh le euh il y a pas d'
037 spk1>	 non non
038 spk2>	 pour les
039 spk2>	 d'accord
040 spk1>	 ce+qu' il y a c' est que euh il faut il faut laisser votre message si vous n' avez personne ils vont vous recontacter
041 spk1>	 hein mais c' est le numéro
042 spk2>	 d'accord
043 spk1>	 du service hein d'accord
044 spk2>	 d'accord
045 spk2>	 merci beaucoup monsieur
046 spk1>	 je vous en prie monsieur merci vous de+même
047 spk2>	 je vous souhaite bonne journée au+revoir
048 spk1>	 au+revoir

