
DIALOG=20091112_RATP_SCD_1225

001 spk1>	 NNAAMMEE
002 spk2>	 NNAAMMEE bonsoir
003 spk2>	 NNAAMMEE bonsoir
004 spk3>	 bonsoir monsieur
005 spk3>	 c' est pour euh savoir pour aller à Porte+Maillot s'il+vous+plaît je suis à Chevilly euh Villejuif
006 spk2>	 Villejuif-Louis+Aragon
007 spk3>	 oui
008 spk2>	 alors Villejuif
009 spk2>	 pour aller vous m' avez dit
010 spk2>	 à
011 spk3>	 Porte+Maillot
012 spk2>	 Porte+Maillot hein
013 spk2>	 alors veuillez patienter je regarde hein
014 spk3>	 oui
015 spk2>	 s'il+vous+plaît
016 spk3>	 oui
017 spk2>	 vous allez prendre le métro de la ligne sept
018 spk3>	 oui
019 spk2>	 en direction de La+Courneuve jusqu'à Palais+Royal
020 spk3>	 à La+Courneuve
021 spk2>	 et descendre à Palais+Royal
022 spk3>	 Palais+Royal
023 spk3>	 je croyais que c' était euh
024 spk3>	 euh oh comment ça s' appelle
025 spk3>	 un+peu plus loin le Opéra
026 spk2>	 non vous pouvez à Palais+Royal parce+que après vous avez le la ligne une qui vous amène directement à la à La+Défense à Porte de Porte+Maillot
027 spk3>	 ah+bon alors là euh la direction de La+Courneuve Palais et
028 spk3>	 je
029 spk2>	 puis vous
030 spk2>	 descendez à Palais+Royal
031 spk3>	 euh changement Palais
032 spk3>	 Royal
033 spk2>	 exactement
034 spk2>	 là vous prenez la ligne une en direction de La+Défense
035 spk2>	 et vous descendez à
036 spk3>	 La+Défense
037 spk2>	 Porte+Maillot
038 spk2>	 c' est bien Porte+Maillot que vous souhaitez hein
039 spk3>	 oui oui
040 spk3>	 Porte+Maillot
041 spk2>	 hein c' est ça
042 spk2>	 hm d'accord il y a aucun problème hein
043 spk3>	 d'accord
044 spk3>	 je vous remercie
045 spk3>	 beaucoup
046 spk2>	 mais je vous
047 spk2>	 en prie bonne fin de soirée
048 spk3>	 merci
049 spk3>	 vous aussi
050 spk2>	 au+revoir
051 spk3>	 au+revoir

