
DIALOG=20091112_RATP_SCD_0414

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour euh je vous appelle parce+que j' ai très certainement perdu un passe+Navigo à une à la station La+Chapelle sur la ligne deux
003 spk1>	 oui quand
004 spk2>	 et euh avant-hier
005 spk1>	 avant hier d'accord
006 spk2>	 et je voulais savoir
007 spk2>	 si euh oui s' il y aurait un numéro auquel je pourrais appeler pour avoir le
008 spk1>	 ben je vais regarder s' il a été retrouvé à quel nom
009 spk2>	 NNAAMMEE
010 spk1>	 vous patientez je regarde
011 spk1>	 hein
012 spk2>	 merci
013 spk1>	 c' est un Navigo Orange Imagine+R Intégrale
014 spk2>	 Imagine+R
015 spk1>	 d'accord vous patientez un
016 spk1>	 instant
017 spk2>	 merci
018 spk1>	 s'il+vous+plaît
019 spk2>	 oui
020 spk1>	 malheureusement non ça n' a pas été retrouvé
021 spk2>	 bon très+bien ben
022 spk1>	 faut aller en agence avec euh une pièce d' identité on va vous refaire un nouveau passe
023 spk2>	 euh je vais où
024 spk1>	 en agence commerciale vous êtes où
025 spk2>	 euh dans le dix-neuvième
026 spk1>	 c' est à dire quel métro qui est proche+de vous
027 spk2>	 euh Jaurès
028 spk1>	 Jaurès donc vous allez avoir Gare+du+Nord ou République
029 spk2>	 Gare+du+Nord ou République d'accord merci beaucoup
030 spk1>	 bonne journée
031 spk1>	 au+revoir
032 spk2>	 au+revoir

