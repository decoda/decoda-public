
DIALOG=20091112_RATP_SCD_1058

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour
003 spk2>	 voilà euh je viens de descendre du bus et j' ai oublié en+fait euh un une petite boîte euh dans le bus euh où il y a mes affaires euh c' est le c' est le deux+cent+quatre-vingt-un qui je pense qu' il arrive à Eurostar là à Europarc pardon
004 spk1>	 alors vous avez pris le deux+cent+quatre-vingt-un
005 spk2>	 oui qui arrive
006 spk1>	 et vous avez
007 spk2>	 à
008 spk2>	 qui arrive là à Europarc je pense
009 spk1>	 d'accord et vous me dites que vous avez laissé une boîte
010 spk1>	 à l' intérieur
011 spk2>	 alors
012 spk2>	 c' est oui voilà c' est une boîte en métal euh carrée avec euh il y a un drapeau de l' Angleterre dessus
013 spk1>	 il y a un drapeau dessus hum d'accord je vais voir si ça a été signalé auprès+de
014 spk1>	 de la ligne
015 spk2>	 très+bien de
016 spk1>	 de bus mais ça vient de se faire là
017 spk2>	 oui oui à l' instant je viens de descendre du bus et de m' en rendre compte
018 spk1>	 d'accord alors je me renseigne un instant
019 spk1>	 s'il+vous+plaît
020 spk2>	 merci
021 spk3>	 je vais lui demander
022 spk3>	 oui le terminus de Joinville bonjour
023 spk1>	 oui bonjour excuse -moi de te déranger c' est le Service+Clients+à+Distance
024 spk1>	 je
025 spk3>	 oui
026 spk1>	 t' appelle pour un OT qu' une personne a laissé dans le bus deux cent quatre-vingt-un là qui allait en direction d' Europarc
027 spk1>	 euh
028 spk3>	 alors
029 spk3>	 je vais elle l' a elle l' a perdu maintenant là
030 spk1>	 oui oui oui c' est une euh boîte en métal
031 spk3>	 bon attends ne ne quitte pas je vais ne quittez pas je vais demander
032 spk1>	 d'accord merci
033 spk3>	 oui sur l' ensemble de la ligne euh deux quatre-vingt-un sur l' ensemble de la ligne deux+quatre-vingt-un un machiniste aurait -il un OT en sa possession
034 spk3>	 le quarante-et-unième
035 spk3>	 qu'est-ce+que c' est qu'est-ce+que c' est s' il te plaît
036 spk3>	 OK d'accord je te remercie non ne quitte pas
037 spk3>	 oui allo
038 spk1>	 oui
039 spk3>	 oui ben la la boîte elle est bien au niveau du du terminus d' Europarc la personne elle est à quel endroit
040 spk1>	 ah alors attends ben je vais lui demander
041 spk1>	 s'il+vous+plaît
042 spk2>	 oui
043 spk1>	 oui donc euh vous êtes à quel endroit
044 spk2>	 euh ben là je veux je vais prendre le deux+cent+quatre-vingt-un qui va arriver à Europarc euh apparemment j' ai demandé au chauffeur il a été retrouvé mon
045 spk1>	 ah donc oui
046 spk2>	 paquet
047 spk1>	 tout+à+fait donc c' est pour ça donc euh vous allez retourner à Europarc enfin vous allez
048 spk1>	 aller à Europarc
049 spk2>	 oui j' arrive
050 spk2>	 tout+de+suite là
051 spk2>	 dans cinq minutes
052 spk1>	 ah
053 spk1>	 d'accord alors attendez
054 spk2>	 voilà
055 spk1>	 s' il te plaît
056 spk1>	 s' il
057 spk3>	 oui
058 spk3>	 oui allo
059 spk1>	 oui euh écoute ben la jeune fille elle est dans le bus deux+cent+quatre-vingt-un elle a pris l'autre pour aller
060 spk1>	 à Europarc
061 spk3>	 bon
062 spk3>	 OK ben je viens d' avoir l' autre personne
063 spk1>	 oui
064 spk3>	 je lui ai demandé
065 spk3>	 qu' il qu' il aille jusqu'à Europarc et que le transbordement va se ben du+moins euh
066 spk3>	 l' échange
067 spk1>	 se
068 spk3>	 va se faire au+niveau+du terminus
069 spk1>	 d'accord très+bien ben écoute je te remercie pour elle
070 spk1>	 au+revoir
071 spk3>	 de rien
072 spk3>	 au+revoir
073 spk1>	 s'il+vous+plaît
074 spk2>	 oui
075 spk1>	 oui donc ben là vous pourriez voir vous pourrez voir avec le machiniste hein parce+qu' ils ont fait appel et ils sont au courant
076 spk1>	 donc vous pourrez récupérer votre boîte à Europarc tout+à+fait
077 spk2>	 d'accord très+bien donc je vais aller directement avec euh très+bien je vous remercie
078 spk1>	 mais je vous en prie
079 spk2>	 merci au+revoir
080 spk1>	 bonne soirée
081 spk1>	 au+revoir

