
DIALOG=20091112_RATP_SCD_0705

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 bonjour monsieur
004 spk3>	 je ne sais pas si je je suis au bon numéro bon j' ai fait le cent+dix-huit+deux+cent+dix-huit
005 spk2>	 oui
006 spk3>	 mais euh je cherche à être renseignée sur des transports de Nice
007 spk2>	 oh+là
008 spk3>	 alors alors bon+bah c' est eux qui m' ont mal renseigné
009 spk2>	 non vous n' avez pas fait le bon numéro c' est sûr bah oui
010 spk2>	 parce+que euh ils vous ont donné le numéro de téléphone des des renseignements Paris Île-de-France
011 spk3>	 oh+bah alors moi je leur dis bien Nice
012 spk2>	 bah ouais ouais ouais hey vous avez pas internet
013 spk3>	 non monsieur
014 spk2>	 non
015 spk2>	 bon+bah parce+que sinon sur le site+internet vous auriez pu en euh
016 spk3>	 non non non bon
017 spk2>	 voilà
018 spk2>	 donc euh là vous êtes à Paris Île-de-France
019 spk3>	 bon alors
020 spk2>	 et je peux pas vous
021 spk3>	 qu'est-ce+qu' il faut que je fasse
022 spk2>	 bah et il faut à la limite bah il faudrait les rappeler il faut rappeler euh les renseignements et leur demander qu' ils vous donnent bien les euh numéros de téléphone des transports à Nice
023 spk2>	 à la limite vous changez hein changez de euh de renseignements là
024 spk3>	 qu'est-ce+qu' il y a d' autre que cent+dix-huit+deux+cent+dix-huit
025 spk2>	 ah+bah bah vous avez euh cent+dix-huit euh vous patientez je vais regarder
026 spk3>	 oui merci monsieur merci
027 spk2>	 patientez merci
028 spk2>	 s'il+vous+plaît
029 spk3>	 oui monsieur
030 spk2>	 vous pouvez appeler le euh cent+dix-huit huit
031 spk3>	 zéro+zéro+huit d'accord
032 spk2>	 et après euh voilà
033 spk3>	 OK
034 spk2>	 essayez celui-ci hein
035 spk3>	 voilà merci beaucoup
036 spk2>	 je vous en prie bonne journée madame
037 spk3>	 au+revoir
038 spk2>	 au+revoir

