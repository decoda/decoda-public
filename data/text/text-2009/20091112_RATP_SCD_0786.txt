
DIALOG=20091112_RATP_SCD_0786

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 oui bonjour
004 spk3>	 euh je j' ai un déplacement là à faire entre Paris et la banlieue
005 spk2>	 oui
006 spk3>	 et j' aurais voulu connaître euh
007 spk3>	 le moyen
008 spk3>	 alors je je partirai
009 spk2>	 oui
010 spk3>	 euh de la station a+priori ça serait le+plus simple euh du RER+C
011 spk2>	 oui
012 spk3>	 hein qui s' appelle euh Pont+du+Garigliano
013 spk2>	 d'accord oui
014 spk3>	 boulevard Victor d'accord
015 spk2>	 oui
016 spk3>	 et il faut que j' aille à Évry-Bras+de+Fer
017 spk2>	 et vous souhaitez aller à Évry-Bras+de+Fer donc en partant de Boulevard+Victor sur le RER+C hein
018 spk3>	 voilà c' est ça
019 spk2>	 c' est ça
020 spk2>	 d'accord
021 spk2>	 et c' est pour euh quand
022 spk3>	 et
023 spk2>	 ce trajet monsieur
024 spk3>	 comment
025 spk2>	 c' est pour quand ce trajet
026 spk3>	 alors c' est pour euh mercredi euh de la semaine prochaine
027 spk3>	 attendez mercredi
028 spk2>	 oui
029 spk3>	 oui le dix-huit c' est ça
030 spk2>	 d'accord
031 spk3>	 hein et euh il faudrait que je sois là-bas vers huit heures trente
032 spk2>	 d'accord
033 spk2>	 alors ne quittez pas je vais rechercher
034 spk3>	 c' est gentil merci
035 spk3>	 allo
036 spk2>	 alors vous allez récupérer le RER+C donc à Boulevard+Victor direction Dourdan-La+Forêt
037 spk3>	 attendez excusez -moi je prends note
038 spk2>	 allez -y
039 spk2>	 monsieur
040 spk3>	 donc oui
041 spk3>	 d'accord Dourdan
042 spk2>	 Dourdan vous descendez à Juvisy-sur-Orge
043 spk3>	 oui
044 spk2>	 de là vous récupérez le RER+D comme Daniel
045 spk3>	 d'accord
046 spk2>	 direction Corbeil-Essonnes
047 spk3>	 Corbeil oui
048 spk2>	 et vous descendez à l' arrêt le Bras+de+Fer
049 spk3>	 Bras+de+Fer
050 spk2>	 hm
051 spk3>	 oui
052 spk2>	 alors moi j' ai un un départ à Boulevard+Victor à sept heures vingt-et-un
053 spk3>	 attendez sept heures
054 spk3>	 vingt-et-un oui
055 spk2>	 vingt-et-un
056 spk2>	 vous arrivez à Juvisy-sur-Orge à huit heures
057 spk3>	 oui
058 spk2>	 hein vous avez un départ avec le RER+D à huit heures six
059 spk3>	 ouais d'accord
060 spk2>	 et vous arrivez à huit heures vingt-trois au Bras+de+Fer
061 spk3>	 oh+bah ça ça passe hein
062 spk2>	 oui
063 spk3>	 euh s() s() c' est juste mais c' est c' est bon
064 spk2>	 hm
065 spk3>	 d'accord
066 spk2>	 parce+que
067 spk2>	 l' arrivée d'avant j' ai sept heures cinquante-trois
068 spk3>	 oui non non non là huit heures vingt-trois non non c' est très bon hein
069 spk2>	 d'accord
070 spk3>	 d'accord je peux vous poser
071 spk2>	 bien+sûr
072 spk3>	 euh
073 spk3>	 euh la même question mais au retour
074 spk2>	 bien+sûr alors
075 spk2>	 le retour
076 spk3>	 alors attendez
077 spk3>	 le retour c' est vers vingt-trois heures
078 spk2>	 ah
079 spk2>	 alors
080 spk2>	 le retour
081 spk3>	 même jour
082 spk2>	 alors
083 spk2>	 pour le retour qu'est-ce+que ça va me faire
084 spk2>	 ah+ben dis donc retour euh c' est pas euh
085 spk2>	 euh le retour
086 spk2>	 alors le retour alors vous récupérez le RER+D
087 spk3>	 oui
088 spk2>	 hein direction Gare+de+Lyon jusqu'à la Gare+de+Lyon
089 spk3>	 alors attendez Gare+de+Lyon d'accord c' est pas la même d'accord
090 spk2>	 non
091 spk2>	 non parce+que ça dépend des horaires
092 spk3>	 oui oui oui
093 spk2>	 hein
094 spk3>	 non mais c' est pas
095 spk2>	 donc vous descendez à la Gare+de+Lyon
096 spk3>	 oui
097 spk2>	 et quand vous êtes à la Gare+de+Lyon vous récupérez le la ligne six
098 spk3>	 d'accord
099 spk2>	 Bercy direction Charles+de+Gaulle-Étoile
100 spk3>	 oui là en métro ça va euh
101 spk2>	 d'accord
102 spk3>	 je m' y trouve
103 spk2>	 hm
104 spk3>	 euh
105 spk3>	 mais euh
106 spk2>	 d'accord
107 spk3>	 en RER
108 spk2>	 mais vous avez
109 spk2>	 le le RER alors euh c' est ah+bon pourquoi vingt-trois heures cinquante-deux c' est pas avant
110 spk2>	 aïe
111 spk2>	 alors pourquoi il me le donne pas avant
112 spk3>	 même+si c' est vingt-deux heures trente hein euh quand
113 spk2>	 je vais je vais voir oui
114 spk3>	 oui
115 spk2>	 vingt-deux heures trente
116 spk2>	 si ah+bah voilà c' est impeccable ça
117 spk2>	 non j' en ai un mais euh vingt-deux heures cinquante-deux
118 spk3>	 oui bah c' est c' est
119 spk2>	 alors si vous le prenez à vingt-deux heures cinquante-deux
120 spk3>	 oui
121 spk2>	 celui-là vous allez euh vous faites comme le retour c'est-à-dire direction Gare+de+Lyon jusqu'à Juvisy-sur-Orge
122 spk3>	 d'accord
123 spk2>	 et là vous avez la possibilité de récupérer le RER+C
124 spk3>	 d'accord et il y en aura il y en aura encore à cette
125 spk2>	 voilà
126 spk3>	 heure là
127 spk2>	 mais ça sera le dernier hein
128 spk2>	 direction Saint-Quentin et vous descendez boulevard Victor
129 spk3>	 d'accord et là je le récupère hein sûr
130 spk2>	 tout+à+fait oui mais euh voilà faut pas faut pas louper celui de vingt-deux heures cinquante-deux
131 spk3>	 non non d'accord
132 spk2>	 hein
133 spk3>	 OK
134 spk3>	 bah je vous remercie
135 spk2>	 je vous en prie monsieur
136 spk2>	 bonne journée au+revoir
137 spk3>	 au+revoir madame

