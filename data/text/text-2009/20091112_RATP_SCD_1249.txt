
DIALOG=20091112_RATP_SCD_1249

001 spk1>	 NNAAMMEE bonsoir
002 spk2>	 oui
003 spk1>	 oui bonsoir
004 spk2>	 bonsoir monsieur excusez -moi de vous déranger
005 spk1>	 euh je vous en prie
006 spk2>	 euh
007 spk2>	 j' habite Savigny et le quatre+cent+quatre-vingt-douze est en grève apparemment vous savez jusqu'à quand
008 spk1>	 non malheureusement
009 spk1>	 non
010 spk2>	 non
011 spk2>	 vous ne savez
012 spk1>	 non
013 spk2>	 pas
014 spk1>	 non
015 spk2>	 d'accord
016 spk1>	 je vais peut-être essayer de vous
017 spk1>	 donner leur numéro de téléphone si vous souhaitez
018 spk2>	 oui je
019 spk2>	 veux bien
020 spk1>	 alors je alors
021 spk1>	 je vais essayer de le trouver alors veuillez patienter je regarde hein
022 spk2>	 merci beaucoup
023 spk1>	 oh je vous en prie
024 spk1>	 s'il+vous+plaît
025 spk2>	 oui
026 spk1>	 alors le numéro de téléphone
027 spk2>	 oui
028 spk1>	 c' est le zéro
029 spk1>	 un
030 spk2>	 oui
031 spk1>	 soixante-neuf
032 spk2>	 oui
033 spk1>	 zéro+un
034 spk2>	 oui
035 spk1>	 zéro+zéro
036 spk2>	 oui
037 spk1>	 zéro+neuf
038 spk2>	 merci beaucoup
039 spk2>	 monsieur je vous en prie
040 spk1>	 mais je vous en prie bonne fin de journée au+revoir
041 spk2>	 au+revoir

