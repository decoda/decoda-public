
DIALOG=20091112_RATP_SCD_0565

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 oui bonjour
003 spk2>	 euh voilà j' aurais voulu un petit renseignement s() au+sujet+de la ligne euh soixante-neuf euh des des bus
004 spk1>	 hm ouais
005 spk2>	 euh sur euh internet visiblement les nouveaux les nouveaux horaires n' ont pas été mis à
006 spk2>	 jour
007 spk2>	 et il y a marqué le dernier départ je crois à vingt heures trente ou vingt+et+une heures trente et d'+après ce+que j' avais vu c' était bien plus tardif que ça est-ce+que vous pouvez me confirmer euh
008 spk1>	 alors
009 spk1>	 au+départ+de quelle euh
010 spk2>	 alors euh au départ ben c' est euh Champ-de-Mars euh Gambetta
011 spk1>	 d'accord
012 spk1>	 et vous le prenez où vous à Champ-de-Mars
013 spk2>	 non non non
014 spk1>	 vous le prenez où
015 spk2>	 elle le le prendra pas s()
016 spk2>	 euh elle va le prendre euh à je crois à Saint-Paul
017 spk2>	 à+peu+près
018 spk1>	 Saint-Paul d'accord
019 spk1>	 en direction de Gambetta
020 spk2>	 oui
021 spk2>	 parce+que c' est pareil sur les c' est marqué qu' il y a pas de bus le le dimanche alors+qu' en+fait ils ils roulent les bus le le dimanche le le dimanche
022 spk1>	 alors le dernier passage à Saint-Paul
023 spk1>	 vingt-deux heures cinquante-six
024 spk2>	 vingt-deux heures cinquante-six
025 spk1>	 ouais
026 spk2>	 d'accord bon+ben je vous remercie
027 spk1>	 je vous en prie
028 spk2>	 au+revoir
029 spk1>	 au+revoir

