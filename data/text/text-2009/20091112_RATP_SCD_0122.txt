
DIALOG=20091112_RATP_SCD_0122

001 spk1>	 NNAAMMEE va vous répondre
002 spk2>	 NNAAMMEE bonjour
003 spk3>	 oui bonjour monsieur
004 spk2>	 bonjour madame
005 spk3>	 je vous appelle euh j' habite plutôt dans les Hauts-de-Seine euh pas loin+de la mairie de Puteaux ou+alors sur les quais vers le square Léon+Blum
006 spk2>	 oui
007 spk3>	 et j' aurais voulu savoir l' autobus qui dessert la clinique Ambroise+Paré à Neuilly-sur-Seine
008 spk2>	 d'accord ouais
009 spk3>	 dans le quatre-vingt-douze
010 spk3>	 s'il+vous+plaît
011 spk2>	 donc vous êtes
012 spk2>	 euh vous m' avez dit à
013 spk2>	 proximité de
014 spk3>	 ben moi je
015 spk3>	 suis si vous voulez
016 spk3>	 pas loin du+tout de euh des des quais Léon+Blum euh Soljenitsyne là je sais pas comment vous appelez la
017 spk3>	 la station
018 spk2>	 Soljenitsyne là
019 spk3>	 voilà
020 spk2>	 oui par+rapport+au au cent cinquante sept qui passe dans le secteur sur le quai en+fait hein au+niveau+du quai donc pour aller au l' Hôpital+Ambroise+Paré on va dire euh vous avez l' arrêt Bellini qui est pas très loin aussi
021 spk3>	 arrêt Bellini
022 spk2>	 peut-être avant ou après
023 spk2>	 enfin c' est c' est avant Soljenitsyne sur sur le cent+soixante-quinze
024 spk3>	 oui
025 spk2>	 hein donc je vais me renseigner pour voir vous patientez
026 spk2>	 pour vous donner un trajet efficace hein merci
027 spk3>	 merci beaucoup
028 spk3>	 je suis à la hauteur du frein
029 spk3>	 mais enfin ça vous peut-être rien
030 spk2>	 d'accord
031 spk2>	 un petit instant je me renseigne hein
032 spk3>	 merci
033 spk2>	 madame s'il+vous+plaît
034 spk3>	 oui
035 spk2>	 je vous remercie d' avoir patienté
036 spk2>	 donc allez emprunter le+mieux c' est de récupérer le bus cent soixante quinze
037 spk3>	 oui
038 spk2>	 donc à
039 spk2>	 Soljenitsyne ou ou Bellini hein sur le quai de Dion+Bouton
040 spk3>	 oui
041 spk2>	 direction Porte+de+Saint-Cloud
042 spk3>	 Porte+de+Saint-Cloud
043 spk2>	 donc vous allez descendre à
044 spk1>	 au Pont+de+Suresnes
045 spk3>	 je descends au Pont+de+Suresnes
046 spk2>	 voilà
047 spk3>	 oui
048 spk2>	 alors par+contre pour récupérer le bus suivant c' est pas au même arrêt parce+que le cent+soixante-quinze vous laisse sur le quai hein
049 spk2>	 quai Léon
050 spk3>	 ouais
051 spk2>	 Blum
052 spk3>	 ouais
053 spk2>	 sous le Pont+de+Suresnes en+fait et au+dessus
054 spk2>	 vous avez l' accès c' est pas très loin mais c' est au+dessus hein il faut récupérer le deux+cent+quarante-et-un
055 spk3>	 deux+cent+quarante-et-un
056 spk2>	 direction Porte+d'Auteuil
057 spk3>	 ouais
058 spk2>	 jusqu'à l' arrêt Hôpital+Ambroise+Paré
059 spk3>	 d'accord
060 spk2>	 d'accord
061 spk3>	 Porte+d'Auteuil
062 spk3>	 voilà
063 spk2>	 tout+à+fait
064 spk3>	 d'accord
065 spk2>	 et là il me dépose près+de la de l' hôpital
066 spk3>	 de la clinique
067 spk2>	 il vous dépose sur le
068 spk2>	 boulevard Anatole+France c' est bien à l' Hôpital+Ambroise+Paré que vous allez hein
069 spk3>	 c' est une clinique
070 spk2>	 c' est une clinique
071 spk2>	 avez vous l' adresse
072 spk3>	 oui
073 spk2>	 parce+que moi j' ai hôpital hein et c' est tout hein c' est sur Boulogne
074 spk3>	 non c' est sur Neuilly sur
075 spk3>	 Seine
076 spk2>	 ah
077 spk2>	 au temps pour moi
078 spk2>	 alors j' étais pas sur le même trajet
079 spk3>	 alors on est pas
080 spk2>	 attendez on va rectifier tout ça hein
081 spk3>	 on va rectifier
082 spk3>	 tout ça
083 spk2>	 oh oui
084 spk2>	 parce+que c' est pas+du+tout le même endroit vous clinique Ambroise+Paré Hôpital+Ambroise+Paré c' est pas pareil
085 spk3>	 non
086 spk2>	 donc
087 spk2>	 c' est pas grave le premier bus c' est le bon hein on le garde hein
088 spk3>	 ouais
089 spk2>	 cent+soixante-quinze
090 spk3>	 oui
091 spk2>	 vous allez le prendre dans l' autre sens par+contre direction Gabriel+Péri
092 spk3>	 Gabriel
093 spk3>	 Péri
094 spk2>	 voilà vous le tr()
095 spk2>	 prenez sur le trottoir opposé hein pas vers le
096 spk2>	 hein le voilà mais vers Gabriel+Péri
097 spk3>	 pas vers Saint-Cloud voilà
098 spk2>	 on est d'accord
099 spk3>	 voilà
100 spk3>	 d'accord
101 spk2>	 vous allez descendre à l' arrêt Bineau Pont+Bineau pardon
102 spk3>	 Bineau
103 spk2>	 Pont+Bineau
104 spk3>	 Pont+Bineau
105 spk2>	 et là vous récupérez juste à l' angle en+fait euh
106 spk3>	 oui
107 spk2>	 du boulevard euh sur le boulevard euh de Verdun le bus cent soixante-quatre
108 spk3>	 oui
109 spk2>	 direction Porte+de+Champerret
110 spk3>	 cent soixante-quatre
111 spk2>	 direction Porte+de+Champerret
112 spk3>	 porte Champerret
113 spk3>	 oui
114 spk2>	 c' est pas très loin après hein et vous descendez à l' arrêt Victor+Hugo
115 spk3>	 arrêt
116 spk2>	 Victor+Hugo
117 spk3>	 Victor+Hugo
118 spk2>	 et là il vous laisse juste à l' angle de la du boulevard Victor+Hugo et face+à la clinique Am() Ambroise+Paré
119 spk3>	 ah d'accord
120 spk2>	 voilà
121 spk3>	 d'accord
122 spk2>	 c' est mieux là
123 spk3>	 ben écoutez je vous remercie
124 spk3>	 infiniment
125 spk2>	 je vous en prie madame
126 spk3>	 merci beaucoup
127 spk3>	 bonne journée au+revoir
128 spk2>	 bonne journée au+revoir madame
129 spk2>	 au+revoir

