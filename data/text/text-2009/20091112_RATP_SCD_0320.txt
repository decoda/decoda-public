
DIALOG=20091112_RATP_SCD_0320

001 spk1>	 NNAAMMEE va vous répondre
002 spk1>	 bonjour monsieur NNAAMMEE
003 spk2>	 bonjour
004 spk1>	 euh voilà je voudrais savoir je m' en vais sur Créteil je prends bon les autobus et le RER+A donc je voulais savoir s' il n' y avait pas de problèmes
005 spk2>	 ah non il n' y a pas de soucis madame
006 spk1>	 il n' y a pas de soucis
007 spk2>	 non non
008 spk1>	 je voulais me renseigner
009 spk2>	 sur le RER+A
010 spk1>	 d'accord
011 spk2>	 vous prenez quel bus
012 spk1>	 eh+bien écoutez je prends le cent+dix-huit d'+abord je prends le RER jusqu'à Joinville et après je prends le deux+cent+quatre-vingt-un jusqu'à Créteil
013 spk2>	 alors le deux+cent+quatre-vingt-un c' est bon madame
014 spk1>	 c' est bon
015 spk2>	 oui oui
016 spk1>	 merci beaucoup
017 spk2>	 je vous en prie madame
018 spk1>	 au+revoir
019 spk2>	 au+revoir bonne journée
020 spk1>	 vous aussi
021 spk2>	 merci

