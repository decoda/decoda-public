
DIALOG=20091112_RATP_SCD_0525

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 bonjour monsieur excusez -moi je voulais un+peu me renseigner là parce+que mon fils il est arrivé dans son internat en retard on m' a appelée c' est pour cela je voulais vérifier s' il nous a dit la vérité ou pas
003 spk1>	 ben oui mais pourquoi
004 spk2>	 pour pour les problèmes de bus
005 spk2>	 et
006 spk1>	 de quel bus
007 spk2>	 du cent+soixante-deux
008 spk1>	 quand aujourd'hui
009 spk2>	 oui pour aujourd'hui
010 spk1>	 quand ce matin
011 spk2>	 il avait
012 spk2>	 oui ce matin il avait attendu ça à Cachan il m' a dit qu' il a fait à+peu+près deux heures de temps à l' arrêt du bus
013 spk1>	 ben
014 spk2>	 c' était à
015 spk2>	 dix heures et quart c' est pour cela il est arrivé en retard c' est pour cela internat m' a appelée on est en+train+de vérifier si c' était vrai
016 spk1>	 d'accord le cent+soixante+deux ce matin il était à quel arrêt vous me dites à
017 spk2>	 à arrêt Cachan
018 spk1>	 ouais ça f() ouais ça m' étonnerait pas parce+que c' est vrai qu' il y avait euh comme il y avait encore des problèmes sur le RER+B et que tout+le+monde se soit rabattu sur les bus c' est possible et il allait dans quelle direction excusez -moi il allait dans quelle direction
019 spk2>	 euh hum forêt euh comment on appelle ça Val de Fo() Val de vers Meudon
020 spk1>	 d'accord
021 spk2>	 direction Meudon
022 spk1>	 je vous fais patienter
023 spk2>	 merci
024 spk1>	 s'il+vous+plaît
025 spk2>	 allo
026 spk1>	 oui je vous confirme hein qu' il y a eu un gros problème parce+que il y a eu un arrêt euh de travail euh spontané donc euh du enfin des des machinistes entre+autres du cent+soixante-deux et il y avait en+moyenne euh quarante-quatre pourcent de euh du trafic assuré
027 spk2>	 merci beaucoup monsieur
028 spk1>	 c' était vers quelle heure
029 spk2>	 bien il était à l' arrêt du bus vers huit heures et demie
030 spk1>	 ouais c' est ça ouais ouais là à+ce+moment-là il y avait même moins hein il y avait trente-huit pourcent de trente trente-huit pourcent de de de de de bus qui qui assuraient le le service
031 spk2>	 merci beaucoup monsieur
032 spk1>	 à votre service
033 spk1>	 bonne journée
034 spk2>	 au
035 spk1>	 au+revoir
036 spk2>	 au+revoir

