
DIALOG=20091112_RATP_SCD_0470

001 spk1>	 NNAAMMEE bonjour
002 spk2>	 bonjour madame
003 spk1>	 je vous écoute madame
004 spk2>	 oui j' ai perdu un un sac euh porte-documents ce matin
005 spk1>	 oui
006 spk2>	 euh sur la ligne sept et euh j' ai téléphoné à un collègue à vous il m' a dit de téléphoner à midi pour voir
007 spk1>	 d'accord est-ce+que vous pouvez me le décrire le porte-document
008 spk1>	 s'il+vous+plaît
009 spk2>	 noir
010 spk1>	 oui
011 spk2>	 où y a un classeur rouge
012 spk1>	 oui un parapluie
013 spk2>	 des gants
014 spk1>	 oui
015 spk2>	 et y a un sac où y a un pyjama tout neuf Triumph
016 spk1>	 d'accord est-ce+qu' il y avait des documents à votre nom
017 spk2>	 oui
018 spk1>	 à quel nom madame
019 spk2>	 NNAAMMEE
020 spk1>	 oui
021 spk2>	 NNAAMMEE
022 spk1>	 oui
023 spk2>	 NNAAMMEE
024 spk1>	 d'accord donc là vous m' indiquez avoir perdu aujourd'hui dans le métro ligne sept un porte-document noir avec des documents au+nom+de NNAAMMEE et il y aurait il y avait d' autres documents d' autres euh éléments à l' intérieur
025 spk1>	 c' est ça d'accord
026 spk2>	 oui voilà voilà oui
027 spk1>	 un instant j' effectue la recherche
028 spk2>	 merci beaucoup madame
029 spk1>	 s'il+vous+plaît
030 spk2>	 allo
031 spk1>	 actuellement je n' ai aucun porte-document qui puisse correspondre hein ni même des documents au+nom+de NNAAMMEE hein
032 spk2>	 moui c' est un classeur rouge où y a plein+de feuilles euh
033 spk1>	 parce+que depuis ce matin il n' a pas été enregistré hein
034 spk2>	 il n' a pas été enregistré
035 spk1>	 tout+à+fait
036 spk2>	 oui parce+que j' ai un collègue à vous ce matin il m' a dit qu' ils ont trouvé un porte euh document mais il m' a pas précisé il m' a dit euh moi j' ai dit que c' est un sac noir
037 spk1>	 alors le le+vôtre serait plutôt sous forme de sac
038 spk2>	 sous forme de sac
039 spk1>	 plutôt+qu' un
040 spk1>	 porte-documents
041 spk2>	 en eb()
042 spk1>	 c' est ça
043 spk2>	 forme de sac euh long euh
044 spk1>	 oui
045 spk2>	 oui un l() où j' ai mis mes tout mes toutes+mes affaires euh
046 spk1>	 alors je vais vérifier si y aurait un sac qui puisse se rapprocher
047 spk2>	 voilà
048 spk1>	 un instant s'il+vous+plaît
049 spk2>	 ouais
050 spk1>	 madame
051 spk2>	 oui
052 spk1>	 sur la ligne quatre depuis ce matin je n' ai aucun sac ni même porte-documents d' enregistré
053 spk2>	 c' est pas
054 spk1>	 hein
055 spk2>	 la ligne quatre c' est la ligne sept
056 spk1>	 ah parce+que là j' ai effectué une recherche sur l' ensemble de toutes+les lignes
057 spk2>	 ah oui
058 spk1>	 alors par+contre ce+que je peux faire je vais vous dire ça tout+de+suite alors aussi+bien la quatre la sept ou autre hein on va regarder sur tout+le réseau un instant
059 spk1>	 s'il+vous+plaît
060 spk2>	 moi j' étais
061 spk2>	 sur la ligne sept entre neuf heures et quart et neuf heures et demie provenance euh Villejuif à La+Courneuve c' est à Château-Landon que je suis descendue
062 spk1>	 d'accord non non moi je n' ai vraiment rien qui pourrait se rapprocher hein mis+à+part euh euh un porte-carte mais ça n' a rien à voir du+tout hein par rapport à aux indications que vous m' indiquez
063 spk2>	 ah non porte-carte non
064 spk1>	 je n' ai vraiment rien qui se rapprocherait
065 spk2>	 et et est-ce+que
066 spk2>	 est-ce+que est-ce+que je peux laisser mon numéro de téléphone s' ils le trouvent
067 spk1>	 alors
068 spk2>	 ou quelque+chose comme+ça
069 spk1>	 c' est vrai que de de de de ce côté là ça sera à vous de renouveler votre appel et on pourra effectuer une autre recherche à tout moment
070 spk2>	 ah d'accord merci beaucoup
071 spk1>	 je vous en prie
072 spk2>	 faut le renouveler
073 spk2>	 l' après-midi
074 spk1>	 bien+sûr tout+à+fait
075 spk2>	 d'accord merci beaucoup madame
076 spk1>	 bonne journée madame au+revoir
077 spk2>	 au+revoir

