
DIALOG=20091112_RATP_SCD_0814

001 spk1>	 NNAAMMEE bonjour
002 spk1>	 NNAAMMEE bonjour
003 spk2>	 oui bonjour
004 spk2>	 je vous téléphone pour un petit renseignement là euh je suis à Montreuil et je voulais s() en savoir plus euh j' ai pas le temps d' aller sur internet
005 spk1>	 oui
006 spk2>	 sur la ligne B euh du RER
007 spk1>	 oui
008 spk2>	 RATP est-ce+que où en on en est où là pour euh
009 spk1>	 alors
010 spk2>	 euh
011 spk1>	 je euh je regarde là en temps euh alors
012 spk1>	 là j' en vois un deux
013 spk2>	 parce+que j' ai failli pas arriver ce matin moi hein
014 spk1>	 a+priori
015 spk2>	 j' ai eu du mal hein
016 spk1>	 t() oui je sais bien là ils ont ils ont dit trois trains sur quatre moi je serais plutôt sur deux sur quatre
017 spk1>	 euh mais il y a des trains hein
018 spk2>	 oui mais il y a toujours on est loin des cent pour cent
019 spk2>	 parce+que quand on est à cent pour cent on a du mal à monter déjà
020 spk1>	 oui ouais ouais non non non non
021 spk1>	 non non non on n' est pas à cent pour cent non non non
022 spk2>	 donc on est de deux ou trois sur quatre se
023 spk1>	 voilà malheureusement oui
024 spk2>	 comme ce matin en+fait
025 spk1>	 oui oui oui pas plus
026 spk2>	 d'accord bon alors on va faire un autre circuit
027 spk1>	 pas plus hm
028 spk2>	 euh il y a quoi comme perturbations RATP connues euh ligne B RATP sud et
029 spk2>	 c' est tout il y a pas de euh
030 spk1>	 c' est tout c' est tout
031 spk2>	 il y a pas de ligne euh la treize par+exemple fonctionne
032 spk1>	 elle fonctionne euh tout+le RER+A euh le le métro en+général
033 spk2>	 d'accord donc il y a rien d' autre à signaler
034 spk1>	 non
035 spk2>	 OK ben je vous remercie
036 spk1>	 ben je vous en prie
037 spk1>	 bonne fin de journée quand+même au+revoir
038 spk2>	 bonne soirée euh

