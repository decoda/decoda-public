
DIALOG=20091112_RATP_SCD_0458

001 spk1>	 NNAAMMEE bonjour s'il+vous+plaît
002 spk2>	 oui bonjour madame excusez m() est-ce+que vous pourriez me donner le euh le le trajet aller-retour en euh en bus de de Cachan La+Plaine à Sainte-Anne
003 spk1>	 alors pardon vous dites Cachan La+Plaine
004 spk2>	 oui à Sainte-Anne
005 spk1>	 à Sainte-Anne à Paris non
006 spk2>	 oui le le l' hôpital
007 spk1>	 l' hôpital
008 spk2>	 oui
009 spk2>	 oui
010 spk1>	 un instant s'il+vous+plaît
011 spk1>	 voulez toute euh tout+l' itinéraire en bus
012 spk2>	 ben les deux parce+que je sais quel est quel est le+mieux parce+qu' en bus c' est pas facile non
013 spk2>	 de de
014 spk1>	 ben en bus
015 spk1>	 ça va pas être évident hein ça va être long hein
016 spk2>	 c'est-à-dire y a combien+de changements
017 spk1>	 ben trois deux hein
018 spk2>	 ben c' est pas grave parce+que en métro ça va plus vite
019 spk1>	 ah+ben en métro ça va automatiquem() oui tout+à+fait
020 spk2>	 ah oui
021 spk1>	 faut que vous
022 spk1>	 alliez au RER de Cachan
023 spk2>	 oui
024 spk1>	 après vous prenez le RER jusqu'à Luxembourg
025 spk2>	 RER Cachan y a pas de grève là
026 spk1>	 euh le B en ce moment euh le B y a trois trains sur quatre aujourd'hui c' est pour aujourd'hui
027 spk2>	 oui alors les alors attendez je vais prendre les deux mais alors
028 spk1>	 ouais on va voir le bus ben sinon
029 spk2>	 donc Luxembourg
030 spk1>	 en bus
031 spk2>	 attendez je vais prendre les deux parce+que je je risque d' y aller plusieurs fois Luxembourg p() et après Luxembourg
032 spk1>	 a() alors attendez excusez -moi ben après Luxembourg vous avez à la limite un bus le vingt-et-un
033 spk2>	 alors attendez
034 spk1>	 qui vous dépose à côté
035 spk2>	 ah oui ben c' est pas mal ça
036 spk1>	 le vingt-et-un c' est direction Stade
037 spk1>	 Charléty
038 spk2>	 attendez
039 spk2>	 Luxembourg vingt+et+un et je m' arrête
040 spk1>	 et vous descendez à l' arrêt Daviel D A V I E L
041 spk2>	 et là c' est bon là
042 spk1>	 ben vous êtes à cinq minutes
043 spk2>	 alors par le bu je
044 spk1>	 sinon oui ben sinon tout
045 spk2>	 oui aujourd'hui je vais prendre
046 spk2>	 le bus parce+que
047 spk1>	 ben sinon en bus en bus euh parce+que sinon vous pouvez ah oui ben alors en bus un instant
048 spk1>	 ben en bus sinon vous avez le attendez je suis en tr() euh la Valouette vous pouvez la récupérer le bus Valou() ah non non à l' arrivée ça vous fait un quart d' heure à pied c' est pas la peine alors je reviens au ce+que j' allais vous dire au départ le cent+quatre-vingt-quatre au départ
049 spk2>	 oui ça ça je connais bien
050 spk1>	 qui vous dépose à Porte+d'Italie
051 spk2>	 effectivement oui Porte+d'Italie oui
052 spk1>	 là le quarante-sept
053 spk2>	 Porte+d'Italie
054 spk1>	 direction Gare+de+l'Est
055 spk2>	 quarante-sept
056 spk1>	 vous descendez à Italie Tol() Italie-Tolbiac s'il+vous+plaît
057 spk2>	 oui je suis là attendez si je marque pas bien après Italie alors direction oui Tolbiac
058 spk1>	 vous descendez donc à Italie-Tolbiac et là vous prenez le soixante-deux direction Porte+de+Saint-Cloud et vous descendez à Glacière-Tolbiac
059 spk2>	 et je prends l() euh
060 spk1>	 le soixante-deux
061 spk2>	 parce+que
062 spk2>	 je pendant je pense que je pense que je vais euh je vais prendre par le RER euh RER Cachan Luxembourg Luxembourg prendre le bus vingt-et-un Daviel
063 spk1>	 hm+hm
064 spk2>	 et après pour le pour le retour
065 spk2>	 hm
066 spk1>	 ben
067 spk1>	 pour le retour alors attendez un instant
068 spk2>	 jusqu'à Arcueil-Cachan après je me débrouille
069 spk1>	 ben le retour le vingt-et-un ben le vingt-et-un on avait dit quoi
070 spk2>	 oui je reprends le vingt-et-un dans l' autre sens mais je m' arrête où
071 spk1>	 alors attendez
072 spk2>	 ben à Luxembourg ben non
073 spk1>	 je vous ai dit quoi
074 spk1>	 le vingt-et-un ben vous pouvez aussi le récup() euh Luxembourg je je crois que je vous avais dit tout à l' heure non je sais plus
075 spk2>	 ben oui si je prends le vingt-et-un je me je
076 spk1>	 dans l' autre sens vous descendez à Luxembourg
077 spk2>	 oui c' est ce+que je pensais oui
078 spk1>	 et à Luxembourg le B ben vous le reprenez direction Saint Ma() Saint Saint-Rémy
079 spk2>	 voilà puis je descendrai qu' à Cachan je crois que c' est mieux hein parce+que
080 spk1>	 oh oui c' est plus rapide mais bon c' est vrai que là Stade pour aujourd() ça fait quand+même plus rapide
081 spk2>	 mais euh y en a régulièrement ou c' est vraiment serré la va la
082 spk1>	 les B y en a trois sur quatre là
083 spk2>	 c' est c' est c' est vrai vraiment suivi oh excusez -moi parce+que
084 spk1>	 ah non là y en a trois sur non non là ça roule euh presque normal y en a trois sur quatre hein ça roule beaucoup mieux là hein
085 spk2>	 bon d'accord alors le euh le vingt-et-un euh et le vingt-et-un euh quand je euh de de Sainte-Anne c' est quel euh c' est c' est quel arrêt je suis un+peu perdue là de Sainte-Anne je le je le récupère où
086 spk2>	 le vingt-et-un
087 spk1>	 l' arrêt
088 spk1>	 Saint ben c' est vingt c' est je vous avais dit je crois euh attendez
089 spk2>	 de l' arrêt Sainte-Anne comment je fais moi
090 spk1>	 Da Daviel non
091 spk2>	 euh c'est-à-dire que je descendais à Daviel pour Sainte-Anne et je le reprends à Da à Daviel
092 spk1>	 vous le reprenez alors attendez non vous le reprenez parce+que y a des sens uniques
093 spk2>	 ben oui au fait oui
094 spk2>	 oui
095 spk1>	 euh
096 spk1>	 alors attendez je vous avais dit quoi je vous avais dit de changer à Luxembourg c' est ça
097 spk2>	 vous m' avez dit euh
098 spk1>	 à l' aller
099 spk1>	 Luxembourg
100 spk2>	 oui
101 spk2>	 oui je Luxembourg
102 spk2>	 prendre le vingt
103 spk1>	 là vingt
104 spk1>	 le vingt-et-un
105 spk2>	 oui
106 spk1>	 alors attendez parce+que le vingt-et-un y a des sens uniques attendez alors donc là on va revérifier dans le sens Luxembourg on va dire euh Daviel alors de Luxembourg à Sainte-Anne donc c' est le vingt-et-un direction Stade+Charléty pour l' aller vous descendez à Daviel
107 spk2>	 alors Luxembourg euh prendre la
108 spk1>	 alors quand vous êtes
109 spk2>	 dire
110 spk1>	 à Daviel vous êtes dans une petite rue
111 spk1>	 hein vous avancez un+petit+peu
112 spk2>	 c' est quel c' est direction où
113 spk1>	 direction Stade+Charléty
114 spk2>	 et je desc() je descends à D à Daviel
115 spk1>	 Daviel
116 spk1>	 oui
117 spk2>	 oui
118 spk1>	 et quand vous êtes à Daviel vous sortez du bus vous avancez un+petit+peu et vous aurez la rue de la Santé sur la droite et là vous aurez l' hôpital vous l' aurez devant vous avez peut-être une entrée devant aussi hein
119 spk2>	 d'accord alors après pour récupérer de Sainte-Anne je prends le vingt-et-un et je le prends à quel arrêt
120 spk1>	 alors p() pardon pour le retour oui un instant
121 spk2>	 parce+que je je connais pas+du+tout je vais me paumer hein euh
122 spk1>	 ben le retour alors le Daviel vous Daviel vous le récupérez
123 spk2>	 ben c' est pas Da euh c' est pas Daviel c' est pas le même sens si
124 spk1>	 euh si mais c' est rue oui c' est l' arrêt mais ça va pas vous euh
125 spk1>	 l' arrêt ça sera mais ça c' est presque à+côté+de l' arrêt où vous le où vous descendez
126 spk2>	 faut pas que je me trompe de sens euh
127 spk1>	 hein c' est rue Glacière mais c' est c' est à côté hein d'où vous descendez hein euh de de l' aller hein c' est à quelques mètres hein
128 spk2>	 c' est c' est quelle rue que je me trompe pas
129 spk2>	 de station
130 spk1>	 la même
131 spk1>	 c' est rue rue Glacière intersection de la rue Boutin et de la rue Glacière
132 spk2>	 sur Luxembourg je peux demander voilà
133 spk1>	 hm
134 spk2>	 euh rue euh rue Glacière le vingt-et-un et je vais jusqu'à Luxembourg
135 spk1>	 là c' est direction Saint-Lazare
136 spk2>	 ah oui direction Saint-Lazare oh+ben je trouverai
137 spk1>	 mais là ouais
138 spk2>	 et je vais à jusqu'à Luxembourg
139 spk1>	 oui
140 spk2>	 après c' est tout simple
141 spk2>	 bon mais merci madame
142 spk1>	 ben je vous en prie madame
143 spk2>	 au+revoir
144 spk1>	 bonne journée au rev()
145 spk1>	 au+revoir

