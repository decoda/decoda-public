
DIALOG=20091112_RATP_SCD_0009

001 spk1>	 bonjour
002 spk2>	 oui bonjour madame
003 spk1>	 euh je voudrais savoir le RER+B est-ce+qu' il refonctionne ou
004 spk2>	 pas
005 spk1>	 je crois
006 spk1>	 trois trains sur quatre
007 spk2>	 ah d'accord euh en+effet je dois me rendre ce matin au+départ+du RER de mais c' est le RER+C
008 spk1>	 ah c' est le C ça oui c' est le C
009 spk2>	 donc il y a il y a pas de
010 spk2>	 problème
011 spk1>	 non non non non
012 spk2>	 d'accord je dois me rendre en+fait euh au cent+quatre avenue de Verdun euh à hum que je vous dise pas des bêtises à Villeneuve-la-Garenne
013 spk2>	 euh
014 spk1>	 oui
015 spk2>	 et je voudrais que vous me disiez à quelle heure je dois partir de de il n' y a pas de un impératif vers huit heures et demie euh voilà et après je sais que je dois prendre le bus cent+soixante-dix-huit
016 spk1>	 vous me dites euh pardon et vous allez à Villeneuve-la-Garenne
017 spk2>	 au cent+quatre avenue Verdun donc je sais que je dois prendre le cent+soixante-dix-huit
018 spk2>	 mais je
019 spk1>	 hum
020 spk2>	 dois y être vers euh huit heures et demie et je voudrais que vous me disiez à quelle heure j' ai un RER de de
021 spk1>	 un instant s'il+vous+plaît
022 spk2>	 merci beaucoup
023 spk1>	 s'il+vous+plaît on m' avait dit pardon huit heures et demie il arrivait
024 spk2>	 c' est possible ça
025 spk1>	 moi j' ai une arrivée ou à huit heures dix-sept ou à huit euh pardon pardon c' est le bus euh votre bus euh oui donc avec votre bus ça vous fait arriver vers huit heures trente-cinq ou huit heures dix-sept avec votre bus donc t' aurais fait un départ de sept heures cinquante-six de
026 spk1>	 huit heures onze oui de
027 spk2>	 euh alors
028 spk1>	 oui
029 spk2>	 huit heures onze de et j' arrive à quelle heure
030 spk2>	 à
031 spk1>	 ça arrive à huit
032 spk1>	 heures vingt-deux à
033 spk2>	 euh c' est ah oui vous m' avez dit à quelle
034 spk1>	 huit heures vingt-deux
035 spk2>	 oui
036 spk1>	 et vous prenez votre bus vous en avez un à huit heures trente et ça vous fait arriver en+principe à trente-quatre
037 spk2>	 ah c' est super OK
038 spk2>	 merci beaucoup
039 spk1>	 ben je vous en prie
040 spk1>	 bonne journée au+revoir
041 spk2>	 merci au+revoir

