
DIALOG=20091112_RATP_SCD_0823

001 spk1>	 ah on va faire autre+chose
002 spk1>	 NNAAMMEE bonjour
003 spk2>	 allo
004 spk1>	 oui NNAAMMEE bonjour
005 spk2>	 oui bonjour monsieur
006 spk2>	 je vous appelle parce+que je suis à un arrêt de bus et le bus n' est pas passé depuis dix minutes euh c' est la ligne cent+trente-sept
007 spk1>	 cent+trente-sept oui
008 spk2>	 et c' est le bus qui va en direction euh de Paris en+fait qui part de la zone industrielle euh de Villeneuve-la-Garenne et direction Saint-Ouen Porte+de+Saint-Ouen
009 spk1>	 d'accord
010 spk2>	 et je me demande j' en n' ai vu passer aucun dans l' autre sens non+plus donc je m' inquiète
011 spk2>	 euh est-ce+que c' est juste un tout petit retard ou pas
012 spk1>	 d'accord je vais voir si je peux
013 spk1>	 si je peux avoir le prochain passage vous pouvez patienter
014 spk2>	 merci
015 spk1>	 merci
016 spk1>	 oui madame donc le prochain passage prévu c' est dans vingt-et-une minutes
017 spk2>	 vingt+et+un et ça veut dire
018 spk2>	 qu' il y en a
019 spk1>	 d()
020 spk2>	 deux qui sont pas passés
021 spk1>	 euh maintenant je peux me renseigner à savoir pourquoi ils sont pas passés
022 spk1>	 mais
023 spk2>	 parce+que
024 spk2>	 il y avait celui de dix-huit là vous me dites que celui de trente-trois viendra pas non+plus enfin là euh
025 spk1>	 oui
026 spk1>	 oui ceci est
027 spk1>	 tellement fréquent
028 spk2>	 euh je suis à l' arrêt de
029 spk2>	 parc des Chanter des Chanteraines il y a il y en a pas quoi enfin ils je
030 spk1>	 ah là j' ai pris euh zone industrielle hein
031 spk1>	 vous me parliez de la zone industrielle
032 spk2>	 ah oui non mais
033 spk1>	 bon c' est pas très loin
034 spk2>	 oui oui ce c' est l' arrêt de c' est le
035 spk2>	 début de la ligne
036 spk1>	 d'accord
037 spk2>	 hein ouais ouais
038 spk1>	 et là le prochain pour euh Porte+de+Clignancourt c' est vingt+et+une minutes
039 spk2>	 pff
040 spk2>	 d'accord
041 spk2>	 bon+ben je vous remercie
042 spk1>	 je vous en prie madame
043 spk1>	 au+revoir
044 spk2>	 au+revoir

