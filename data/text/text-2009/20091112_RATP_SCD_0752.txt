
DIALOG=20091112_RATP_SCD_0752

001 spk1>	 NNAAMMEE bonjour
002 spk1>	 NNAAMMEE bonjour
003 spk2>	 oui bonjour je suis bien à la RATP
004 spk1>	 oui
005 spk2>	 oui bonjour madame société à Saran euh je vous appelle car euh euh je vais vous faire une commande ben comme d'+habitude de tickets de métro et de RER par+contre je suis un+peu perdue dans le bon de commande parce+qu' il a changé
006 spk1>	 oui
007 spk2>	 en+fait euh euh dans quantité je sais pas combien marquer en+fait vous voyez un pack de hum de métro dedans il y a dix carnets de dix tickets
008 spk1>	 oui
009 spk2>	 donc dans quantité euh je marque euh en+fait je voudrais quinze blocs comme+ça je marque cent cinquante
010 spk1>	 non vous marquez quinze
011 spk2>	 je marque quinze
012 spk2>	 d'accord
013 spk1>	 oui
014 spk2>	 OK et euh après on en RER euh IDF j' en voudrais euh dix
015 spk1>	 oui
016 spk2>	 donc là je marquerai dix par+contre la dernière fois on a été embêtés parce+qu' on a reçu des tickets de RER qui n' étaient plus violets mais blancs
017 spk1>	 oh ça a rien à voir ça
018 spk2>	 euh ouais
019 spk1>	 ça c' est ils sont blancs depuis+longtemps hein
020 spk2>	 ah+bon ils sont blancs
021 spk2>	 depuis+longtemps hm d'accord
022 spk1>	 oui oui oui tout+à+fait
023 spk2>	 bon+ben on on va s' y faire alors et+puis par+contre on était un+petit+peu embêtés parce+qu' on on les a pas reçus emballés ils étaient par blocs de cent enroulés autour d' un d' un caoutchouc mais euh d'+habitude on les recevait par petits carnets de dix scellés en+fait
024 spk1>	 oui
025 spk1>	 ah oui mais ça dépend
026 spk1>	 des des lots hein en+fait
027 spk1>	 hein donc la même chose
028 spk2>	 d'accord OK
029 spk2>	 d'accord bon+ben je je vais vous envoyer ça je pense que je l' enverrai que lundi la la commande
030 spk1>	 ouais mais il y a pas de problème
031 spk2>	 OK
032 spk1>	 hein
033 spk2>	 ben je vous remercie
034 spk1>	 je vous en prie
035 spk2>	 bonne journée
036 spk2>	 au+revoir
037 spk1>	 bonne journée
038 spk1>	 au+revoir

