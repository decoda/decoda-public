# Format TSV SILVER (July 2014)

The linguistic annotations have been automatically produced by the <a href="https://hal.archives-ouvertes.fr/hal-01194861">MACAON NLP tool suite</a>, they might contain errors since they were not manually corrected.

- 0	= dialog ID
- 1	= turn ID
- 2	= word ID
- 3	= word
- 4	= disfluence
- 5	= POS
- 6	= Named Entity
- 7	= Chunk
- 8	= dependency label
- 9	= dependency link (w.r.t. word ID)
- 10	= text segment ID in the corresponding Transcriber (TRS) file
- 11	= lemma
- 12	= morphology (Lefff format)
- 13	= speaker (according to the TRS file)
- 14	= begin time in the audio file
- 15	= end time in the audio file
- 16	= pause duration after the end of the word (if available or ?)
- 17 	= end of sentence tag


## Example

`20101206_RATP_SCD_0476 16 6 signalé NULL vppart NULL I_GVfn ROOT 0 idtext0032 signaler K##m#s## spk2 109.25 109.73 0.01`

## Tagsets

- **disfluencies**
    - DISF_M	= discourse marker
    - DISF_R	= repetition
    - DISF_T	= troncation
    - GREF	= "greffe"
    - NULL	= none


- **POS**
    - adj
    - adv
    - advneg
    - cln
    - clneg
    - clo
    - clr
    - coo
    - csu
    - det
    - etr
    - nc
    - np
    - NULL
    - prep
    - pres
    - pri
    - pro
    - prorel
    - tronc
    - v
    - vinf
    - vppart
    - vprespart

- **Named Entity**
    - B_A	= begin adress
    - B_CP	= begin zip code (code postal)
    - B_D	= begin date
    - B_H	= begin time (horaire)
    - B_HU	= begin human (person)
    - B_L	= begin location
    - B_M	= begin monument
    - B_ORG	= begin organisation
    - B_P	= begin price
    - B_PRD	= begin product
    - B_T	= begin transport
    - B_TEL	= begin telephone
    - I_A	= inside adress
    - I_CP	= inside zip code (code postal)
    - I_D	= inside date
    - I_H	= inside time (horaire)
    - I_HU	= inside human (person)
    - I_L	= inside location
    - I_M	= inside monument
    - I_ORG	= inside organisation
    - I_P	= inside price
    - I_PRD	= inside product
    - I_T	= inside transport
    - I_TEL	= inside telephone
    - NULL	= none

- **dependency label**
    - AFF
    - A_OBJ
    - ARG_COMP
    - ATO
    - ATS
    - AUX
    - COORD
    - DE_OBJ
    - DEP
    - DEP_COORD
    - DET
    - MOD
    - MOD_REL
    - NOLINK
    - NULL
    - OBJ
    - P_OBJ
    - P_OBJ_LOC
    - ROOT
    - SUJ
    - SUJ_IMP
   
